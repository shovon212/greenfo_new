package layout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.muttasimfuad.greenfo.MainActivity;
import com.muttasimfuad.greenfo.MyProfileActivity;
import com.muttasimfuad.greenfo.R;
import com.muttasimfuad.greenfo.UploadProfilePic;
import com.muttasimfuad.greenfo.activity.MainActivity2;
import com.muttasimfuad.greenfo.app.Config;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;
import com.muttasimfuad.greenfo.oldutil.NotificationUtils;

public class EditProfileActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    // FCM Cloud Msg
    private static final String TAGFCM = MainActivity2.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String regId;
// FCM Cloud Msg


    private static final String TAG = "PhoneAuth";

    private EditText nameText, idnoText;
    private Spinner genderSpinner, deptSpinner, batchSpinner, shiftSpinner, secSpinner, bloodgroupSpinner, donation_availability_spinner;
    private Button saveButton;

    String nameOfUser, IdOfUser;

   // FirebaseAuth mAuth;
    String PropicURL, MyName, MyID, MyGender, MyDept, MyBatch, MyShift, MySec, MyBloodG, MyBloodDonationAvailability;
    ImageView myGenIMG;

    //new
//    private ValueEventListener mUserValueEventListener;
//    private DatabaseReference mUserRef;

//    private FirebaseAuth.AuthStateListener mAuthStateListener;
    FirebaseAuth mAuthx;
    FirebaseUser mFirebaseUser;
    AppUser appUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_my_profile);

        mAuthx = FirebaseAuth.getInstance();
        mFirebaseUser = mAuthx.getCurrentUser();

        nameText = (EditText) findViewById(R.id.usernameET);
        idnoText = (EditText) findViewById(R.id.IdNoET);
        saveButton = (Button) findViewById(R.id.SaveBTN);

        myGenIMG = (ImageView) findViewById(R.id.IMGgenderImage);

        genderSpinner = (Spinner) findViewById(R.id.SelectGenderSP);
        genderSpinner.setOnItemSelectedListener(this);

        deptSpinner = (Spinner) findViewById(R.id.SelectDeptSP);
        deptSpinner.setOnItemSelectedListener(this);

        batchSpinner = (Spinner) findViewById(R.id.SelectBatchSP);
        batchSpinner.setOnItemSelectedListener(this);

        shiftSpinner = (Spinner) findViewById(R.id.shiftSP);
        shiftSpinner.setOnItemSelectedListener(this);

        secSpinner = (Spinner) findViewById(R.id.secSP);
        secSpinner.setOnItemSelectedListener(this);

        bloodgroupSpinner = (Spinner) findViewById(R.id.bloodGroupSP);
        bloodgroupSpinner.setOnItemSelectedListener(this);

        donation_availability_spinner = (Spinner) findViewById(R.id.donatebloodSP);
        donation_availability_spinner.setOnItemSelectedListener(this);

        try {
            Intent cr = getIntent();
            MyName = cr.getStringExtra("nam");
            MyID = cr.getStringExtra("id");

            MyGender = cr.getStringExtra("gender");
            MyDept = cr.getStringExtra("dept");
            MyBatch = cr.getStringExtra("batch");
            MyShift = cr.getStringExtra("shift");
            MySec = cr.getStringExtra("sec");

            MyBloodG= cr.getStringExtra("blood_group");
            MyBloodDonationAvailability = cr.getStringExtra("donate_blood");
            PropicURL = cr.getStringExtra("propicurl");

            nameText.setText(MyName);
            idnoText.setText(MyID);


            try {
                if(PropicURL != null){
                Glide.with(EditProfileActivity.this)
                                    .load(PropicURL)
                                    .into(myGenIMG);
                }else {
                    Glide.with(EditProfileActivity.this)
                            .load("https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3")
                            .into(myGenIMG);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (MyGender.equals("MALE")) {
                genderSpinner.setSelection(0);
            } else genderSpinner.setSelection(1);

            if (MyDept.equals("CSE")) {
                deptSpinner.setSelection(0);
            }
            if (MyDept.equals("EEE")) {
                deptSpinner.setSelection(1);
            }
            if (MyDept.equals("TEXTILE")) {
                deptSpinner.setSelection(2);
            }
            if (MyDept.equals("ENGLISH")) {
                deptSpinner.setSelection(3);
            }
            if (MyDept.equals("BBA")) {
                deptSpinner.setSelection(4);
            }

            if (MyDept.equals("MBA")) {
                deptSpinner.setSelection(5);
            }

            if (MyDept.equals("EMBA")) {
                deptSpinner.setSelection(6);
            }
            if (MyDept.equals("SOCIOLOGY")) {
                deptSpinner.setSelection(7);
            }
            if (MyDept.equals("ANTHROPOLOGY")) {
                deptSpinner.setSelection(8);
            }
            if (MyDept.equals("ANTHROPOLOGY-MSS")) {
                deptSpinner.setSelection(9);
            }

            if (MyDept.equals("LLB(Honors)")) {
                deptSpinner.setSelection(10);
            }
            if (MyDept.equals("LLB(Pass)")) {
                deptSpinner.setSelection(11);
            }
            if (MyDept.equals("LLM")) {
                deptSpinner.setSelection(12);
            }
            if (MyDept.equals("FTDM")) {
                deptSpinner.setSelection(13);
            }


            //---------------------------

            if (MyBatch.equals("141")) {
                batchSpinner.setSelection(0);
            }
            if (MyBatch.equals("142")) {
                batchSpinner.setSelection(1);
            }
            if (MyBatch.equals("143")) {
                batchSpinner.setSelection(2);
            }
            if (MyBatch.equals("151")) {
                batchSpinner.setSelection(3);
            }
            if (MyBatch.equals("152")) {
                batchSpinner.setSelection(4);
            }

            if (MyBatch.equals("153")) {
                batchSpinner.setSelection(5);
            }

            if (MyBatch.equals("161")) {
                batchSpinner.setSelection(6);
            }
            if (MyBatch.equals("162")) {
                batchSpinner.setSelection(7);
            }
            if (MyBatch.equals("163")) {
                batchSpinner.setSelection(8);
            }
            if (MyBatch.equals("171")) {
                batchSpinner.setSelection(9);
            }

            if (MyBatch.equals("172")) {
                batchSpinner.setSelection(10);
            }
            if (MyBatch.equals("173")) {
                batchSpinner.setSelection(11);
            }
            if (MyBatch.equals("181")) {
                batchSpinner.setSelection(12);
            }
            if (MyBatch.equals("182")) {
                batchSpinner.setSelection(13);
            }
            if (MyBatch.equals("182")) {
                batchSpinner.setSelection(14);
            }
            if (MyBatch.equals("183")) {
                batchSpinner.setSelection(15);
            }
            if (MyBatch.equals("1001")) {
                batchSpinner.setSelection(16);
            }
            if (MyBatch.equals("1002")) {
                batchSpinner.setSelection(17);
            }
            if (MyBatch.equals("1003")) {
                batchSpinner.setSelection(18);
            }
            if (MyBatch.equals("1101")) {
                batchSpinner.setSelection(19);
            }
            if (MyBatch.equals("1102")) {
                batchSpinner.setSelection(20);
            }
            if (MyBatch.equals("1103")) {
                batchSpinner.setSelection(21);
            }
            if (MyBatch.equals("1201")) {
                batchSpinner.setSelection(22);
            }
            if (MyBatch.equals("1202")) {
                batchSpinner.setSelection(23);
            }
            if (MyBatch.equals("1203")) {
                batchSpinner.setSelection(24);
            }
            if (MyBatch.equals("1301")) {
                batchSpinner.setSelection(25);
            }
            if (MyBatch.equals("1302")) {
                batchSpinner.setSelection(26);
            }
            if (MyBatch.equals("1303")) {
                batchSpinner.setSelection(27);
            }


            //--------------------


            if (MyShift.equals("DAY")) {
                shiftSpinner.setSelection(0);
            }
            if (MyShift.equals("EVENING")) {
                shiftSpinner.setSelection(1);
            }


            if (MySec.equals("A")) {
                secSpinner.setSelection(0);
            }
            if (MySec.equals("B")) {
                secSpinner.setSelection(1);
            }
            if (MySec.equals("C")) {
                secSpinner.setSelection(2);
            }
            if (MySec.equals("D")) {
                secSpinner.setSelection(3);
            }
            if (MySec.equals("E")) {
                secSpinner.setSelection(4);
            }
            if (MySec.equals("F")) {
                secSpinner.setSelection(5);
            }
            if (MySec.equals("G")) {
                secSpinner.setSelection(6);
            }
            if (MySec.equals("H")) {
                secSpinner.setSelection(7);
            }
            if (MySec.equals("No Section")) {
                secSpinner.setSelection(8);
            }


            //blood group spinner set from record

            if (MyBloodG.equals("Unknown")) {
                bloodgroupSpinner.setSelection(0);
            }
            if (MyBloodG.equals("O+")) {
                bloodgroupSpinner.setSelection(1);
            }
            if (MyBloodG.equals("O-")) {
                bloodgroupSpinner.setSelection(2);
            }
            if (MyBloodG.equals("A+")) {
                bloodgroupSpinner.setSelection(3);
            }
            if (MyBloodG.equals("A-")) {
                bloodgroupSpinner.setSelection(4);
            }
            if (MyBloodG.equals("B+")) {
                bloodgroupSpinner.setSelection(5);
            }
            if (MyBloodG.equals("B-")) {
                bloodgroupSpinner.setSelection(6);
            }
            if (MyBloodG.equals("AB+")) {
                bloodgroupSpinner.setSelection(7);
            }
            if (MyBloodG.equals("AB-")) {
                bloodgroupSpinner.setSelection(8);
            }

            //donation set

            if (MyBloodDonationAvailability.equals("NO")) {
                donation_availability_spinner.setSelection(0);
            } else donation_availability_spinner.setSelection(1);



        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (mFirebaseUser != null) {
//            // mUserRef = FirebaseUtilsForUserInfo.getUserDpRef();
//            //mUserRef = FirebaseUtilsForUserInfo.getUserPhnRef(mFirebaseUser.getPhoneNumber());
//            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
//        }


//        try {
//
//            mUserValueEventListener = new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    if (dataSnapshot.getValue() != null) {
//
//                        AppUser appUser = dataSnapshot.getValue(AppUser.class);
//                        if (appUser.getPropicurl() == null) {
//                            pp = "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
//                        } else pp = appUser.getPropicurl();
//                        Glide.with(EditProfileActivity.this)
//                                .load(pp)
//                                .into(myGenIMG);
//                    }
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            };
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        // FCM

        try {
            // register GCM registration complete receiver
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.REGISTRATION_COMPLETE));

            // register new push message receiver
            // by doing this, the activity will be notified each time a new message arrives
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.PUSH_NOTIFICATION));

            // clear the notification area when the app is opened
            NotificationUtils.clearNotifications(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //FCM

    }

    //CHECK IF NETWORK IS CONNECTED OR NOT
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void saveBtnAction(View view) {

        nameOfUser = nameText.getText().toString().trim();
        IdOfUser = idnoText.getText().toString().trim();

        String gender = genderSpinner.getSelectedItem().toString();
        String dept = deptSpinner.getSelectedItem().toString();
        String batch = batchSpinner.getSelectedItem().toString();
        String shift = shiftSpinner.getSelectedItem().toString();
        String sec = secSpinner.getSelectedItem().toString();
        String blood_group = bloodgroupSpinner.getSelectedItem().toString();
        String donate_blood = donation_availability_spinner.getSelectedItem().toString();
        if (!nameOfUser.equals("") && !IdOfUser.equals("")) {
            if (isNetworkConnected() == true) {
                try {
//                    mAuth = FirebaseAuth.getInstance();
//                    FirebaseUser appUser = mAuth.getCurrentUser();
//                    CurrentUserPhoneNo = appUser.getPhoneNumber();
//
//                    DatabaseReference user_ifo = FirebaseDatabase.getInstance().getReference("user_info");
//                    DatabaseReference currentUserRefNew = user_ifo.child(CurrentUserPhoneNo);
//
//                    currentUserRefNew.child("personal_info").setValue("0");
//                    DatabaseReference personalInfoRef = currentUserRefNew.child("personal_info");
//                    personalInfoRef.child("username").setValue(nameOfUser);
//                    personalInfoRef.child("gender").setValue(gender);
//                    personalInfoRef.child("id").setValue(IdOfUser);
//                    personalInfoRef.child("batch").setValue(batch);
//                    personalInfoRef.child("department").setValue(dept);
//                    personalInfoRef.child("shift").setValue(shift);
//                    personalInfoRef.child("sec").setValue(sec);
//
//                    // FCM TEST
//                    // FCM _____________________________

                    try {
                        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                            @Override
                            public void onReceive(Context context, Intent intent) {

                                // checking for type intent filter
                                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                                    // gcm successfully registered
                                    // now subscribe to `global` topic to receive app wide notifications
                                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                                    displayFirebaseRegId();

                                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                                    // new push notification is received

                                    //                                                    String message = intent.getStringExtra("message");
                                    //
                                    //                                                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                                    //
                                    //                                                    txtMessage.setText(message);
                                }
                            }
                        };

                        displayFirebaseRegId();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    try {
//
//                        DatabaseReference personalInfoRef2 = currentUserRefNew.child("personal_info");
//                        if (!TextUtils.isEmpty(regId)) {
//                            personalInfoRef2.child("FCM_ID").setValue(regId);
//                        } else
//                            personalInfoRef2.child("FCM_ID").setValue("Firebase Reg Id is not received yet!");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    //FCM END
//
//                    //FCM Test
//
//
//                    Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();


                    appUser = new AppUser();


                    //      ProfilePicUploadInfo profilePicUploadInfo = new ProfilePicUploadInfo();
                    //profilePicUploadInfo.setPropicurl(null);
                    //          personalInfoRef.child("FCM_ID").setValue(regId);

                    //FirebaseUtilsForUserInfo.getUserDpRef().setValue(null);
                    appUser.setBatch(batch);
                    appUser.setDepartment(dept);
                    appUser.setGender(gender);
                    appUser.setId(IdOfUser);
                    appUser.setShift(shift);
                    appUser.setUsername(nameOfUser);
                    appUser.setSec(sec);
                    appUser.setPropicurl(PropicURL);

                    appUser.setBlood_group(blood_group);
                    appUser.setDonate_blood(donate_blood);
                    //FCM

                    if (!TextUtils.isEmpty(regId)) {
                        // personalInfoRef.child("FCM_ID").setValue(regId);
//                                                FcmIDInfo fcmIDInfo = new FcmIDInfo(regId);
//                                                FirebaseUtilsForUserInfo.getUserFcmRef().setValue(fcmIDInfo);
                        appUser.setFCM_ID(regId);


                    } else {
                        //   personalInfoRef.child("FCM_ID").setValue("Firebase Reg Id is not received yet!");


//                                                FcmIDInfo fcmIDInfo = new FcmIDInfo("Firebase Reg Id is not received yet!");
//                                                FirebaseUtilsForUserInfo.getUserFcmRef().setValue(fcmIDInfo);
                        appUser.setFCM_ID("Could not received");
                    }


                    //FCM


                    FirebaseUtilsForUserInfo.getUserinfoRef()
                            .setValue(appUser, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                                    //finish();
                                }
                            });


                    //new
                    Toast.makeText(EditProfileActivity.this, "Saved Successfully !!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(EditProfileActivity.this, MainActivity.class));
                    finish();


                } catch (Exception e) {
                    Toast.makeText(EditProfileActivity.this, "Failed !!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(EditProfileActivity.this, "Poor Internet Connection !!", Toast.LENGTH_SHORT).show();
            }
        } else {

            Toast.makeText(EditProfileActivity.this, "Please Fill Up All Information !!", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(EditProfileActivity.this, MyProfileActivity.class));
        finish();
    }


    // FCM -----
    private void displayFirebaseRegId() {
        try {
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
            regId = pref.getString("regId", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            super.onPause();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //FCM ----------------

    public void profilepicUploadAction(View view) {



        startActivity(new Intent(EditProfileActivity.this, UploadProfilePic.class));
        finish();
    }


    @Override
    protected void onStart() {
        super.onStart();

        // mAuthx.addAuthStateListener(mAuthStateListener);
//        if (mUserRef != null) {
//            mUserRef.addValueEventListener(mUserValueEventListener);
//        }


    }

    @Override
    protected void onStop() {
        super.onStop();
//        if (mAuthStateListener != null)
//            mAuthx.removeAuthStateListener(mAuthStateListener);
//        if (mUserRef != null)
//            mUserRef.removeEventListener(mUserValueEventListener);
    }
}
