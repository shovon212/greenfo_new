package layout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.muttasimfuad.greenfo.MainActivity;
import com.muttasimfuad.greenfo.R;
import com.muttasimfuad.greenfo.activity.MainActivity2;
import com.muttasimfuad.greenfo.app.Config;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;
import com.muttasimfuad.greenfo.oldutil.NotificationUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class SignupActivity extends AppCompatActivity {
    // FCM Cloud Msg
    private static final String TAGFCM = MainActivity2.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String regId;
// FCM Cloud Msg


    private ProgressBar progressBar;


    private static final String TAG = "PhoneAuth";

    private EditText phoneText;
    private EditText codeText;
    private Button verifyButton;
    private Button sendButton;
    private Button resendButton;
    private Button signInButton;
    private TextView statusText;

    private String phoneVerificationId;
    //   String rr;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            verificationCallbacks;
    private PhoneAuthProvider.ForceResendingToken resendToken;

    private FirebaseAuth fbAuth;

    //new
    private ValueEventListener mUserValueEventListener;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    ;
    FirebaseUser mFirebaseUser;
    private FirebaseAuth mAuthx;
    private DatabaseReference mUserRef;
    private String nam, vId, dep, secc, gend, fcm, shif, batc, ppURL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        //Get Firebase auth instance
        //fbAuth = FirebaseAuth.getInstance();
        //signInButton = (Button) findViewById(R.id.sign_in_button);
        statusText = (TextView) findViewById(R.id.statustextTV);
        resendButton = (Button) findViewById(R.id.resendCodeBTN);
        //btnSignUp = (Button) findViewById(R.id.sign_up_button);
        //inputEmail = (EditText) findViewById(R.id.email);
        //inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        phoneText = (EditText) findViewById(R.id.phoneNoET);
        codeText = (EditText) findViewById(R.id.giveCodeET);
        sendButton = (Button) findViewById(R.id.sendCodeBTN);
        verifyButton = (Button) findViewById(R.id.veryfiSignInBTN);


        verifyButton.setVisibility(View.GONE);
        resendButton.setVisibility(View.GONE);
        codeText.setVisibility(View.GONE);
        statusText.setText("To sign in use an active Phone number");

        fbAuth = FirebaseAuth.getInstance();


        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {

                    // startActivity(new Intent(MainActivity.this, SignupActivity.class));
                    //Toast.makeText(SignupActivity.this,"Login to enjoy all hidden features of this app!",Toast.LENGTH_LONG).show();
                    int c = 0;
                }
            }
        };


    }

    public void sendCodeNow(View view) {

        if (isNetworkConnected() == true) {
            String phoneNumber = "+88" + phoneText.getText().toString();






            if (TextUtils.isEmpty(phoneNumber) || phoneNumber.length() != 14) {
                Toast.makeText(getApplicationContext(), "Invalid Phone Number!", Toast.LENGTH_SHORT).show();
                return;
            } else {

                progressBar.setVisibility(View.VISIBLE);
                try {
                    setUpVerificatonCallbacks();

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            phoneNumber,        // Phone number to verify
                            60,                 // Timeout duration
                            TimeUnit.SECONDS,   // Unit of timeout
                            this,               // Activity (for callback binding)
                            verificationCallbacks);
                    statusText.setText("Sending a verification code to your phone");
                    verifyButton.setVisibility(View.VISIBLE);
                    resendButton.setVisibility(View.VISIBLE);
                    codeText.setVisibility(View.VISIBLE);
                    sendButton.setVisibility(View.GONE);
                } catch (Exception e) {
                    Toast.makeText(SignupActivity.this, "Poor Network Connection! Click Resend Code or try again later!", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }



        } else {
            statusText.setText("TURN ON INTERNET TO LOGIN !!");
        }
    }


    public void resendCodeNow(View view) {

        if (isNetworkConnected() == true) {
            String phoneNumber = "+88" + phoneText.getText().toString();

            if (TextUtils.isEmpty(phoneNumber) || phoneNumber.length() != 14) {
                Toast.makeText(getApplicationContext(), "Invalid Phone Number!", Toast.LENGTH_SHORT).show();
                return;
            } else {

                progressBar.setVisibility(View.VISIBLE);

                try {
                    setUpVerificatonCallbacks();

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            phoneNumber,        // Phone number to verify
                            60,                 // Timeout duration
                            TimeUnit.SECONDS,   // Unit of timeout
                            this,               // Activity (for callback binding)
                            verificationCallbacks);
                    statusText.setText("Sending a verification code to your phone");
                    verifyButton.setVisibility(View.VISIBLE);
                    resendButton.setVisibility(View.VISIBLE);
                    codeText.setVisibility(View.VISIBLE);
                    sendButton.setVisibility(View.GONE);
                } catch (Exception e) {
                    Toast.makeText(SignupActivity.this, "Poor Network Connection! Click Resend Code or try again later!", Toast.LENGTH_SHORT).show();
                }
            }

        } else {
            statusText.setText("TURN ON INTERNET TO LOGIN !!");
        }
    }


    private void setUpVerificatonCallbacks() {

        verificationCallbacks =
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                    @Override
                    public void onVerificationCompleted(
                            PhoneAuthCredential credential) {


                        resendButton.setEnabled(false);
                        verifyButton.setEnabled(false);
                        codeText.setText("");
                        signInWithPhoneAuthCredential(credential);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {

                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            // Invalid request
                            Log.d(TAG, "Invalid credential: "
                                    + e.getLocalizedMessage());
                        } else if (e instanceof FirebaseTooManyRequestsException) {
                            // SMS quota exceeded
                            Log.d(TAG, "SMS Quota exceeded.");
                        }
                    }

                    @Override
                    public void onCodeSent(String verificationId,
                                           PhoneAuthProvider.ForceResendingToken token) {

                        phoneVerificationId = verificationId;
                        resendToken = token;

                        verifyButton.setEnabled(true);
                        sendButton.setEnabled(false);
                        resendButton.setEnabled(true);
                    }
                };
    }

    public void verifyCodeNow(View view) {
        String code = codeText.getText().toString();
        if (!TextUtils.isEmpty(code)) {

            try {
                PhoneAuthCredential credential =
                        PhoneAuthProvider.getCredential(phoneVerificationId, code);
                signInWithPhoneAuthCredential(credential);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(SignupActivity.this, "Verification code is not invalid or Poor Network Connection!", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(SignupActivity.this, "Enter 6 digit code in the box first !", Toast.LENGTH_SHORT).show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        fbAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            codeText.setText("");
                            statusText.setText("Signing In Process..");
                            resendButton.setEnabled(false);
                            verifyButton.setEnabled(false);


                            FirebaseUser user = task.getResult().getUser();

                            fbAuth = FirebaseAuth.getInstance();

                            final String CurrentUserPhoneNo = user.getPhoneNumber();


                            // FCM _____________________________

//                                        try {
//                                            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//                                                @Override
//                                                public void onReceive(Context context, Intent intent) {
//
//                                                    // checking for type intent filter
//                                                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
//                                                        // gcm successfully registered
//                                                        // now subscribe to `global` topic to receive app wide notifications
//                                                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
//
//                                                        displayFirebaseRegId();
//
//                                                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
//                                                        // new push notification is received
//
//                                                        //                                                    String message = intent.getStringExtra("message");
//                                                        //
//                                                        //                                                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
//                                                        //
//                                                        //                                                    txtMessage.setText(message);
//                                                    }
//                                                }
//                                            };
//
//                                            displayFirebaseRegId();
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//
//
//                                        //FCM END
//
//                                        mAuthx=FirebaseAuth.getInstance();
//                                        mFirebaseUser = mAuthx.getCurrentUser();
//
//                                        if (mFirebaseUser != null) {
//                                            // mUserRef = FirebaseUtilsForUserInfo.getUserDpRef();
//                                            //mUserRef = FirebaseUtilsForUserInfo.getUserPhnRef(mFirebaseUser.getPhoneNumber());
//                                            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
//                                        }
//
//
//                                        mUserValueEventListener = new ValueEventListener() {
//                                            @Override
//                                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                                if (dataSnapshot.getValue() != null) {
//
//                                                    AppUser userR = dataSnapshot.getValue(AppUser.class);
//                                                    if(userR.getPropicurl() == null){
//                                                        ppURL= "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
//                                                    }else ppURL= userR.getPropicurl();
//                                                    nam= userR.getUsername();
//                                                    dep= userR.getDepartment();
//                                                    batc=userR.getBatch();
//                                                    vId=userR.getId();
//                                                    shif=userR.getShift();
//                                                    secc=userR.getSec();
//                                                    gend=userR.getGender();
//                                                    fcm=regId;
//
//                                                }
//
//                                            }
//
//                                            @Override
//                                            public void onCancelled(DatabaseError databaseError) {
//
//                                            }
//                                        };


                            //new FCM end


                            DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

                            DatabaseReference queryIfUserExistRef = rootRef.child("user_ids");


                            queryIfUserExistRef.addListenerForSingleValueEvent(new ValueEventListener() {

                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    if (snapshot.hasChild(CurrentUserPhoneNo)) {
                                        Toast.makeText(SignupActivity.this, "Welcome Back!", Toast.LENGTH_SHORT).show();
                                        // FCM _____________________________

//                                        try {
//                                            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//                                                @Override
//                                                public void onReceive(Context context, Intent intent) {
//
//                                                    // checking for type intent filter
//                                                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
//                                                        // gcm successfully registered
//                                                        // now subscribe to `global` topic to receive app wide notifications
//                                                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
//
//                                                        displayFirebaseRegId();
//
//                                                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
//                                                        // new push notification is received
//
//                                                        //                                                    String message = intent.getStringExtra("message");
//                                                        //
//                                                        //                                                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
//                                                        //
//                                                        //                                                    txtMessage.setText(message);
//                                                    }
//                                                }
//                                            };
//
//                                            displayFirebaseRegId();
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//
//
//                                        //FCM END
//
//                                        mAuthx=FirebaseAuth.getInstance();
//                                        mFirebaseUser = mAuthx.getCurrentUser();
//
//                                        if (mFirebaseUser != null) {
//                                            // mUserRef = FirebaseUtilsForUserInfo.getUserDpRef();
//                                            //mUserRef = FirebaseUtilsForUserInfo.getUserPhnRef(mFirebaseUser.getPhoneNumber());
//                                            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
//                                        }
//
//
//                                        mUserValueEventListener = new ValueEventListener() {
//                                            @Override
//                                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                                if (dataSnapshot.getValue() != null) {
//
//                                                    AppUser userR = dataSnapshot.getValue(AppUser.class);
//                                                    if(userR.getPropicurl() == null){
//                                                        ppURL= "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
//                                                    }else ppURL= userR.getPropicurl();
//                                                    nam= userR.getUsername();
//                                                    dep= userR.getDepartment();
//                                                    batc=userR.getBatch();
//                                                    vId=userR.getId();
//                                                    shif=userR.getShift();
//                                                    secc=userR.getSec();
//                                                    gend=userR.getGender();
//                                                    fcm=regId;
//
//                                                }
//
//                                            }
//
//                                            @Override
//                                            public void onCancelled(DatabaseError databaseError) {
//
//                                            }
//                                        };
//
//                                        AppUser appUser = new AppUser();
//                                        //FirebaseUtilsForUserInfo.getUserDpRef().setValue(profilePicUploadInfo1.getPropicurl());
//                                        appUser.setPropicurl(ppURL);
//                                        appUser.setFCM_ID(fcm);
//                                        appUser.setUsername(nam);
//                                        appUser.setDepartment(dep);
//                                        appUser.setBatch(batc);
//                                        appUser.setShift(shif);
//                                        appUser.setSec(secc);
//                                        appUser.setGender(gend);
//                                        appUser.setId(vId);
//
//                                        FirebaseUtilsForUserInfo.getUserinfoRef()
//                                                .setValue(appUser, new DatabaseReference.CompletionListener() {
//                                                    @Override
//                                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                                        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//                                                        finish();
//                                                    }
//                                                });


                                        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Toast.makeText(SignupActivity.this, "Thanks for joining GreenFo community!", Toast.LENGTH_SHORT).show();


                                        // FCM _____________________________

                                        try {
                                            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                                                @Override
                                                public void onReceive(Context context, Intent intent) {

                                                    // checking for type intent filter
                                                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                                                        // gcm successfully registered
                                                        // now subscribe to `global` topic to receive app wide notifications
                                                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                                                        displayFirebaseRegId();

                                                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                                                        // new push notification is received

                                                        //                                                    String message = intent.getStringExtra("message");
                                                        //
                                                        //                                                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                                                        //
                                                        //                                                    txtMessage.setText(message);
                                                    }
                                                }
                                            };

                                            displayFirebaseRegId();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        //FCM _______________________________


                                        DatabaseReference user_ifo = FirebaseDatabase.getInstance().getReference("user_info");
                                        DatabaseReference currentUserRefNew = user_ifo.child(CurrentUserPhoneNo);
                                        currentUserRefNew.setValue("courses");
                                        DatabaseReference courseRef = currentUserRefNew.child("courses");

                                        courseRef.child("Friday").setValue("null");
                                        courseRef.child("Saturday").setValue("null");
                                        courseRef.child("Sunday").setValue("null");
                                        courseRef.child("Monday").setValue("null");
                                        courseRef.child("Tuesday").setValue("null");
                                        courseRef.child("Wednesday").setValue("null");
                                        courseRef.child("Thursday").setValue("null");
                                        courseRef.child("CheckUser").setValue("fuad");


//                                        currentUserRefNew.child("personal_info").setValue("0");
//                                        DatabaseReference personalInfoRef = currentUserRefNew.child("personal_info");
//                                        personalInfoRef.child("username").setValue("null");
//                                        personalInfoRef.child("gender").setValue("null");
//                                        personalInfoRef.child("id").setValue("null");
//                                        personalInfoRef.child("batch").setValue("null");
//                                        personalInfoRef.child("department").setValue("null");
//                                        personalInfoRef.child("shift").setValue("null");


                                        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                                        rootRef.child("user_ids").child(CurrentUserPhoneNo).setValue("true");


                                        //new


                                        AppUser appUser = new AppUser();


                                        //      ProfilePicUploadInfo profilePicUploadInfo = new ProfilePicUploadInfo();
                                        //profilePicUploadInfo.setPropicurl(null);
                                        //          personalInfoRef.child("FCM_ID").setValue(regId);

                                        //FirebaseUtilsForUserInfo.getUserDpRef().setValue(null);
                                        appUser.setBatch("null");
                                        appUser.setDepartment("null");
                                        appUser.setGender("null");
                                        appUser.setId("Edit profile to set your ID");
                                        appUser.setShift("null");
                                        appUser.setUsername("Edit profile to set your name");
                                        appUser.setSec("A");
                                        appUser.setBlood_group("Unknown");
                                        appUser.setDonate_blood("NO");
                                        appUser.setPropicurl("https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3");
                                        //FCM

                                        if (!TextUtils.isEmpty(regId)) {
                                            // personalInfoRef.child("FCM_ID").setValue(regId);
//                                                FcmIDInfo fcmIDInfo = new FcmIDInfo(regId);
//                                                FirebaseUtilsForUserInfo.getUserFcmRef().setValue(fcmIDInfo);
                                            appUser.setFCM_ID(regId);


                                        } else {
                                            //   personalInfoRef.child("FCM_ID").setValue("Firebase Reg Id is not received yet!");


//                                                FcmIDInfo fcmIDInfo = new FcmIDInfo("Firebase Reg Id is not received yet!");
//                                                FirebaseUtilsForUserInfo.getUserFcmRef().setValue(fcmIDInfo);
                                            appUser.setFCM_ID("Could not received");
                                        }


                                        //FCM


                                        FirebaseUtilsForUserInfo.getUserinfoRef()
                                                .setValue(appUser, new DatabaseReference.CompletionListener() {
                                                    @Override
                                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                                                        finish();
                                                    }
                                                });


                                        //new

                                        Intent intent = new Intent(SignupActivity.this, EditProfileActivity.class);
                                        startActivity(intent);
                                        finish();

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                        } else {
                            if (task.getException() instanceof
                                    FirebaseAuthInvalidCredentialsException) {
                                statusText.setText("Verification code is not invalid !");
                                progressBar.setVisibility(View.GONE);
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }


    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);


        // FCM

        try {
            // register GCM registration complete receiver
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.REGISTRATION_COMPLETE));

            // register new push message receiver
            // by doing this, the activity will be notified each time a new message arrives
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.PUSH_NOTIFICATION));

            // clear the notification area when the app is opened
            NotificationUtils.clearNotifications(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //FCM
    }

    //CHECK IF NETWORK IS CONNECTED OR NOT
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    //CHECK IF  INTERNET IS AVAILABLE
    public static boolean isInternetAvailable() {
        String host = "www.google.com";
        int port = 80;
        Socket socket = new Socket();

        try {
            socket.connect(new InetSocketAddress(host, port), 2000);
            socket.close();
            return true;
        } catch (IOException e) {
            try {
                socket.close();
            } catch (IOException es) {
            }
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SignupActivity.this, MainActivity.class));
        finish();
    }

    // FCM -----
    private void displayFirebaseRegId() {
        try {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                // Log.w(TAG, "getInstanceId failed", task.getException());

                                return;
                            }

                            // Get new Instance ID token
                            regId = task.getResult().getToken();
                            Log.i("fcm token in signup: ",regId);
                            storeRegIdInPref(regId);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            regId = "Could not received";
        }
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }


    @Override
    protected void onPause() {
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            super.onPause();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //FCM ----------------


    @Override
    protected void onStart() {
        super.onStart();
        //mAuthx.addAuthStateListener(mAuthStateListener);
        if (mUserRef != null) {
            mUserRef.addValueEventListener(mUserValueEventListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//        if (mAuthStateListener != null)
//            mAuthx.removeAuthStateListener(mAuthStateListener);
//        if (mUserRef != null)
//            mUserRef.removeEventListener(mUserValueEventListener);
    }



}
