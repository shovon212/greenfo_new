package com.muttasimfuad.greenfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

public class EditClassActivity extends AppCompatActivity {

    GridLayout mainGrid;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_class);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainGrid = (GridLayout) findViewById(R.id.mainGrid);

        setSingleEvent(mainGrid);
    }


    private void setSingleEvent(GridLayout mainGrid) {

        //Loop all child item of Main Grid
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(finalI!=7) {
                        Intent intent = new Intent(EditClassActivity.this, EditSpecificDaysClassActivity.class);
                        intent.putExtra("info", "x" + finalI);
                        startActivity(intent);
                    }
                    else {


                        if (isNetworkConnected()) {


                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            //Yes button clicked

                                            try {
                                                mAuth = FirebaseAuth.getInstance();
                                                if (mAuth.getCurrentUser() != null) {
                                                    FirebaseUser user = mAuth.getCurrentUser();
                                                    String MyPhoneNo=user.getPhoneNumber();
                                                    String DeletedPhoneNoRef= "+8800000000000";
                                                    DatabaseReference rootpath = FirebaseDatabase.getInstance().getReference("user_info");
                                                    DatabaseReference fromPath = rootpath.child(DeletedPhoneNoRef).child("courses");
                                                    DatabaseReference toPath = rootpath.child(MyPhoneNo).child("courses");
                                                    deleteAllClasses(fromPath,toPath);

                                                } else {
                                                    Toast.makeText(EditClassActivity.this, "You must have to login to perform this!!", Toast.LENGTH_LONG).show();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Toast.makeText(EditClassActivity.this, "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
                                            }


                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked
                                            break;
                                    }
                                }
                            };

                           // View view = new View(EditClassActivity.this);

                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
                            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();


                        } else {
                            Toast.makeText(EditClassActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }


    private void deleteAllClasses(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                        if (firebaseError != null) {
                            Toast.makeText(EditClassActivity.this, "Delete failed!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(EditClassActivity.this, "Successfully Deleted All Course!", Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(EditClassActivity.this, MainActivity.class));
        finish();
    }
}
