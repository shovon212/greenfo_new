package com.muttasimfuad.greenfo.models;

/**
 * Created by Fuad on 05-Feb-18.
 */

public class FcmIDInfo {

    public String FCM_ID;

    public String getFCM_ID() {
        return FCM_ID;
    }

    public void setFCM_ID(String FCM_ID) {
        this.FCM_ID = FCM_ID;
    }

    public FcmIDInfo(String FCM_ID) {
        this.FCM_ID = FCM_ID;
    }
}
