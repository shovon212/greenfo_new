package com.muttasimfuad.greenfo.models;

import java.io.Serializable;

/**
 * Created by brad on 2017/02/05.
 */

public class Comment implements Serializable {
    private String user;
    private String commentId;
    private long timeCreated;
    private String comment;
    private String commenterphotourl;

    public Comment() {
    }

    public Comment(String user, String commentId, long timeCreated, String comment, String commenterphotourl) {
        this.user = user;
        this.commentId = commentId;
        this.timeCreated = timeCreated;
        this.comment = comment;
        this.commenterphotourl = commenterphotourl;
    }

    public String getCommenterphotourl() {
        return commenterphotourl;
    }

    public void setCommenterphotourl(String commenterphotourl) {
        this.commenterphotourl = commenterphotourl;
    }

    public String getUser() {

        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
