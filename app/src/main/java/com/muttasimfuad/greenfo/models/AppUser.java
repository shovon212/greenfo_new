package com.muttasimfuad.greenfo.models;

import java.io.Serializable;

/**
 * Created by Fuad on 04-Feb-18.
 */

public class AppUser implements Serializable {
    private String username;
    private String id;
    private String Propicurl;
    private String gender;
    private String department;
    private String batch;
    private String shift;
    private String FCM_ID;
    private String sec;
    private String blood_group;
    private String donate_blood;

    public AppUser() {
    }

    public AppUser(String username, String id, String propicurl, String gender, String department, String batch, String shift, String FCM_ID, String sec, String blood_group, String donate_blood) {
        this.username = username;
        this.id = id;
        Propicurl = propicurl;
        this.gender = gender;
        this.department = department;
        this.batch = batch;
        this.shift = shift;
        this.FCM_ID = FCM_ID;
        this.sec = sec;
        this.blood_group = blood_group;
        this.donate_blood = donate_blood;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropicurl() {
        return Propicurl;
    }

    public void setPropicurl(String propicurl) {
        Propicurl = propicurl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getFCM_ID() {
        return FCM_ID;
    }

    public void setFCM_ID(String FCM_ID) {
        this.FCM_ID = FCM_ID;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getDonate_blood() {
        return donate_blood;
    }

    public void setDonate_blood(String donate_blood) {
        this.donate_blood = donate_blood;
    }
}