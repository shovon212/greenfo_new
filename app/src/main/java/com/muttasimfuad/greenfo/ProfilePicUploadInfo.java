package com.muttasimfuad.greenfo;

import java.io.Serializable;

/**
 * Created by Fuad on 04-Feb-18.
 */

public class ProfilePicUploadInfo implements Serializable {

    private String Propicurl;

    public ProfilePicUploadInfo(){

    }

    public ProfilePicUploadInfo(String propicurl) {
        this.Propicurl = propicurl;
    }

    public String getPropicurl() {
        return Propicurl;
    }

    public void setPropicurl(String propicurl) {
        this.Propicurl = propicurl;
    }
}
