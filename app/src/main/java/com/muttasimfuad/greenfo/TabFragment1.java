package com.muttasimfuad.greenfo;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.models.User;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;
import com.muttasimfuad.greenfo.ui.dialogs.NoticeCreateDialog;
import com.muttasimfuad.greenfo.ui.dialogs.PostCreateDialog;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import Adapter.CustomAdapterNoticeList;
import Adapter.CustomAdapterTodayRoutine;
import ModalClass.NoticeInfoModal;
import ModalClass.RoutineInfo;
import layout.SignupActivity;

import static com.muttasimfuad.greenfo.utils.FirebaseUtils.getNoticeRefFinal;


/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment1 extends Fragment {
    //    ListView listView;
//    ArrayList<String> List=new ArrayList<>();
//    ArrayAdapter<String> adapter;
    ListView mListView, dvNoticeListView;
    ArrayList<RoutineInfo> arrayList;
    //    List<NoticeInfoModal> Notices;
    List<Post> Notices2;
    RoutineInfo routineInfo;
    NoticeInfoModal noticeInfoModal;
    Post post;
    CustomAdapterTodayRoutine adapter;
    CustomAdapterNoticeList noticeAdapter;

    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    ArrayList<String> dvNoticeList = new ArrayList<>();
    private DatabaseReference myRef, mUserRef;
    private String CurrentUserPhoneNo;
    TextView todayDateTIme;
    TextView totalclassTodayTV;
    TextView refreshNotice;
    String c;

    Button regBTN, addNoticeBtn;
    ImageView setupClassIMG;


    String ClassTime = " ";
    String CourseCode = " ";
    String courseName = " ";
    String RoomNo = " ";
    int dayNo = 0;
    String TodaysDay;
    Long totalClass;
    String[] dayList = {
            "Friday",
            "Saturday",
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday"
    };

    private ValueEventListener mUserValueEventListener;
    private String userFullBatch, pp, MyName;
    private FirebaseUser mFirebaseUser;


    public TabFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_fragment1, container, false);
        LinearLayout loginLayout = (LinearLayout) view.findViewById(R.id.homeLoginView);
        //LinearLayout afterLoginLayout = (LinearLayout) view.findViewById(R.id.AfterLoginHome);
        regBTN = view.findViewById(R.id.reg_button);
        addNoticeBtn = view.findViewById(R.id.addNoticeHomeTabBtnId);
        setupClassIMG = (ImageView) view.findViewById(R.id.setupClassIMG);
        dvNoticeListView = (ListView) view.findViewById(R.id.homepostbyAdminLV);
        refreshNotice = (TextView) view.findViewById(R.id.refreshNotice);


        refreshNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Setting up Main Fragment Container
                try {
                    HomeFragment homeFragment = new HomeFragment();
                    FragmentTransaction fragmentTransaction =
                            getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, homeFragment);
                    fragmentTransaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // End of setting Main Fragment Container

            }
        });


        mAuth = FirebaseAuth.getInstance();
        mFirebaseUser= mAuth.getCurrentUser();

        if (mAuth.getCurrentUser() == null) {

            loginLayout.setVisibility(View.VISIBLE);
            setupClassIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), "You have to login to Setup classes", Toast.LENGTH_LONG).show();
                }
            });

            regBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), SignupActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
            });





            // Trying to Show Developers notice to home NEW____


            Notices2 = new ArrayList<>();

            DatabaseReference noticeListRef = FirebaseUtils.getNoticeRefFinal();

            noticeListRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    //clearing the previous User list
                    //  Notices.clear();
                    Notices2.clear();

                    //getting all nodes
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        //getting User from firebase console
                        noticeInfoModal = postSnapshot.getValue(NoticeInfoModal.class);
                        post = postSnapshot.getValue(Post.class);
                        //adding User to the list
                        // Notices.add(noticeInfoModal);
                        Notices2.add(post);
                    }
                    //creating Userlist adapter
                    noticeAdapter = new CustomAdapterNoticeList(TabFragment1.this.getActivity(), Notices2);
                    //attaching adapter to the listview
                    dvNoticeListView.setAdapter(noticeAdapter);
                    noticeAdapter.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            DatabaseReference HomeNoticeRef = getNoticeRefFinal();
            HomeNoticeRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Long num = dataSnapshot.getChildrenCount();
                    if (num < 2) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 200;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();

                    } else if (num == 0) {
                        dvNoticeListView.setAdapter(null);
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 50;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();
                    } else if (num == 2) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 350;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();

                    } else if (num == 3) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 420;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();
                    } else if (num > 7) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 900;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }
        if (mAuth.getCurrentUser() != null) {


            //Activating setup Class Button


            setupClassIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), SetupClass.class));
                    getActivity().finish();
                }
            });

            //Setup Done


            loginLayout.setVisibility(View.GONE);
            todayDateTIme = view.findViewById(R.id.dateTodayTV);
            totalclassTodayTV = view.findViewById(R.id.idOftotalclassTodayTv);
            mListView = view.findViewById(R.id.classScheduleListView);

            arrayList = new ArrayList<>();
            //         Notices = new ArrayList<>();
            Notices2 = new ArrayList<>();


            mFirebaseDatabase = FirebaseDatabase.getInstance();
            myRef = mFirebaseDatabase.getReference();
            FirebaseUser user = mAuth.getCurrentUser();
            CurrentUserPhoneNo = user.getPhoneNumber();


            Calendar sCalendar = Calendar.getInstance();
            String dayName = sCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());


            if (dayName.equals("Fri")) {
                dayNo = 0;
            }
            if (dayName.equals("Sat")) {
                dayNo = 1;
            }
            if (dayName.equals("Sun")) {
                dayNo = 2;
            }
            if (dayName.equals("Mon")) {
                dayNo = 3;
            }
            if (dayName.equals("Tue")) {
                dayNo = 4;
            }
            if (dayName.equals("Wed")) {
                dayNo = 5;
            }
            if (dayName.equals("Thu")) {
                dayNo = 6;
            }

            todayDateTIme.setText(dayList[dayNo]);

            TodaysDay = dayList[dayNo];


            DatabaseReference user_info_Ref = myRef.child("user_info").child(CurrentUserPhoneNo).child("courses").child(TodaysDay);

            Query deleteQuery = user_info_Ref.orderByChild(TodaysDay);
            try {
                deleteQuery.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        showData(dataSnapshot);

                        try {
                            arrayList.add(routineInfo);
                            adapter = new CustomAdapterTodayRoutine(TabFragment1.this.getActivity(), arrayList);
                            mListView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.i("Item not found: ", "this item is not in the list");
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


            DatabaseReference totalclassRef = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(TodaysDay);
            totalclassRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Long num = dataSnapshot.getChildrenCount();
                    if (num < 2) {
                        c = "class";
                    } else {
                        c = "classes";
                    }
                    totalclassTodayTV.setText("You have " + num + " " + c + " today!");

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            DatabaseReference HomeClassRef = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(TodaysDay);
            HomeClassRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Long num = dataSnapshot.getChildrenCount();
                    if (num < 2) {
                        c = "class";
                        ViewGroup.LayoutParams params = mListView.getLayoutParams();
                        params.height = 200;
                        mListView.setLayoutParams(params);
                        mListView.requestLayout();

                    } else {
                        c = "classes";

                    }
                    if (num == 0) {
                        mListView.setAdapter(null);
                        ViewGroup.LayoutParams params = mListView.getLayoutParams();
                        params.height = 50;
                        mListView.setLayoutParams(params);
                        mListView.requestLayout();
                    } else if (num == 2) {
                        ViewGroup.LayoutParams params = mListView.getLayoutParams();
                        params.height = 350;
                        mListView.setLayoutParams(params);
                        mListView.requestLayout();

                    } else if (num > 2) {
                        ViewGroup.LayoutParams params = mListView.getLayoutParams();
                        params.height = 420;
                        mListView.setLayoutParams(params);
                        mListView.requestLayout();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            // Trying to Show Developers notice to home NEW____

            DatabaseReference noticeListRef = FirebaseUtils.getNoticeRefFinal();

            noticeListRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    //clearing the previous User list
                    //  Notices.clear();
                    Notices2.clear();

                    //getting all nodes
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        //getting User from firebase console
                        noticeInfoModal = postSnapshot.getValue(NoticeInfoModal.class);
                        post = postSnapshot.getValue(Post.class);
                        //adding User to the list
                        // Notices.add(noticeInfoModal);
                        Notices2.add(post);
                    }
                    //creating Userlist adapter
                    noticeAdapter = new CustomAdapterNoticeList(TabFragment1.this.getActivity(), Notices2);
                    //attaching adapter to the listview
                    dvNoticeListView.setAdapter(noticeAdapter);
                    noticeAdapter.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            DatabaseReference HomeNoticeRef = getNoticeRefFinal();
            HomeNoticeRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Long num = dataSnapshot.getChildrenCount();
                    if (num < 2) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 200;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();

                    } else if (num == 0) {
                        dvNoticeListView.setAdapter(null);
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 50;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();
                    } else if (num == 2) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 350;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();

                    } else if (num == 3) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 420;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();
                    } else if (num > 7) {
                        ViewGroup.LayoutParams params = dvNoticeListView.getLayoutParams();
                        params.height = 900;
                        dvNoticeListView.setLayoutParams(params);
                        dvNoticeListView.requestLayout();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            // Trying to Show Developers notice to home

//            try {
//
//                final ArrayAdapter<String> adapterForDvNotice = new ArrayAdapter<String>(TabFragment1.this.getActivity(), android.R.layout.simple_list_item_1, dvNoticeList);
//                dvNoticeListView.setAdapter(adapterForDvNotice);
//                Dref = FirebaseDatabase.getInstance().getReference().child("Powership").child("Dev").child("notifn_dv").child("home_tab_dv").child("h1");
//                Dref.addChildEventListener(new ChildEventListener() {
//                    @Override
//                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                        dvNoticeList.add(dataSnapshot.getValue(String.class));
//                        adapterForDvNotice.notifyDataSetChanged();
//                    }
//
//                    @Override
//                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                    }
//
//                    @Override
//                    public void onChildRemoved(DataSnapshot dataSnapshot) {
//                        dvNoticeList.remove(dataSnapshot.getValue(String.class));
//                        adapterForDvNotice.notifyDataSetChanged();
//                    }
//
//                    @Override
//                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                    }
//                });
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            addNoticeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    NoticeCreateDialog dialog = new NoticeCreateDialog();
                    dialog.show(getFragmentManager(), null);

                }
            });

            // DONE of Setting Up Developer's Post in HOME
            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();


            FirebaseUtils.getUserDetailsRef(FirebaseUtils.getCurrentUser().getPhoneNumber())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            AppUser user = dataSnapshot.getValue(AppUser.class);

                            try {
                                userFullBatch = user.getDepartment() + "_" + user.getBatch() + "_" + user.getShift() + "_" + user.getSec();
                            } catch (Exception e) {
                                e.printStackTrace();
                                userFullBatch="Unknown";
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

            mUserValueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {

                        AppUser appUser = dataSnapshot.getValue(AppUser.class);
                        if (appUser.getPropicurl() == null) {
                            pp = "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                        }
                        //else pp= "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                        else pp = appUser.getPropicurl();

                        MyName = appUser.getUsername();
                        User user = new User();
                        user.setUser(MyName);
                        user.setPhotoUrl(pp);
                        user.setUid(mFirebaseUser.getUid());
                        user.setEmail(mFirebaseUser.getPhoneNumber());
                        user.setUserFullBatch(userFullBatch);


                        FirebaseUtils.getUserRefNoticeBoard(mAuth.getCurrentUser().getPhoneNumber())
                                .setValue(user, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        mFirebaseUser = mAuth.getCurrentUser();
                                        //finish();
                                    }
                                });
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };


        } else {

            loginLayout.setVisibility(View.VISIBLE);

            addNoticeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(getActivity(), "You have to login to upload notice", Toast.LENGTH_LONG).show();

                }
            });
            //afterLoginLayout.setVisibility(View.GONE);

            // Trying to Show Developers notice to home

//            try {
//
//                final ArrayAdapter<String> adapterForDvNotice = new ArrayAdapter<String>(TabFragment1.this.getActivity(), android.R.layout.simple_list_item_1, dvNoticeList);
//                dvNoticeListView.setAdapter(adapterForDvNotice);
//                Dref = FirebaseDatabase.getInstance().getReference().child("Powership").child("Dev").child("notifn_dv").child("home_tab_dv").child("h1");
//                Dref.addChildEventListener(new ChildEventListener() {
//                    @Override
//                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                        dvNoticeList.add(dataSnapshot.getValue(String.class));
//                        adapterForDvNotice.notifyDataSetChanged();
//                    }
//
//                    @Override
//                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                    }
//
//                    @Override
//                    public void onChildRemoved(DataSnapshot dataSnapshot) {
//                        dvNoticeList.remove(dataSnapshot.getValue(String.class));
//                        adapterForDvNotice.notifyDataSetChanged();
//                    }
//
//                    @Override
//                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                    }
//                });
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            // DONE of Setting Up Developer's Post in HOME

        }


        return view;
    }


    private void showData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            routineInfo = new RoutineInfo();
//            routineInfo.setName("Hi"); //set the name
//            routineInfo.setRelationship(ds.child(CurrentUserPhoneNo).getValue(RoutineInfo.class).getRelationship()); //set the email
//            routineInfo.setNumber(ds.child("CSE205").getValue(String.class)); //set the phone_num
//            routineInfo.setToday(ds.child("CSE205").getValue(String.class)); //set the phone_num
//            Relation = ds.child("name").getKey();
            ClassTime = String.valueOf(value.get("time"));
            CourseCode = String.valueOf(value.get("course_code"));
            courseName = String.valueOf(value.get("name"));
            RoomNo = String.valueOf(value.get("room"));
            routineInfo = new RoutineInfo(ClassTime, CourseCode, courseName, RoomNo);

        }
    }

    private void showNoticeData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            noticeInfoModal = new NoticeInfoModal();
            noticeInfoModal = new NoticeInfoModal(String.valueOf(value.get("postTitle")), String.valueOf(value.get("postImageUrl")), String.valueOf(value.get("postText")), Long.getLong("timeCreated"), Long.getLong("numComments"));

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //mAuthx.addAuthStateListener(mAuthStateListener);
        if (mUserRef != null) {
            mUserRef.addValueEventListener(mUserValueEventListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mUserRef != null)
            mUserRef.removeEventListener(mUserValueEventListener);
    }


}
