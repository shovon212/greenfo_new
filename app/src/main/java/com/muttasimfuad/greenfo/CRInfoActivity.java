package com.muttasimfuad.greenfo;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.Map;

public class CRInfoActivity extends AppCompatActivity {

    EditText CrName,CrPhone,CrPass;
    String OldName,OldPhone,OldPassword,CRdept,CRbatch,CRshift;
    TextView displayboxCRInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crinfo);
        CrName = (EditText) findViewById(R.id.nameofcrET);
        CrPhone= (EditText) findViewById(R.id.phonenofcrET);
        CrPass = (EditText) findViewById(R.id.passofcrET);

        displayboxCRInfo= (TextView) findViewById(R.id.crinfoDisplayTV);

        Intent cr = getIntent();

        CRdept = cr.getStringExtra("crdept");
        CRbatch = cr.getStringExtra("crbatch");
        CRshift = cr.getStringExtra("crshift");

        DatabaseReference crpassPath= FirebaseDatabase.getInstance().getReference().child("batches").child(CRdept).child(CRbatch).child(CRshift).child("cr_info");
        Query deleteQuery = crpassPath.orderByChild("cr_info");
        try {
            deleteQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    showData(dataSnapshot);

                    CrName.setText(OldName);
                    CrPhone.setText(OldPhone);
                    CrPass.setText(OldPassword);



                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i("Item not found: ", "this item is not in the list");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    public void CrInfoSaveAction(View view) {

        if (isNetworkConnected() == true) {

            try {
                String OldNameOfCr = CrName.getText().toString().trim();
                String OldCrPhoneNo = CrPhone.getText().toString().trim();
                String OldCrPassword= CrPass.getText().toString().trim();
                if(!TextUtils.isEmpty(OldNameOfCr) && !TextUtils.isEmpty(OldCrPhoneNo) && !TextUtils.isEmpty(OldCrPassword)){

                    DatabaseReference setNewCrInfoPath= FirebaseDatabase.getInstance().getReference().child("batches").child(CRdept).child(CRbatch).child(CRshift).child("cr_info").child("cr1");

                    try {
                        setNewCrInfoPath.child("name").setValue(OldNameOfCr);
                        setNewCrInfoPath.child("phone").setValue(OldCrPhoneNo);
                        setNewCrInfoPath.child("pass").setValue(OldCrPassword);
                        displayboxCRInfo.setText("Successfully Saved ! Login Again!");
                        CrName.setText("");
                        CrPhone.setText("");
                        CrPass.setText("");
                        startActivity(new Intent(CRInfoActivity.this, CRnDevLoginActivity.class));
                        Toast.makeText(CRInfoActivity.this,"Successfully Changed! Login Again",Toast.LENGTH_LONG).show();
                        finish();


                    } catch (Exception e) {
                        displayboxCRInfo.setText("Not Saved!");
                    }

                }else {
                    Toast.makeText(CRInfoActivity.this,"Fill Up all information!",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(CRInfoActivity.this, "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(CRInfoActivity.this,"Check Your Internet Connection!",Toast.LENGTH_SHORT).show();
        }
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void showData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            OldName = String.valueOf(value.get("name"));
            OldPhone = String.valueOf(value.get("phone"));
            OldPassword = String.valueOf(value.get("pass"));
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(CRInfoActivity.this, CRnDevLoginActivity.class));
        finish();
    }

}
