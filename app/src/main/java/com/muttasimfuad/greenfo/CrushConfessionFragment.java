package com.muttasimfuad.greenfo;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.models.User;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;
import com.muttasimfuad.greenfo.ui.activities.PostActivityCrushConfession;
import com.muttasimfuad.greenfo.ui.dialogs.PostCreateDialogCrushConfession;
import com.muttasimfuad.greenfo.utils.Constants;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;

import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CrushConfessionFragment extends Fragment {

    private View mRootVIew;
    private FirebaseRecyclerAdapter<Post, PostHolder> mPostAdapter;
    private RecyclerView mPostRecyclerView;

    //new
    private ValueEventListener mUserValueEventListener;
    private DatabaseReference mUserRef;

    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseAuth mAuthx;
    private FirebaseUser mFirebaseUser;
    String MyName, pp;
    private String userFullBatch,Power;

    private long mLastClickTime = 0;
    private FragmentActivity myContext;


    public CrushConfessionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootVIew = inflater.inflate(R.layout.fragment_help_zone, container, false);

        try {
            getActivity().setTitle("GUB Crush Confessions");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //new
        mAuthx = FirebaseAuth.getInstance();
        mFirebaseUser = mAuthx.getCurrentUser();
        if (mFirebaseUser != null) {
            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
        }


        FirebaseUtils.getUserDetailsRef(FirebaseUtils.getCurrentUser().getPhoneNumber())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        AppUser user = dataSnapshot.getValue(AppUser.class);

                        userFullBatch = user.getDepartment() + " Department";
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        mUserValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {

                    AppUser appUser = dataSnapshot.getValue(AppUser.class);
                    if (appUser.getPropicurl() == null) {
                        pp = "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                    } else
                        pp = "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                    //}else pp= appUser.getPropicurl();

                    MyName = appUser.getUsername();
                    User user = new User();
                    user.setUser(MyName);
                    user.setPhotoUrl(pp);
                    user.setUid(mFirebaseUser.getUid());
                    user.setEmail(mFirebaseUser.getPhoneNumber());
                    user.setUserFullBatch(userFullBatch);


                    FirebaseUtils.getUserRefCrushConfession(mAuthx.getCurrentUser().getPhoneNumber())
                            .setValue(user, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    mFirebaseUser = mAuthx.getCurrentUser();
                                    //finish();
                                }
                            });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        FloatingActionButton fab = (FloatingActionButton) mRootVIew.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostCreateDialogCrushConfession dialog = new PostCreateDialogCrushConfession();
                dialog.show(getFragmentManager(), null);
            }
        });
        init();
        return mRootVIew;
    }

    private void init() {
        mPostRecyclerView = (RecyclerView) mRootVIew.findViewById(R.id.recyclerview_post);
        mPostRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        setupAdapter();
        mPostRecyclerView.setAdapter(mPostAdapter);
    }

    private void setupAdapter() {
        mPostAdapter = new FirebaseRecyclerAdapter<Post, PostHolder>(
                Post.class,
                R.layout.row_post,
                PostHolder.class,
                FirebaseUtils.getPostRefFinalCrushConfession()
        ) {
            @Override
            protected void populateViewHolder(final PostHolder viewHolder, final Post model, int position) {
                viewHolder.setNumCOmments(String.valueOf(model.getNumComments()));
                viewHolder.setNumLikes(String.valueOf(model.getNumLikes()));
                viewHolder.setTIme(DateUtils.getRelativeTimeSpanString(model.getTimeCreated()));
                viewHolder.setUsername(model.getUser().getUserFullBatch().replace("_", " "));
                viewHolder.setPostText(model.getPostText());

                Glide.with(getActivity())
                        .load(model.getUser().getPhotoUrl())
                        .into(viewHolder.postOwnerDisplayImageView);

                if (model.getPostImageUrl() != null) {
                    viewHolder.postDisplayImageVIew.setVisibility(View.VISIBLE);
                    StorageReference storageReference = FirebaseStorage.getInstance()
                            .getReference(model.getPostImageUrl());
                    Glide.with(getActivity())
                            .using(new FirebaseImageLoader())
                            .load(storageReference)
                            .into(viewHolder.postDisplayImageVIew);
                } else {
                    viewHolder.postDisplayImageVIew.setImageBitmap(null);
                    viewHolder.postDisplayImageVIew.setVisibility(View.GONE);
                }
                viewHolder.postLikeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Preventing multiple clicks, using threshold of 1 second
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        // Handle button clicks
                        onLikeClick(model.getPostId());
                    }
                });

                // POST DELETE ADMIN START

                viewHolder.deleteLL.setVisibility(View.GONE);

                try {

                    DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("postdelete_power").child(FirebaseUtils.getCurrentUser().getPhoneNumber());
                    Query ModeratorQuery = crpassPathModerator.orderByChild(FirebaseUtils.getCurrentUser().getPhoneNumber());

                    ModeratorQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            ModeratorData(dataSnapshot);

                            if (Power.equals("yes")) {
                                viewHolder.deleteLL.setVisibility(View.VISIBLE);
                            }  else {
                                viewHolder.deleteLL.setVisibility(View.GONE);

                            }


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }

                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                }


                // END of delete admin code


                viewHolder.deleteLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onDeleteClick(model.getPostId());
                    }
                });

                viewHolder.postCommentLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), PostActivityCrushConfession.class);
                        intent.putExtra(Constants.EXTRA_POST, model);
                        startActivity(intent);
                    }
                });
                viewHolder.postDisplayImageVIew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), PostActivityCrushConfession.class);
                        intent.putExtra(Constants.EXTRA_POST, model);
                        startActivity(intent);
                    }
                });
                viewHolder.postTextTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), PostActivityCrushConfession.class);
                        intent.putExtra(Constants.EXTRA_POST, model);
                        startActivity(intent);
                    }
                });
            }
        };
    }

    //During the tutorial I think I messed up this code. Make sure your's aligns to this, or just
    //check out the github code
    private void onLikeClick(final String postId) {
        FirebaseUtils.getPostLikedRefCrushConfession(postId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            //AppUser liked
                            FirebaseUtils.getPostRefFinalCrushConfession()
                                    .child(postId)
                                    .child(Constants.NUM_LIKES_KEY)
                                    .runTransaction(new Transaction.Handler() {
                                        @Override
                                        public Transaction.Result doTransaction(MutableData mutableData) {
                                            long num = (long) mutableData.getValue();
                                            mutableData.setValue(num - 1);
                                            return Transaction.success(mutableData);
                                        }

                                        @Override
                                        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                            FirebaseUtils.getPostLikedRefCrushConfession(postId)
                                                    .setValue(null);
                                        }
                                    });
                        } else {
                            FirebaseUtils.getPostRefFinalCrushConfession()
                                    .child(postId)
                                    .child(Constants.NUM_LIKES_KEY)
                                    .runTransaction(new Transaction.Handler() {
                                        @Override
                                        public Transaction.Result doTransaction(MutableData mutableData) {
                                            long num = (long) mutableData.getValue();
                                            mutableData.setValue(num + 1);
                                            return Transaction.success(mutableData);
                                        }

                                        @Override
                                        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                            FirebaseUtils.getPostLikedRefCrushConfession(postId)
                                                    .setValue(true);
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void onDeleteClick(final String postId) {
        if (isNetworkConnected()) {


            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked

                            try {

                                DatabaseReference rootpath_of_postContent = FirebaseDatabase.getInstance().getReference("crush_confession");
                                DatabaseReference postContecnt_fromPath = rootpath_of_postContent.child("posts").child(postId);
                                DatabaseReference CommentContecnt_fromPath = rootpath_of_postContent.child("comments").child(postId);


                                postContecnt_fromPath.removeValue();
                                CommentContecnt_fromPath.removeValue();

                                try {
                                    StorageReference photoRef = FirebaseUtils.getImageSRefCrushConfession().child(postId);
                                    photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                            Toast.makeText(getContext(),"Uploaded picture deleted!",Toast.LENGTH_LONG).show();
                                            // File deleted successfully
                                            // Log.d(TAG, "onSuccess: deleted file");
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {
                                            // Uh-oh, an error occurred!
                                            // Log.d(TAG, "onFailure: did not delete file");
                                            Toast.makeText(getContext(),"No Pic Found in Post OR It May Failed to Delete The Picture!",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
                            }


                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }
    }

    public static class PostHolder extends RecyclerView.ViewHolder {
        ImageView postOwnerDisplayImageView;
        TextView postOwnerUsernameTextView;
        TextView postTimeCreatedTextView;
        ImageView postDisplayImageVIew;
        TextView postTextTextView;
        LinearLayout postLikeLayout;
        LinearLayout postCommentLayout;
        TextView postNumLikesTextView;
        TextView postNumCommentsTextView;

        LinearLayout deleteLL;


        public PostHolder(View itemView) {
            super(itemView);
            postOwnerDisplayImageView = (ImageView) itemView.findViewById(R.id.iv_post_owner_display);
            postOwnerUsernameTextView = (TextView) itemView.findViewById(R.id.tv_post_username);
            postTimeCreatedTextView = (TextView) itemView.findViewById(R.id.tv_time);
            postDisplayImageVIew = (ImageView) itemView.findViewById(R.id.iv_post_display);
            postLikeLayout = (LinearLayout) itemView.findViewById(R.id.like_layout);
            postCommentLayout = (LinearLayout) itemView.findViewById(R.id.comment_layout);
            postNumLikesTextView = (TextView) itemView.findViewById(R.id.tv_likes);
            postNumCommentsTextView = (TextView) itemView.findViewById(R.id.tv_comments);
            postTextTextView = (TextView) itemView.findViewById(R.id.tv_post_text);

            deleteLL = (LinearLayout) itemView.findViewById(R.id.delete_layout);


        }

        public void setUsername(String username) {
            postOwnerUsernameTextView.setText(username);
        }

        public void setTIme(CharSequence time) {
            postTimeCreatedTextView.setText(time);
        }

        public void setNumLikes(String numLikes) {
            postNumLikesTextView.setText(numLikes);
        }

        public void setNumCOmments(String numComments) {
            postNumCommentsTextView.setText(numComments);
        }

        public void setPostText(String text) {
            postTextTextView.setText(text);
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        //mAuthx.addAuthStateListener(mAuthStateListener);
        if (mUserRef != null) {
            mUserRef.addValueEventListener(mUserValueEventListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthStateListener != null)
            mAuthx.removeAuthStateListener(mAuthStateListener);
        if (mUserRef != null)
            mUserRef.removeEventListener(mUserValueEventListener);
    }

    //for  delete post admin

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    private void ModeratorData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            Power = String.valueOf(value.get("power"));
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    HomeFragment helpZoneFragmentPendingPost = new HomeFragment();


                    FragmentTransaction fragmentTransaction =
                            myContext.getSupportFragmentManager().beginTransaction();

                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                    fragmentTransaction.commit();





                    return true;

                }

                return false;
            }
        });

        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

}
