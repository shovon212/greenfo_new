package com.muttasimfuad.greenfo;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

public class CrPanelActivity extends AppCompatActivity {

    TextView welcomeMsgforCR, crName, crPhone, crPass;
    String CrDept, CrBatch, CrShift,CrSec, OldName, OldPhone, OldPassword, Message;
    ProgressBar weekPB, monthPB;

    Calendar calander;
    SimpleDateFormat simpledateformat;
    String Date;

    EditText postBoxForWeekET, postBoxForMonthET, NoticeNoWeekET,NoticeNoMonthET;
    String postTextofWeek, postTextofMonth;

    TextView weekResult, monthResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cr_panel);

        welcomeMsgforCR = (TextView) findViewById(R.id.welcomeMsgTV);

        Intent startingIntent = getIntent();
        Message = startingIntent.getStringExtra("crtotalbatch");
        CrDept = startingIntent.getStringExtra("crdept");
        CrBatch = startingIntent.getStringExtra("crbatch");
        CrShift = startingIntent.getStringExtra("crshift");
        CrSec = startingIntent.getStringExtra("crsec");


        crName = (TextView) findViewById(R.id.crnameInCRPanelTV);
        crPhone = (TextView) findViewById(R.id.crPhoneInCRPanelTV);
        crPass = (TextView) findViewById(R.id.crPassInCRPanelTV);

        weekPB = (ProgressBar) findViewById(R.id.progressBarWEEK);
        monthPB = (ProgressBar) findViewById(R.id.progressBarMONTH);

        postBoxForWeekET = (EditText) findViewById(R.id.ETpostToWeekTab);
        postBoxForMonthET = (EditText) findViewById(R.id.ETpostToMonthTab);
        NoticeNoWeekET = (EditText) findViewById(R.id.ETnoticeNoWeek);
        NoticeNoMonthET = (EditText) findViewById(R.id.ETnoticeNoMonth);

        weekResult = (TextView) findViewById(R.id.TVweekResult);
        monthResult = (TextView) findViewById(R.id.TVmonthResult);


        weekPB.setVisibility(View.INVISIBLE);
        monthPB.setVisibility(View.INVISIBLE);

        DatabaseReference crpassPath = FirebaseDatabase.getInstance().getReference().child("batches").child(CrDept).child(CrBatch).child(CrShift).child("cr_info").child(CrSec);
        Query deleteQuery = crpassPath.orderByChild(CrSec);
        try {
            deleteQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    showData(dataSnapshot);

                    welcomeMsgforCR.setText("Welcome " + OldName + "! Hope you will lead " + Message + " Batch perfectly!");
                    crName.setText("Your Name: " + OldName);
                    crPhone.setText("Your Phone: " + OldPhone);
                    crPass.setText("Your Password: " + OldPassword);

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i("Item not found: ", "this item is not in the list");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }




    }

    public void editCRinfoAction(View view) {
        if (isNetworkConnected() == true) {
            Intent intent = new Intent(CrPanelActivity.this, CRInfoActivity.class);
            intent.putExtra("crdept", CrDept);
            intent.putExtra("crbatch", CrBatch);
            intent.putExtra("crshift", CrShift);
            startActivity(intent);
            finish();
        }else {
            Toast.makeText(CrPanelActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            OldName = String.valueOf(value.get("name"));
            OldPhone = String.valueOf(value.get("phone"));
            OldPassword = String.valueOf(value.get("pass"));
        }
    }



    public void exitFromCRpanelAction(View view) {
        startActivity(new Intent(CrPanelActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(CrPanelActivity.this, MainActivity.class));
        finish();
    }

    public void BTNcrPostToWeekOnClickAction(View view) {
        weekPB.setVisibility(View.VISIBLE);
        String NoticeNo = NoticeNoWeekET.getText().toString().trim();
        String PostTextWeek=postBoxForWeekET.getText().toString().trim();
        if (isNetworkConnected() == true) {

            try {
                if(!TextUtils.isEmpty(NoticeNo) && !TextUtils.isEmpty(PostTextWeek)) {


                    calander = Calendar.getInstance();
                    simpledateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    Date = simpledateformat.format(calander.getTime());

                    postTextofWeek = "Notice No: "+NoticeNo+" (Posted:"+Date +")"+"\n" +PostTextWeek;

                    DatabaseReference setCrPostToWeekPath = FirebaseDatabase.getInstance().getReference().child("batches").child(CrDept).child(CrBatch).child(CrShift).child("notifn_cr").child(CrSec).child("week_tab_cr").child("w1");

                    setCrPostToWeekPath.child(NoticeNo).setValue(postTextofWeek);
                    weekPB.setVisibility(View.INVISIBLE);
                    weekResult.setText("Posted Successfully!");
                    postBoxForWeekET.setText("");
                    NoticeNoWeekET.setText("");
                }else {
                    weekResult.setText("Fill Up Both Box!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                weekResult.setText("Not posted! poor network");
            }


        } else {
            Toast.makeText(CrPanelActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            weekPB.setVisibility(View.INVISIBLE);
        }


    }

    public void BTNcrPostToMonthOnClickAction(View view) {
        monthPB.setVisibility(View.VISIBLE);
        String NoticeNo2 = NoticeNoMonthET.getText().toString().trim();
        String PostTextMonth=postBoxForMonthET.getText().toString().trim();
        if (isNetworkConnected() == true) {
            try {
                if(!TextUtils.isEmpty(NoticeNo2) && !TextUtils.isEmpty(PostTextMonth)) {
                    calander = Calendar.getInstance();
                    simpledateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    Date = simpledateformat.format(calander.getTime());

                    postTextofMonth = "Notice No: "+NoticeNo2+" (Posted:"+Date +")"+"\n" +PostTextMonth;

                    DatabaseReference setCrPostToMonthPath = FirebaseDatabase.getInstance().getReference().child("batches").child(CrDept).child(CrBatch).child(CrShift).child("notifn_cr").child(CrSec).child("month_tab_cr").child("m1");

                    setCrPostToMonthPath.child(NoticeNo2).setValue(postTextofMonth);
                    monthPB.setVisibility(View.INVISIBLE);
                    monthResult.setText("Posted Successfully!");
                    postBoxForMonthET.setText("");
                    NoticeNoMonthET.setText("");
                }else {
                        monthResult.setText("Fill Up Both Box!");
                    }
            } catch (Exception e) {
                e.printStackTrace();
                monthResult.setText("Not posted! poor network");
            }


        } else {
            Toast.makeText(CrPanelActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            monthPB.setVisibility(View.INVISIBLE);
        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void BTNviewpostWeekAction(View view) {

        Intent intent = new Intent(CrPanelActivity.this, ThisWeekCrPostActivity.class);
        intent.putExtra("crdept", CrDept);
        intent.putExtra("crbatch", CrBatch);
        intent.putExtra("crshift", CrShift);
        intent.putExtra("crsec", CrSec);
        startActivity(intent);
    }

    public void BTNviewpostMonthAction(View view) {

        Intent intent = new Intent(CrPanelActivity.this, ThisMonthCrPostActivity.class);
        intent.putExtra("crdept", CrDept);
        intent.putExtra("crbatch", CrBatch);
        intent.putExtra("crshift", CrShift);
        intent.putExtra("crsec", CrSec);
        startActivity(intent);
    }
}
