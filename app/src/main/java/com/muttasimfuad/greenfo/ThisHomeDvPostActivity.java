package com.muttasimfuad.greenfo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ThisHomeDvPostActivity extends AppCompatActivity {

    private DatabaseReference Dref;

    private ListView dvNoticeListView;
    private ArrayList<String> dvNoticeList = new ArrayList<>();

    String mKey;

    List<String> ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_this_home_dv_post);

        dvNoticeListView = (ListView) findViewById(R.id.thisweektabDViewLV);


        // FOUND USERS DEPT...


        try {

            final ArrayAdapter<String> adapterForDvNotice = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dvNoticeList);
            dvNoticeListView.setAdapter(adapterForDvNotice);
            Dref = FirebaseDatabase.getInstance().getReference().child("Powership").child("Dev").child("notifn_dv").child("home_tab_dv").child("h1");
            Dref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    dvNoticeList.add(dataSnapshot.getValue(String.class));
                    adapterForDvNotice.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {


                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    dvNoticeList.remove(dataSnapshot.getValue(String.class));
                    adapterForDvNotice.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        // DONE of Setting Up AppUser's CR NOTICE

//        Cref.getRoot().addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot messages : dataSnapshot.getChildren()) {
//
//                    Map<String,Object> value = (Map<String, Object>) dataSnapshot.getValue();
//                    keyList.add(dataSnapshot.getKey());
//                    items.add(value.get(keyList));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//        /*handle errors*/
//            }
//        });
//
//        crNoticeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                items.remove(position);
//               // adapt.notifyDataSetChanged();
//                //new code below
//                Cref.getRoot().child(keyList.get(position)).removeValue();
//                keyList.remove(position);
//            }
//        });


        //crNoticeListView.






    }

    public void DeleteRowActionDV(View view) {
        EditText edit = (EditText) findViewById(R.id.delTextd);
        String name = edit.getText().toString();
        Query query = Dref.orderByKey().equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapshot : dataSnapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        edit.setText("");
    }
}

