package com.muttasimfuad.greenfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;

import java.util.Map;

import layout.EditProfileActivity;
import layout.SignupActivity;

public class MyProfileActivity extends AppCompatActivity {


    TextView nameText, genderText, deptText, batchText, shiftText,secText, idText, myphnNO,myBloodGroupTV,DonationAvailabilityTV;
    FirebaseAuth mAuth;
    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference myRef;
    String CurrentUserPhoneNo,pp;
    Button Login, Logout, EditBio;
    String MyName,MyID,MyGender,MyDept,MyBatch,MyShift,MySec,myBloodGroup,DonattionAvailability;

    ImageView myGenIMG;
    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        nameText = (TextView) findViewById(R.id.mynameTV);
        genderText = (TextView) findViewById(R.id.genderTV);
        deptText = (TextView) findViewById(R.id.deptTV);
        batchText = (TextView) findViewById(R.id.batchTV);
        shiftText = (TextView) findViewById(R.id.shiftTV);
        secText = (TextView) findViewById(R.id.secTV);
        idText = (TextView) findViewById(R.id.myidTV);
        myphnNO = (TextView) findViewById(R.id.phnNoTV);
        Login = (Button) findViewById(R.id.logMeInBTN);
        Logout = (Button) findViewById(R.id.logMeOutBTN);
        EditBio = (Button) findViewById(R.id.editInfoBTN);

        myGenIMG = (ImageView) findViewById(R.id.IMGgenderImageinMyAccount);

        myBloodGroupTV= (TextView) findViewById(R.id.BloogGroupTV);
        DonationAvailabilityTV= (TextView) findViewById(R.id.availableForDonate);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        if (mAuth.getCurrentUser() != null) {



            Login.setVisibility(View.GONE);
            CurrentUserPhoneNo = user.getPhoneNumber();
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            myRef = mFirebaseDatabase.getReference();

            DatabaseReference user_info_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

            Query deleteQuery = user_info_Ref.orderByChild(CurrentUserPhoneNo);

            deleteQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    showData(dataSnapshot);
                    myphnNO.setText("Phone No:" + CurrentUserPhoneNo.substring(3) + " (Hidden from others)");

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i("Item not found: ", "this item is not in the list");
                }
            });







//            try {
//                mUserValueEventListener = new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        if (dataSnapshot.getValue() != null) {
//
//                            AppUser appUser = dataSnapshot.getValue(AppUser.class);
//                            if (appUser.getPropicurl() == null) {
//                                pp = "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
//                            } else pp = appUser.getPropicurl();
//                            Glide.with(MyProfileActivity.this)
//                                    .load(pp)
//                                    .into(myGenIMG);
//                        }
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                };
//            } catch (Exception e) {
//                e.printStackTrace();
//            }


        } else {
            Toast.makeText(MyProfileActivity.this, "You must have to login to get all feature of this app!!", Toast.LENGTH_LONG).show();

            nameText.setVisibility(View.GONE);
            genderText.setVisibility(View.GONE);
            deptText.setVisibility(View.GONE);
            batchText.setVisibility(View.GONE);
            shiftText.setVisibility(View.GONE);
            secText.setVisibility(View.GONE);
            idText.setVisibility(View.GONE);
            myphnNO.setVisibility(View.GONE);

            myBloodGroupTV.setVisibility(View.GONE);
            DonationAvailabilityTV.setVisibility(View.GONE);

            Logout.setVisibility(View.GONE);
            EditBio.setVisibility(View.GONE);

            Login.setVisibility(View.VISIBLE);
        }


    }


    private void showData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();

            nameText.setText("Name: " + String.valueOf(value.get("username")));
            MyName = String.valueOf(value.get("username"));
            genderText.setText("Gender: " + String.valueOf(value.get("gender")));
            MyGender= String .valueOf(value.get(("gender")));
            deptText.setText("Deparment: " + String.valueOf(value.get("department")));
            MyDept= String .valueOf(value.get("department"));
            batchText.setText("Batch: " + String.valueOf(value.get("batch")));
            MyBatch= String.valueOf(value.get("batch"));
            shiftText.setText("Shift: " + String.valueOf(value.get("shift")));
            MyShift = String .valueOf(value.get("shift"));
            idText.setText("Id: " + String.valueOf(value.get("id")));
            MyID = String.valueOf(value.get("id"));
            MySec = String .valueOf(value.get("sec"));
            secText.setText("Section: " + String.valueOf(value.get("sec")));

            myBloodGroup= String .valueOf(value.get("blood_group"));
            myBloodGroupTV.setText("Blood Group: " + String.valueOf(value.get("blood_group")));

            DonattionAvailability= String .valueOf(value.get("donate_blood"));
            DonationAvailabilityTV.setText("Available for Blood Donation: " + String.valueOf(value.get("donate_blood")));

            pp = String.valueOf(value.get("propicurl"));


            try {
                Glide.with(MyProfileActivity.this)
                        .load(pp)
                        .into(myGenIMG);
            } catch (Exception e) {
                e.printStackTrace();
                Glide.with(MyProfileActivity.this)
                        .load("https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3")
                        .into(myGenIMG);
            }


        }
    }

    public void editInfoAction(View view) {

        if (isNetworkConnected() == true) {

            Intent editMe = new Intent(MyProfileActivity.this, EditProfileActivity.class);
            editMe.putExtra("nam", MyName);
            editMe.putExtra("id", MyID);
            editMe.putExtra("gender",MyGender );
            editMe.putExtra("dept",MyDept);
            editMe.putExtra("batch",MyBatch);
            editMe.putExtra("shift",MyShift);
            editMe.putExtra("sec",MySec);
            editMe.putExtra("blood_group",myBloodGroup);
            editMe.putExtra("donate_blood",DonattionAvailability);
            editMe.putExtra("propicurl",pp);
            startActivity(editMe);
            finish();

        } else {
            Toast.makeText(MyProfileActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }


    }

    public void logMeOutAction(View view) {


        if (isNetworkConnected() == true) {


            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked

                            try {
                                if (mAuth.getCurrentUser() != null) {
                                    mAuth.signOut();
                                    nameText.setVisibility(View.GONE);
                                    genderText.setVisibility(View.GONE);
                                    deptText.setVisibility(View.GONE);
                                    batchText.setVisibility(View.GONE);
                                    shiftText.setVisibility(View.GONE);
                                    secText.setVisibility(View.GONE);
                                    idText.setVisibility(View.GONE);
                                    myphnNO.setVisibility(View.GONE);

                                    myBloodGroupTV.setVisibility(View.GONE);
                                    DonationAvailabilityTV.setVisibility(View.GONE);

                                    Logout.setVisibility(View.GONE);
                                    EditBio.setVisibility(View.GONE);

                                    Login.setVisibility(View.VISIBLE);
                                    Glide.with(MyProfileActivity.this)
                                            .load("https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3")
                                            .into(myGenIMG);
                                    startActivity(new Intent(MyProfileActivity.this, MainActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(MyProfileActivity.this, "You must have to login to get all feature!", Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                Toast.makeText(MyProfileActivity.this, "Failed to log out. Poor Network Connection!", Toast.LENGTH_LONG).show();
                            }

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


        } else {
            Toast.makeText(MyProfileActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }


    }

    public void logMeInAction(View view) {

        if (isNetworkConnected()) {

            startActivity(new Intent(MyProfileActivity.this, SignupActivity.class));
            finish();

        } else {
            Toast.makeText(MyProfileActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }


    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(MyProfileActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();



    }

    @Override
    protected void onStop() {
        super.onStop();

    }



}
