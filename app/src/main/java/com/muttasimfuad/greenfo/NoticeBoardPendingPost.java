package com.muttasimfuad.greenfo;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.models.User;
import com.muttasimfuad.greenfo.utils.Constants;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;

import static android.app.Activity.RESULT_OK;
import static com.muttasimfuad.greenfo.utils.FirebaseUtils.getImageSRefNoticeBoard;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoticeBoardPendingPost extends Fragment {

    private View mRootVIew;
    private FirebaseRecyclerAdapter<Post, PostHolder> mPostAdapter;
    private RecyclerView mPostRecyclerView;

    //new
    private ValueEventListener mUserValueEventListener;
    private DatabaseReference mUserRef;

    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseAuth mAuthx;
    private FirebaseUser mFirebaseUser;
    String MyName, pp, ModName;
    private FragmentActivity myContext;
    String NoticeImageURL, postIdPending;
    private static final int RC_PHOTO_PICKER = 1;
    private Uri mSelectedUri;
    private ProgressDialog mProgressDialog;

    private int ImageResult;

    private Post mPost;


    public NoticeBoardPendingPost() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootVIew = inflater.inflate(R.layout.fragment_help_zone_fragment_pending_post, container, false);

        try {
            getActivity().setTitle("Pending Notices");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Retrieve the value
        ModName = getArguments().getString("ModName");



        init();
        return mRootVIew;
    }

    private void init() {
        mPostRecyclerView = (RecyclerView) mRootVIew.findViewById(R.id.recyclerview_post);
        mPostRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        setupAdapter();
        mPostRecyclerView.setAdapter(mPostAdapter);
    }

    private void setupAdapter() {
        mPostAdapter = new FirebaseRecyclerAdapter<Post, PostHolder>(
                Post.class,
                R.layout.row_post_help_zone_pending,
                PostHolder.class,
                FirebaseUtils.getNoticeRef()
        ) {
            @Override
            protected void populateViewHolder(PostHolder viewHolder, final Post model, int position) {
//                viewHolder.setNumCOmments(String.valueOf(model.getNumComments()));
//                viewHolder.setNumLikes(String.valueOf(model.getNumLikes()));
                viewHolder.setTIme(DateUtils.getRelativeTimeSpanString(model.getTimeCreated()));
                viewHolder.setUsername(model.getUser().getUser());
                viewHolder.setPostText(model.getPostText());
                viewHolder.setPostTitle(model.getPostTitle());

                Glide.with(getActivity())
                        .load(model.getUser().getPhotoUrl())
                        .into(viewHolder.postOwnerDisplayImageView);

                if (model.getPostImageUrl() != null) {
                    viewHolder.postDisplayImageVIew.setVisibility(View.VISIBLE);
//                    StorageReference storageReference = FirebaseStorage.getInstance()
//                            .getReference(model.getPostImageUrl());
//                    Glide.with(getActivity())
//                            .using(new FirebaseImageLoader())
//                            .load(storageReference)
//                            .into(viewHolder.postDisplayImageVIew);
                    Glide.with(getActivity()).load(model.getPostImageUrl()).into(viewHolder.postDisplayImageVIew);
                } else {
                    viewHolder.postDisplayImageVIew.setImageBitmap(null);
                    viewHolder.postDisplayImageVIew.setVisibility(View.GONE);
                }

                viewHolder.deletePostBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (isNetworkConnected()) {


                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            //Yes button clicked



                                            try {
                                                String postId = model.getPostId();


                                                DatabaseReference rootpath_of_postContent = FirebaseDatabase.getInstance().getReference("notice_board_pending_notice");
                                                DatabaseReference postContecnt_fromPath = rootpath_of_postContent.child("posts").child(postId);


                                                postContecnt_fromPath.removeValue();

                                                try {
                                                    StorageReference photoRef = FirebaseUtils.getImageSRefNoticeBoard().child(postId);
                                                    photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            Toast.makeText(getContext(),"Picture deleted!",Toast.LENGTH_LONG).show();
                                                            // File deleted successfully
                                                            // Log.d(TAG, "onSuccess: deleted file");
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception exception) {
                                                            // Uh-oh, an error occurred!
                                                            // Log.d(TAG, "onFailure: did not delete file");
                                                            //Toast.makeText(getContext(),"Failed to delete! Inform it to Muttasim Fuad",Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Toast.makeText(getContext(), "Failed! Poor Network Connection!", Toast.LENGTH_LONG).show();
                                            }


                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
                            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();


                        } else {
                            Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                        }

                    }

                });
                viewHolder.KeepPostBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (isNetworkConnected()) {

                            try {

                                CallUpdateOrEditNoticeDialog(model.getPostTitle(),model.getPostText(),model.getPostImageUrl(),model.getPostId(),model.getTimeCreated(),model.getUser());

                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "Failed! Poor Network Connection!", Toast.LENGTH_LONG).show();
                            }
//                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    switch (which) {
//                                        case DialogInterface.BUTTON_POSITIVE:
//                                            //Yes button clicked
//
//
//
//
//                                            break;
//
//                                        case DialogInterface.BUTTON_NEGATIVE:
//                                            //No button clicked
//                                            break;
//                                    }
//                                }
//                            };

//                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
//                            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
//                                    .setNegativeButton("No", dialogClickListener).show();


                        } else {
                            Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        };
    }

    private void CallUpdateOrEditNoticeDialog(final String noticetitle, String noticedescription, String noticeimageurl, String postID, Long PostedTime, User user) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(myContext);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_or_edit_notice_dialog, null);
        dialogBuilder.setView(dialogView);


        //Access Dialog views
        final EditText updateNoticeTitleET = (EditText) dialogView.findViewById(R.id.updateTitleOfNoticeET);
        final EditText updateNoticeDescET = (EditText) dialogView.findViewById(R.id.updateNoticeDescriptionET);
        final ImageView noticeImageIV = (ImageView) dialogView.findViewById(R.id.noticeImageViewIV);

        final Button deleteImageBtn = (Button) dialogView.findViewById(R.id.DltImgBTN);
        final Button AddimgBtn = (Button) dialogView.findViewById(R.id.AddImgBTN);

        final Button buttonPublish = (Button) dialogView.findViewById(R.id.buttonUpdateClass);
        final Button buttonCancel = (Button) dialogView.findViewById(R.id.buttonDeleteClass);




        updateNoticeTitleET.setText(noticetitle);
        updateNoticeDescET.setText(noticedescription);
        NoticeImageURL = noticeimageurl;
        postIdPending = postID;




        if (NoticeImageURL != null) {
            noticeImageIV.setVisibility(View.VISIBLE);
            deleteImageBtn.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(noticeimageurl).into(noticeImageIV);
            AddimgBtn.setVisibility(View.GONE);
        } else {
            noticeImageIV.setImageBitmap(null);
            noticeImageIV.setVisibility(View.GONE);
            deleteImageBtn.setVisibility(View.GONE);
            AddimgBtn.setVisibility(View.VISIBLE);
        }


        //coursename for set dialog title
        dialogBuilder.setTitle("Notice ID: 062098"+user.getEmail());
        final AlertDialog b = dialogBuilder.create();
        b.show();



        deleteImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkConnected()) {


                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Yes button clicked


                                    try {
                                            StorageReference photoRef = FirebaseUtils.getImageSRefNoticeBoard().child(postID);
                                            photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {

                                                    Toast.makeText(getContext(),"Picture deleted!",Toast.LENGTH_LONG).show();
                                                    NoticeImageURL=null;
                                                    noticeImageIV.setVisibility(View.GONE);
                                                    AddimgBtn.setVisibility(View.VISIBLE);
                                                    // File deleted successfully
                                                    // Log.d(TAG, "onSuccess: deleted file");
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception exception) {
                                                    // Uh-oh, an error occurred!
                                                    // Log.d(TAG, "onFailure: did not delete file");
                                                    //Toast.makeText(getContext(),"Failed to delete! Inform it to Muttasim Fuad",Toast.LENGTH_LONG).show();
                                                }
                                            });



                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(getContext(), "Failed! Poor Network Connection!", Toast.LENGTH_LONG).show();

                                    }


                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(myContext); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
                    builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();


                } else {
                    Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        AddimgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
//                if(ImageResult == 1){
//
//                    noticeImageIV.setVisibility(View.VISIBLE);
//                    Glide.with(getActivity()).load(NoticeImageURL).into(noticeImageIV);
//                    AddimgBtn.setVisibility(View.GONE);
//                    deleteImageBtn.setVisibility(View.VISIBLE);
//                } else if(ImageResult == 0) {
//                    noticeImageIV.setImageBitmap(null);
//                    noticeImageIV.setVisibility(View.GONE);
//                    AddimgBtn.setVisibility(View.VISIBLE);
//                    deleteImageBtn.setVisibility(View.GONE);
//                }

            }
        });



        // Click listener for Update data
        buttonPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mPost = new Post();

                String Title = updateNoticeTitleET.getText().toString().trim();
                String Description = updateNoticeDescET.getText().toString().trim();

                if (!TextUtils.isEmpty(Title)) {

                    if (!TextUtils.isEmpty(Description)) {
                                    //Method for update data

                                    mPost.setUser(user);
                                    mPost.setNumComments(0);
                                    mPost.setNumLikes(0);
                                    mPost.setTimeCreated(PostedTime);
                                    mPost.setPostId(postID);
                                    mPost.setPostText(Description);
                                    mPost.setPostTitle(Title);
                                    mPost.setPostImageUrl(NoticeImageURL);

                        FirebaseUtils.getNoticeRef().child(postID)
                                .setValue(mPost);


                        try {
                        DatabaseReference rootpath_of_postContent = FirebaseDatabase.getInstance().getReference("notice_board_pending_notice");
                        DatabaseReference postContecnt_fromPath = rootpath_of_postContent.child("posts").child(postID);
                        DatabaseReference postContecnt_toPath = FirebaseDatabase.getInstance().getReference("notice_board").child("posts").child(postID);

                        addToMyPostListFinal(postID);  // postCreate dialog a dorkar nai ei action



                        moveGameRoom(postContecnt_fromPath, postContecnt_toPath);


                        postContecnt_fromPath.removeValue();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        b.dismiss();

                    }

                } else {

                   Toast.makeText(myContext, "Fill Up The Form!", Toast.LENGTH_LONG).show();
                }

                //checking if the value is provided or not Here, you can Add More Validation as you required


            }
        });

        // Click listener for Delete data
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Method for delete data
                b.dismiss();
            }
        });
    }


    private void addToMyPostListFinal(String postId) {
//        FirebaseUtils.getPostRef().child(postId)
//                .setValue(mPost);
        FirebaseUtils.getMyPostRefFinalNoticeBoard().child(postId).setValue(true);

        FirebaseUtils.addToMyRecordFinalNoticeBoard(Constants.POST_KEY, postId);
    }

    //During the tutorial I think I messed up this code. Make sure your's aligns to this, or just
    //check out the github code




    public static class PostHolder extends RecyclerView.ViewHolder {
        ImageView postOwnerDisplayImageView;
        TextView postOwnerUsernameTextView;
        TextView Someone;
        TextView postTimeCreatedTextView;
        ImageView postDisplayImageVIew;
        TextView postTextTextView,postTitleTV;
        LinearLayout postLikeLayout;
        LinearLayout postCommentLayout;
        TextView postNumLikesTextView;
        TextView postNumCommentsTextView;
        Button deletePostBTN, KeepPostBTN;


        public PostHolder(View itemView) {
            super(itemView);
            postOwnerDisplayImageView = (ImageView) itemView.findViewById(R.id.iv_post_owner_display);
            postOwnerUsernameTextView = (TextView) itemView.findViewById(R.id.tv_post_username);
            Someone = (TextView) itemView.findViewById(R.id.SomeOneTV);
            postTimeCreatedTextView = (TextView) itemView.findViewById(R.id.tv_time);
            postDisplayImageVIew = (ImageView) itemView.findViewById(R.id.iv_post_display);
            postTitleTV = (TextView) itemView.findViewById(R.id.tv_post_title);
            postTextTextView = (TextView) itemView.findViewById(R.id.tv_post_text);
            deletePostBTN = (Button) itemView.findViewById(R.id.deleteBTNhelpZone);
            KeepPostBTN = (Button) itemView.findViewById(R.id.allowBTNhelpZone);
        }

        public void setUsername(String username) {
            postOwnerUsernameTextView.setText(username);
            Someone.setVisibility(View.GONE);
            postOwnerUsernameTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        }

        public void setTIme(CharSequence time) {
            postTimeCreatedTextView.setText(time);
        }

//        public void setNumLikes(String numLikes) {
//            postNumLikesTextView.setText(numLikes);
//        }
//
//        public void setNumCOmments(String numComments) {
//            postNumCommentsTextView.setText(numComments);
//        }

        public void setPostText(String text) {
            postTextTextView.setText(text);
        }

        public void setPostTitle(String text) {
            postTitleTV.setText(text);
        }



    }


//    @Override
//    public void onStart() {
//        super.onStart();
//        //mAuthx.addAuthStateListener(mAuthStateListener);
//        if (mUserRef != null) {
//            mUserRef.addValueEventListener(mUserValueEventListener);
//        }
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mAuthStateListener != null)
//            mAuthx.removeAuthStateListener(mAuthStateListener);
//        if (mUserRef != null)
//            mUserRef.removeEventListener(mUserValueEventListener);
//    }

    private void moveGameRoom(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                        if (firebaseError != null) {
                            //toPath.child("Approved By: "+ModName);
                            //   DisPlayText.setText("Copy failed");
                            Toast.makeText(getContext(),"Network error! Failed to approve!",Toast.LENGTH_LONG).show();

                        } else {
                            //  DisPlayText.setText("Successfully Copied!!");
                            toPath.child("Approved By: ").setValue(ModName);
                            Toast.makeText(getContext(),"Notice has been approved!",Toast.LENGTH_LONG).show();

                        }
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    HomeFragment helpZoneFragmentPendingPost = new HomeFragment();


                    FragmentTransaction fragmentTransaction =
                            myContext.getSupportFragmentManager().beginTransaction();

                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                    fragmentTransaction.commit();





                    return true;

                }

                return false;
            }
        });

        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }


    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_PHOTO_PICKER) {
            if (resultCode == RESULT_OK) {

                mProgressDialog = new ProgressDialog(getContext());
                mProgressDialog.setMessage("Uploading Picture...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.show();

                mSelectedUri = data.getData();

                if (mSelectedUri != null) {
                    getImageSRefNoticeBoard()
                            //.child(mSelectedUri.getLastPathSegment())
                            .child(postIdPending)
                            .putFile(mSelectedUri)
                            .addOnSuccessListener(getActivity(),
                                    new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                            //StorageReference photoRef2 = getImageSRefNoticeBoard().child(mSelectedUri.getLastPathSegment());
                                            StorageReference photoRef =getImageSRefNoticeBoard().child("/" + postIdPending);
                                            // Download file From Firebase Storage
                                            photoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                @Override
                                                public void onSuccess(Uri downloadPhotoUrl) {
                                                    //Now play with downloadPhotoUrl
                                                    //Store data into Firebase Realtime Database
                                                    NoticeImageURL = downloadPhotoUrl.toString();


                                                }
                                            });

                                            //String url = Constants.POST_IMAGES + "/" + postId; //mSelectedUri.getLastPathSegment();



                                            Toast.makeText(getContext(),"Upload Complete!",Toast.LENGTH_LONG).show();
                                            ImageResult= 1;

//                                            LayoutInflater inflater = getLayoutInflater();
//                                            final View dialogView = inflater.inflate(R.layout.update_or_edit_notice_dialog, null);
//
//                                            final ImageView noticeImageIV = (ImageView) dialogView.findViewById(R.id.noticeImageViewIV);
//
//                                            final Button deleteImageBtn = (Button) dialogView.findViewById(R.id.DltImgBTN);
//                                            final Button AddimgBtn = (Button) dialogView.findViewById(R.id.AddImgBTN);
//
//
//                                            if(ImageResult == 1){
//
//                                                noticeImageIV.setVisibility(View.VISIBLE);
//                                                Glide.with(getActivity()).load(NoticeImageURL).into(noticeImageIV);
//                                                AddimgBtn.setVisibility(View.GONE);
//                                                deleteImageBtn.setVisibility(View.VISIBLE);
//                                            } else if(ImageResult == 0) {
//                                                noticeImageIV.setImageBitmap(null);
//                                                noticeImageIV.setVisibility(View.GONE);
//                                                AddimgBtn.setVisibility(View.VISIBLE);
//                                                deleteImageBtn.setVisibility(View.GONE);
//                                            }

                                            mProgressDialog.dismiss();

                                        }
                                    });
                } else {

                    mProgressDialog.dismiss();

                    Toast.makeText(getContext(),"Upload Failed!",Toast.LENGTH_LONG).show();
                    NoticeImageURL=null;
                    ImageResult=0;


//                    LayoutInflater inflater = getLayoutInflater();
//                    final View dialogView = inflater.inflate(R.layout.update_or_edit_notice_dialog, null);
//
//                    final ImageView noticeImageIV = (ImageView) dialogView.findViewById(R.id.noticeImageViewIV);
//
//                    final Button deleteImageBtn = (Button) dialogView.findViewById(R.id.DltImgBTN);
//                    final Button AddimgBtn = (Button) dialogView.findViewById(R.id.AddImgBTN);
//
//                    if(ImageResult == 1){
//
//                        noticeImageIV.setVisibility(View.VISIBLE);
//                        Glide.with(getActivity()).load(NoticeImageURL).into(noticeImageIV);
//                        AddimgBtn.setVisibility(View.GONE);
//                        deleteImageBtn.setVisibility(View.VISIBLE);
//                    } else if(ImageResult == 0) {
//                        noticeImageIV.setImageBitmap(null);
//                        noticeImageIV.setVisibility(View.GONE);
//                        AddimgBtn.setVisibility(View.VISIBLE);
//                        deleteImageBtn.setVisibility(View.GONE);
//                    }
                }


            }
        }
    }
    

}
