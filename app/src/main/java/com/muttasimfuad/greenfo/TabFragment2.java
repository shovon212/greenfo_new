package com.muttasimfuad.greenfo;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import Adapter.CustomAdapterTodayRoutine;
import ModalClass.RoutineInfo;


/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment2 extends Fragment {

    ListView mListView, crNoticeListView;
    ArrayList<RoutineInfo> arrayList;
    ArrayList<String> crNoticeList = new ArrayList<>();
    RoutineInfo routineInfo;
    CustomAdapterTodayRoutine adapter;

    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef, Cref;
    private String CurrentUserPhoneNo;
    TextView tomorrowDateTIme;
    TextView tomorrowTclassTV;
    TextView userBatchInCrNotice;


    String Time;
    String CourseCode;
    String CourseName;
    String RoomNo;
    String c;

    String userDept, userBatch, userShift,userSec, UserID;
    int dayNo;
    int tomoDayCounter;
    String TommoDay;
    String[] dayList = {
            "Friday",
            "Saturday",
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday"
    };

    public TabFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_fragment2, container, false);
        //return inflater.inflate(R.layout.fragment_tab_fragment1, container, false);
        tomorrowDateTIme = view.findViewById(R.id.tomorrowDayTV);
        tomorrowTclassTV = view.findViewById(R.id.tomorrowTotalClassTV);
        userBatchInCrNotice = view.findViewById(R.id.TVstudentBatchInCRNotice);

        mListView = view.findViewById(R.id.tab2ListView);
        crNoticeListView = view.findViewById(R.id.tab2CRListView);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            FirebaseUser user = mAuth.getCurrentUser();
            CurrentUserPhoneNo = user.getPhoneNumber();


            arrayList = new ArrayList<>();

            mFirebaseDatabase = FirebaseDatabase.getInstance();
            myRef = mFirebaseDatabase.getReference();


            Calendar sCalendar = Calendar.getInstance();
            String dayName = sCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());


            if (dayName.equals("Fri")) {
                dayNo = 0;
            }
            if (dayName.equals("Sat")) {
                dayNo = 1;
            }
            if (dayName.equals("Sun")) {
                dayNo = 2;
            }
            if (dayName.equals("Mon")) {
                dayNo = 3;
            }
            if (dayName.equals("Tue")) {
                dayNo = 4;
            }
            if (dayName.equals("Wed")) {
                dayNo = 5;
            }
            if (dayName.equals("Thu")) {
                dayNo = 6;
            }
            tomoDayCounter = dayNo + 1;
            if (tomoDayCounter > 6) {
                tomoDayCounter = 0;
            }

            tomorrowDateTIme.setText("Tomorrow is: " + dayList[tomoDayCounter]);


            TommoDay = dayList[tomoDayCounter];


            DatabaseReference user_info_Ref;
            try {
                user_info_Ref = myRef.child("user_info").child(CurrentUserPhoneNo).child("courses").child(TommoDay);

                Query deleteQuery = user_info_Ref.orderByChild(TommoDay);

                deleteQuery.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        showData(dataSnapshot);

                        try {
                            arrayList.add(routineInfo);
                            adapter = new CustomAdapterTodayRoutine(TabFragment2.this.getActivity(), arrayList);
                            mListView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("Item not found: ", "this item is not in the list");
                    }
                });


                        // FINDING USERS DEPARTMENT TO SHOW THEIR CR's NOTICE


                        DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

                        Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

                        findBatchQuery.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                showDataofUserBatch(dataSnapshot);
                                // FOUND USERS DEPT and ID...
                                try {
                                    if (UserID.length() == 9) {
    //                                    if (UserID.substring(0,3).equals(userBatch) || UserID.substring(0,4).equals(userBatch)) {  //updateIT. rather you can check the length of userID!

                                            userBatchInCrNotice.setText("Batch: " + userDept + " " + userBatch + " " + userShift+" "+userSec); //check another field Section
                                            final ArrayAdapter<String> adapterForCrNotice = new ArrayAdapter<String>(TabFragment2.this.getActivity(), android.R.layout.simple_list_item_1, crNoticeList);
                                            crNoticeListView.setAdapter(adapterForCrNotice);
                                            Cref = FirebaseDatabase.getInstance().getReference().child("batches").child(userDept).child(userBatch).child(userShift).child("notifn_cr").child(userSec).child("week_tab_cr").child("w1");
                                            Cref.addChildEventListener(new ChildEventListener() {
                                                @Override
                                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                    crNoticeList.add(dataSnapshot.getValue(String.class));
                                                    adapterForCrNotice.notifyDataSetChanged();

                                                }

                                                @Override
                                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                                }

                                                @Override
                                                public void onChildRemoved(DataSnapshot dataSnapshot) {
                                                    crNoticeList.remove(dataSnapshot.getValue(String.class));
                                                    adapterForCrNotice.notifyDataSetChanged();
                                                }

                                                @Override
                                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                }
                                            });

                                            // DONE of Setting Up AppUser's CR NOTICE
         //                               }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    crNoticeListView.setVisibility(View.INVISIBLE);
                                }


                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.i("Item not found: ", "this item is not in the list");
                            }
                        });



            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                DatabaseReference totalclassRef = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(TommoDay);

                totalclassRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long num = dataSnapshot.getChildrenCount();
                        if (num < 2) {
                            c = "class";
                        } else {
                            c = "classes";
                        }
                        tomorrowTclassTV.setText("You have " + num + " " + c + " tomorrow!");

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                tomorrowTclassTV.setText("You have no classes tomorrow!");
            }


            try {
                DatabaseReference firstListViewHeightRef = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(TommoDay);
                firstListViewHeightRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long num = dataSnapshot.getChildrenCount();
                        if (num < 2) {
                            c = "class";
                            ViewGroup.LayoutParams params = mListView.getLayoutParams();
                            params.height = 200;
                            mListView.setLayoutParams(params);
                            mListView.requestLayout();

                        } else {
                            c = "classes";

                        }
                        if (num == 0) {
                            mListView.setAdapter(null);
                            ViewGroup.LayoutParams params = mListView.getLayoutParams();
                            params.height = 0;
                            mListView.setLayoutParams(params);
                            mListView.requestLayout();
                        } else if (num == 2) {
                            ViewGroup.LayoutParams params = mListView.getLayoutParams();
                            params.height = 350;
                            mListView.setLayoutParams(params);
                            mListView.requestLayout();

                        } else if (num > 2) {
                            ViewGroup.LayoutParams params = mListView.getLayoutParams();
                            params.height = 420;
                            mListView.setLayoutParams(params);
                            mListView.requestLayout();

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        return view;
    }


    private void showData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {

            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            routineInfo = new RoutineInfo();
//            routineInfo.setName("Hi"); //set the name
//            routineInfo.setRelationship(ds.child(CurrentUserPhoneNo).getValue(RoutineInfo.class).getRelationship()); //set the email
//            routineInfo.setNumber(ds.child("CSE205").getValue(String.class)); //set the phone_num
//            routineInfo.setToday(ds.child("CSE205").getValue(String.class)); //set the phone_num
//            Relation = ds.child("name").getKey();
            Time = String.valueOf(value.get("time"));
            CourseCode = String.valueOf(value.get("course_code"));
            CourseName = String.valueOf(value.get("name"));
            RoomNo = String.valueOf(value.get("room"));
            routineInfo = new RoutineInfo(Time, CourseCode, CourseName, RoomNo);

        }
    }

    private void showDataofUserBatch(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            userDept = String.valueOf(value.get("department"));
            userBatch = String.valueOf(value.get("batch"));
            userShift = String.valueOf(value.get("shift"));
            userSec = String.valueOf(value.get("sec"));
            UserID = String.valueOf(value.get("id"));

        }
    }


}