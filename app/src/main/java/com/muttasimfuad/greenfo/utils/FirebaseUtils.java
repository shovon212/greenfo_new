package com.muttasimfuad.greenfo.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by fuad on 2017/02/05.
 */

public class FirebaseUtils {
    //I'm creating this class for similar reasons as the Constants class, and to make my code a bit
    //cleaner and more well managed.

    public static DatabaseReference getUserRef(String phn){
        return FirebaseDatabase.getInstance()
                .getReference().child("help_zone").child(Constants.USERS_KEY)
                .child(phn);
    }
    public static DatabaseReference getUserRefNoticeBoard(String phn){
        return FirebaseDatabase.getInstance()
                .getReference().child("notice_board").child(Constants.USERS_KEY)
                .child(phn);
    }

    public static DatabaseReference getUserRefGubTroll(String phn){
        return FirebaseDatabase.getInstance()
                .getReference().child("gub_troll").child(Constants.USERS_KEY)
                .child(phn);
    }

    public static DatabaseReference getUserRefCrushConfession(String phn) {
        return FirebaseDatabase.getInstance()
                .getReference().child("crush_confession").child(Constants.USERS_KEY)
                .child(phn);
    }

    public static DatabaseReference getUserRefadmiration_post(String phn) {
        return FirebaseDatabase.getInstance()
                .getReference().child("admiration_post").child(Constants.USERS_KEY)
                .child(phn);
    }


    public static DatabaseReference getUserDetailsRef(String phn) {
        return FirebaseDatabase.getInstance()
                .getReference().child("user_info").child(phn)
                .child("personal_info");
    }






    public static DatabaseReference getPostRef(){
        return FirebaseDatabase.getInstance()
                .getReference().child("help_zone_pending_post").child(Constants.POST_KEY);
    }
    public static DatabaseReference getNoticeRef(){
        return FirebaseDatabase.getInstance()
                .getReference().child("notice_board_pending_notice").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostRefGubTroll() {
        return FirebaseDatabase.getInstance()
                .getReference().child("gub_troll_pending_post").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostRefCrushConfession() {
        return FirebaseDatabase.getInstance()
                .getReference().child("crush_confession_pending_post").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostRefadmiration_post() {
        return FirebaseDatabase.getInstance()
                .getReference().child("admiration_post_pending_post").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostRefFinal() {
        return FirebaseDatabase.getInstance()
                .getReference().child("help_zone").child(Constants.POST_KEY);
    }
    public static DatabaseReference getNoticeRefFinal() {
        return FirebaseDatabase.getInstance()
                .getReference().child("notice_board").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostRefFinalGubTroll() {
        return FirebaseDatabase.getInstance()
                .getReference().child("gub_troll").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostRefFinalCrushConfession() {
        return FirebaseDatabase.getInstance()
                .getReference().child("crush_confession").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostRefFinaladmiration_post() {
        return FirebaseDatabase.getInstance()
                .getReference().child("admiration_post").child(Constants.POST_KEY);
    }

    public static DatabaseReference getPostLikedRef(){
        return FirebaseDatabase.getInstance()
                .getReference().child("help_zone").child(Constants.POST_LIKED_KEY);
    }
    public static DatabaseReference getPostLikedRefNoticeBoard(){
        return FirebaseDatabase.getInstance()
                .getReference().child("notice_board").child(Constants.POST_LIKED_KEY);
    }

    public static DatabaseReference getPostLikedRefGubTroll() {
        return FirebaseDatabase.getInstance()
                .getReference().child("gub_troll").child(Constants.POST_LIKED_KEY);
    }

    public static DatabaseReference getPostLikedRefCrushConfession() {
        return FirebaseDatabase.getInstance()
                .getReference().child("crush_confession").child(Constants.POST_LIKED_KEY);
    }

    public static DatabaseReference getPostLikedRefadmiration_post() {
        return FirebaseDatabase.getInstance()
                .getReference().child("admiration_post").child(Constants.POST_LIKED_KEY);
    }

    public static DatabaseReference getPostLikedRef(String postId){
        return getPostLikedRef().child(getCurrentUser().getPhoneNumber()).child(postId);
    }
    public static DatabaseReference getPostLikedRefNoticeBoard(String postId){
        return getPostLikedRefNoticeBoard().child(getCurrentUser().getPhoneNumber()).child(postId);
    }


    public static DatabaseReference getPostLikedRefGubTroll(String postId) {
        return getPostLikedRefGubTroll().child(getCurrentUser().getPhoneNumber()).child(postId);
    }

    public static DatabaseReference getPostLikedRefCrushConfession(String postId) {
        return getPostLikedRefCrushConfession().child(getCurrentUser().getPhoneNumber()).child(postId);
    }

    public static DatabaseReference getPostLikedRefadmiration_post(String postId) {
        return getPostLikedRefadmiration_post().child(getCurrentUser().getPhoneNumber()).child(postId);
    }

    public static FirebaseUser getCurrentUser(){
        return FirebaseAuth.getInstance().getCurrentUser();
    }



    public static String getUid(){  //fix
        String path = FirebaseDatabase.getInstance().getReference().child("help_zone_pending_post").push().toString();
        return path.substring(path.lastIndexOf("/") + 1);
    }

    public static StorageReference getImageSRef(){
        return FirebaseStorage.getInstance().getReference().child(Constants.POST_IMAGES);
    }
    public static StorageReference getImageSRefProPic(){
        return FirebaseStorage.getInstance().getReference().child(Constants.DP_IMAGES);
    }
    public static StorageReference getImageSRefNoticeBoard(){
        return FirebaseStorage.getInstance().getReference().child(Constants.POST_IMAGES_Notice);
    }


    public static StorageReference getImageSRefGubTroll() {
        return FirebaseStorage.getInstance().getReference().child(Constants.POST_IMAGES_GUB_TROLL);
    }

    public static StorageReference getImageSRefCrushConfession() {
        return FirebaseStorage.getInstance().getReference().child(Constants.POST_IMAGES_CRUSH_CONFESSION);
    }

    public static StorageReference getImageSRefadmiration_post() {
        return FirebaseStorage.getInstance().getReference().child(Constants.POST_IMAGES_ADMIRATION_POST);
    }

    public static StorageReference getMyPP(String pp) {
        return FirebaseStorage.getInstance().getReference().child("All_Profile_Pics").child(pp);
    }

    public static DatabaseReference getMyPostRef(){
        return FirebaseDatabase.getInstance().getReference().child("help_zone_pending_post").child(Constants.MY_POSTS)
                .child(getCurrentUser().getPhoneNumber());
    }

    public static DatabaseReference getMyPostRefFinal() {
        return FirebaseDatabase.getInstance().getReference().child("help_zone").child(Constants.MY_POSTS)
                .child(getCurrentUser().getPhoneNumber());
    }
    public static DatabaseReference getMyPostRefFinalNoticeBoard() {
        return FirebaseDatabase.getInstance().getReference().child("notice_board").child(Constants.MY_POSTS)
                .child(getCurrentUser().getPhoneNumber());
    }


    public static DatabaseReference getMyPostRefFinalGubTroll() {
        return FirebaseDatabase.getInstance().getReference().child("gub_troll").child(Constants.MY_POSTS)
                .child(getCurrentUser().getPhoneNumber());
    }

    public static DatabaseReference getMyPostRefFinalCrushConfession() {
        return FirebaseDatabase.getInstance().getReference().child("crush_confession").child(Constants.MY_POSTS)
                .child(getCurrentUser().getPhoneNumber());
    }

    public static DatabaseReference getMyPostRefFinaladmiration_post() {
        return FirebaseDatabase.getInstance().getReference().child("admiration_post").child(Constants.MY_POSTS)
                .child(getCurrentUser().getPhoneNumber());
    }

    public static DatabaseReference getCommentRef(String postId){
        return FirebaseDatabase.getInstance().getReference().child("help_zone").child(Constants.COMMENTS_KEY)
                .child(postId);
    }
    public static DatabaseReference getCommentRefNoticeBoard(String postId){
        return FirebaseDatabase.getInstance().getReference().child("notice_board").child(Constants.COMMENTS_KEY)
                .child(postId);
    }


    public static DatabaseReference getCommentRefGubTroll(String postId) {
        return FirebaseDatabase.getInstance().getReference().child("gub_troll").child(Constants.COMMENTS_KEY)
                .child(postId);
    }

    public static DatabaseReference getCommentRefCrushConfession(String postId) {
        return FirebaseDatabase.getInstance().getReference().child("crush_confession").child(Constants.COMMENTS_KEY)
                .child(postId);
    }

    public static DatabaseReference getCommentRefadmiration_post(String postId) {
        return FirebaseDatabase.getInstance().getReference().child("admiration_post").child(Constants.COMMENTS_KEY)
                .child(postId);
    }

//    public static DatabaseReference getMyRecordRef(){
//        return FirebaseDatabase.getInstance().getReference().child("help_zone_pending_post").child(Constants.USER_RECORD)
//                .child(getCurrentUser().getPhoneNumber());
//    }

    public static DatabaseReference getMyRecordRefFinal() {
        return FirebaseDatabase.getInstance().getReference().child("help_zone").child(Constants.USER_RECORD)
                .child(getCurrentUser().getPhoneNumber());
    }
    public static DatabaseReference getMyRecordRefFinalNoticeBoard() {
        return FirebaseDatabase.getInstance().getReference().child("notice_board").child(Constants.USER_RECORD)
                .child(getCurrentUser().getPhoneNumber());
    }


    public static DatabaseReference getMyRecordRefFinalGubTroll() {
        return FirebaseDatabase.getInstance().getReference().child("gub_troll").child(Constants.USER_RECORD)
                .child(getCurrentUser().getPhoneNumber());
    }

    public static DatabaseReference getMyRecordRefFinalCrushConfession() {
        return FirebaseDatabase.getInstance().getReference().child("crush_confession").child(Constants.USER_RECORD)
                .child(getCurrentUser().getPhoneNumber());
    }

    public static DatabaseReference getMyRecordRefFinaladmiration_post() {
        return FirebaseDatabase.getInstance().getReference().child("admiration_post").child(Constants.USER_RECORD)
                .child(getCurrentUser().getPhoneNumber());
    }


    public static void addToMyRecordFinal(String node, final String id) {
        getMyRecordRefFinal().child(node).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                ArrayList<String> myRecordCollection;
                if (mutableData.getValue() == null) {
                    myRecordCollection = new ArrayList<String>(1);
                    myRecordCollection.add(id);
                } else {
                    myRecordCollection = (ArrayList<String>) mutableData.getValue();
                    myRecordCollection.add(id);
                }

                mutableData.setValue(myRecordCollection);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }
    public static void addToMyRecordFinalNoticeBoard(String node, final String id) {
        getMyRecordRefFinalNoticeBoard().child(node).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                ArrayList<String> myRecordCollection;
                if (mutableData.getValue() == null) {
                    myRecordCollection = new ArrayList<String>(1);
                    myRecordCollection.add(id);
                } else {
                    myRecordCollection = (ArrayList<String>) mutableData.getValue();
                    myRecordCollection.add(id);
                }

                mutableData.setValue(myRecordCollection);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }


    public static void addToMyRecordFinalGubTroll(String node, final String id) {
        getMyRecordRefFinalGubTroll().child(node).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                ArrayList<String> myRecordCollection;
                if (mutableData.getValue() == null) {
                    myRecordCollection = new ArrayList<String>(1);
                    myRecordCollection.add(id);
                } else {
                    myRecordCollection = (ArrayList<String>) mutableData.getValue();
                    myRecordCollection.add(id);
                }

                mutableData.setValue(myRecordCollection);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }

    public static void addToMyRecordFinalCrushConfession(String node, final String id) {
        getMyRecordRefFinalCrushConfession().child(node).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                ArrayList<String> myRecordCollection;
                if (mutableData.getValue() == null) {
                    myRecordCollection = new ArrayList<String>(1);
                    myRecordCollection.add(id);
                } else {
                    myRecordCollection = (ArrayList<String>) mutableData.getValue();
                    myRecordCollection.add(id);
                }

                mutableData.setValue(myRecordCollection);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }

    public static void addToMyRecordFinaladmiration_post(String node, final String id) {
        getMyRecordRefFinaladmiration_post().child(node).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                ArrayList<String> myRecordCollection;
                if (mutableData.getValue() == null) {
                    myRecordCollection = new ArrayList<String>(1);
                    myRecordCollection.add(id);
                } else {
                    myRecordCollection = (ArrayList<String>) mutableData.getValue();
                    myRecordCollection.add(id);
                }

                mutableData.setValue(myRecordCollection);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }

}
