package com.muttasimfuad.greenfo;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import Adapter.CustomAdapterTodayRoutine;
import ModalClass.RoutineInfo;


/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment3 extends Fragment {


    ListView mListView, crNoticeListView;
    ArrayList<RoutineInfo> arrayList;
    RoutineInfo routineInfo;
    CustomAdapterTodayRoutine adapter;

    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef, Cref;
    private String CurrentUserPhoneNo;
    ArrayList<String> crNoticeList = new ArrayList<>();
    //TextView tomorrowDateTIme;
    TextView tomorrowTclassTV;
    TextView userBatchInCrNotice;


    String Name;
    String Relation;
    String CourseNo;
    String PhoneNo;
    String c;
    String UserID;
    int dayNo;
    String SelectedDay;
    String userDept, userBatch, userShift, userSec;
    CalendarView calendarView;
    String[] dayList = {
            "Friday",
            "Saturday",
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday"
    };

    public TabFragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_fragment3, container, false);
        //return inflater.inflate(R.layout.fragment_tab_fragment1, container, false);

        //tomorrowDateTIme = view.findViewById(R.id.tomorrowDayTV);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {


            tomorrowTclassTV = view.findViewById(R.id.tomorrowTotalClassTV);

            userBatchInCrNotice = view.findViewById(R.id.TVstudentBatchInCRNotice2);

            mListView = view.findViewById(R.id.tab3ListView);
            crNoticeListView = view.findViewById(R.id.tab3CRListView);


            arrayList = new ArrayList<>();


            mAuth = FirebaseAuth.getInstance();
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            myRef = mFirebaseDatabase.getReference();
            FirebaseUser user = mAuth.getCurrentUser();
            CurrentUserPhoneNo = user.getPhoneNumber();

            calendarView = (CalendarView) view.findViewById(R.id.calendarView);

            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                    Calendar calendar = new GregorianCalendar(year, month
                            , dayOfMonth);
                    int result = calendar.get(Calendar.DAY_OF_WEEK);
                    switch (result) {
                        case Calendar.FRIDAY:
                            dayNo = 0;
                            break;
                        case Calendar.SATURDAY:
                            dayNo = 1;
                            break;
                        case Calendar.SUNDAY:
                            dayNo = 2;
                            break;
                        case Calendar.MONDAY:
                            dayNo = 3;
                            break;
                        case Calendar.TUESDAY:
                            dayNo = 4;
                            break;
                        case Calendar.WEDNESDAY:
                            dayNo = 5;
                            break;
                        case Calendar.THURSDAY:
                            dayNo = 6;
                            break;
                    }
                    arrayList.clear();
                    SelectedDay = dayList[dayNo];

                    //tomorrowTclassTV.setText("You have selected "+SelectedDay+" "+" day!");
                    DatabaseReference user_info_Ref = myRef.child("user_info").child(CurrentUserPhoneNo).child("courses").child(SelectedDay);

                    Query deleteQuery = user_info_Ref.orderByChild(SelectedDay);
                    deleteQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            showData(dataSnapshot);


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d("Item not found: ", "this item is not in the list");
                        }
                    });

                    DatabaseReference totalclassRef = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(SelectedDay);
                    totalclassRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Long num = dataSnapshot.getChildrenCount();
                            if (num < 2) {
                                c = "class";
                                ViewGroup.LayoutParams params = mListView.getLayoutParams();
                                params.height = 200;
                                mListView.setLayoutParams(params);
                                mListView.requestLayout();

                            } else {
                                c = "classes";

                            }
                            if (num == 0) {
                                mListView.setAdapter(null);
                                ViewGroup.LayoutParams params = mListView.getLayoutParams();
                                params.height = 0;
                                mListView.setLayoutParams(params);
                                mListView.requestLayout();
                            } else if (num == 2) {
                                ViewGroup.LayoutParams params = mListView.getLayoutParams();
                                params.height = 350;
                                mListView.setLayoutParams(params);
                                mListView.requestLayout();

                            } else if (num > 2) {
                                ViewGroup.LayoutParams params = mListView.getLayoutParams();
                                params.height = 420;
                                mListView.setLayoutParams(params);
                                mListView.requestLayout();

                            }
                            tomorrowTclassTV.setText("You have " + num + " " + c + " in that day!");

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                }
            });


            // FINDING USERS DEPARTMENT TO SHOW THEIR CR's NOTICE


            DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

            Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

            findBatchQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    showDataofUserBatch(dataSnapshot);
                    // FOUND USERS and ID DEPT...
                    try {
                        if (UserID.length() == 9) {
                           // if (UserID.substring(0, 3).equals(userBatch) || UserID.substring(0, 4).equals(userBatch)) {

                                try {
                                    userBatchInCrNotice.setText("Batch: " + userDept + " " + userBatch + " " + userShift + " " + userSec);

                                    final ArrayAdapter<String> adapterForCrNotice = new ArrayAdapter<String>(TabFragment3.this.getActivity(), android.R.layout.simple_list_item_1, crNoticeList);
                                    crNoticeListView.setAdapter(adapterForCrNotice);
                                    Cref = FirebaseDatabase.getInstance().getReference().child("batches").child(userDept).child(userBatch).child(userShift).child("notifn_cr").child(userSec).child("month_tab_cr").child("m1");
                                    Cref.addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            crNoticeList.add(dataSnapshot.getValue(String.class));
                                            adapterForCrNotice.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                                            crNoticeList.remove(dataSnapshot.getValue(String.class));
                                            adapterForCrNotice.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                // DONE of Setting Up AppUser's CR NOTICE
                         //   }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        crNoticeListView.setVisibility(View.INVISIBLE);
                    }


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i("Item not found: ", "this item is not in the list");
                }
            });


        }
        return view;
    }


    private void showData(DataSnapshot dataSnapshot) {


        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            routineInfo = new RoutineInfo();
            Name = String.valueOf(value.get("time"));
            Relation = String.valueOf(value.get("course_code"));
            CourseNo = String.valueOf(value.get("name"));
            PhoneNo = String.valueOf(value.get("room"));
            routineInfo = new RoutineInfo(Name, Relation, CourseNo, PhoneNo);
        }
        arrayList.add(routineInfo);

        if (getActivity()!=null) {
            adapter = new CustomAdapterTodayRoutine(TabFragment3.this.getActivity(), arrayList);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    private void showDataofUserBatch(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            userDept = String.valueOf(value.get("department"));
            userBatch = String.valueOf(value.get("batch"));
            userShift = String.valueOf(value.get("shift"));
            userSec = String.valueOf(value.get("sec"));
            UserID = String.valueOf(value.get("id"));

        }
    }


}