package com.muttasimfuad.greenfo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.muttasimfuad.greenfo.app.Config;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;


public class Activity_TermsOfService extends AppCompatActivity {

    Button Accept,Decline;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_activity__terms_of_service);

        Accept = (Button) findViewById(R.id.acceptBTN);
        Decline= (Button) findViewById(R.id.declineBTN);


        Accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences pref = getApplication().getSharedPreferences(Config.SHOW_TERMS, 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("show_terms", "NO");
                editor.commit();
                finish();

            }
        });

        Decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences pref = getApplication().getSharedPreferences(Config.SHOW_TERMS, 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("show_terms", "YES");
                editor.commit();
                finish();
                System.exit(0);

            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
        System.exit(0);
    }


}
