package com.muttasimfuad.greenfo;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.muttasimfuad.greenfo.app.Config;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdminFragment extends Fragment {

    private View mRootVIew;
    private FragmentActivity myContext;
    private Button help,troll,crush,admire,notice,checkNotice,adminlogoutBTN;
    private String RealNameOfMod;


    public AdminFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootVIew = inflater.inflate(R.layout.fragment_admin, container, false);

        try {
            getActivity().setTitle("Main Admin | GreenFo");
        } catch (Exception e) {
            e.printStackTrace();
        }
        RealNameOfMod = "Main Admin";

        help = (Button) mRootVIew.findViewById(R.id.helpzoneBTN);
        troll = (Button) mRootVIew.findViewById(R.id.trollBTN);
        crush = (Button) mRootVIew.findViewById(R.id.crushBTN);
        admire = (Button) mRootVIew.findViewById(R.id.admireBTN);
        notice = (Button) mRootVIew.findViewById(R.id.noticeBTN);
        checkNotice = (Button) mRootVIew.findViewById(R.id.checkNoticeBTN);
        adminlogoutBTN = (Button) mRootVIew.findViewById(R.id.adminlogoutBTN);


        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                HelpZoneFragmentPendingPost helpZoneFragmentPendingPost = new HelpZoneFragmentPendingPost();

                Bundle args = new Bundle();
                args.putString("ModName", RealNameOfMod);
                helpZoneFragmentPendingPost.setArguments(args);

                FragmentTransaction fragmentTransaction =
                        myContext.getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                fragmentTransaction.commit();


            }
        });
        troll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GubTrollFragmentPendingPost helpZoneFragmentPendingPost = new GubTrollFragmentPendingPost();

                Bundle args = new Bundle();
                args.putString("ModName", RealNameOfMod);
                helpZoneFragmentPendingPost.setArguments(args);

                FragmentTransaction fragmentTransaction =
                        myContext.getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                fragmentTransaction.commit();

            }
        });
        crush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CrushConfessionFragmentPendingPost helpZoneFragmentPendingPost = new CrushConfessionFragmentPendingPost();

                Bundle args = new Bundle();
                args.putString("ModName", RealNameOfMod);
                helpZoneFragmentPendingPost.setArguments(args);

                FragmentTransaction fragmentTransaction =
                        myContext.getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                fragmentTransaction.commit();

            }
        });
        admire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AdmirationPostFragmentPendingPost helpZoneFragmentPendingPost = new AdmirationPostFragmentPendingPost();

                Bundle args = new Bundle();
                args.putString("ModName", RealNameOfMod);
                helpZoneFragmentPendingPost.setArguments(args);

                FragmentTransaction fragmentTransaction =
                        myContext.getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                fragmentTransaction.commit();

            }
        });
        checkNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NoticeBoardPendingPost noticeBoardPendingPost = new NoticeBoardPendingPost();

                Bundle args = new Bundle();
                args.putString("ModName", RealNameOfMod);
                noticeBoardPendingPost.setArguments(args);

                FragmentTransaction fragmentTransaction =
                        myContext.getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, noticeBoardPendingPost);
                fragmentTransaction.commit();

            }
        });

        adminlogoutBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences pref = getActivity().getSharedPreferences(Config.ALLOW_ADMIN_LOGIN, 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("allow_admin_login", "NO");
                editor.commit();

                CRnDevLoginActivity cRnDevLoginActivity = new CRnDevLoginActivity();
                FragmentTransaction fragmentTransaction =
                        getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, cRnDevLoginActivity);
                fragmentTransaction.commit();
            }
        });


        notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UploadNotice.class);
                startActivity(intent);
            }
        });





        return mRootVIew;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    HomeFragment helpZoneFragmentPendingPost = new HomeFragment();


                    FragmentTransaction fragmentTransaction =
                            myContext.getSupportFragmentManager().beginTransaction();

                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                    fragmentTransaction.commit();





                    return true;

                }

                return false;
            }
        });

        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



}
