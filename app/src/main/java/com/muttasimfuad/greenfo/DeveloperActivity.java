package com.muttasimfuad.greenfo;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DeveloperActivity extends AppCompatActivity {
    Button fuadFb, grpGreenFoBTN,sendFeedback;
    ImageButton closeBTN;
    TextView bitspider;
    ImageView fuadImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

        fuadFb = (Button) findViewById(R.id.fuadfbIdBTN);
        grpGreenFoBTN = (Button) findViewById(R.id.fbGroup);
        closeBTN = (ImageButton) findViewById(R.id.closeme);
        bitspider = (TextView) findViewById(R.id.bitspider);
        fuadImage = (ImageView) findViewById(R.id.fuadIMG);
        //sendFeedback=(Button) findViewById(R.id.feedbanckBTN);

        TextView tv = (TextView) findViewById(R.id.desApp);
        tv.setText(Html.fromHtml(getString(R.string.desofApp)));
        tv.setMovementMethod(LinkMovementMethod.getInstance());


        fuadFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {     //left out the profile id on purpose...
                    Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/100000472464955"));
                    startActivity(facebook);

                } catch (Exception e) {
                    Uri uri = Uri.parse("https://www.facebook.com/Shovon20");
                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW, uri), "Open Fuad Shovon's Facebook ID Using: "));
                }
            }
        });


        grpGreenFoBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {     //left out the profile id on purpose...
                    Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://group/298534307304196/"));
                    startActivity(facebook);

                } catch (Exception e) {
                    Uri uri = Uri.parse("https://www.facebook.com/groups/298534307304196");
                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW, uri), "Open GreenFo's Facebook Group Using: "));
                }
            }
        });

//        sendFeedback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {     //left out the profile id on purpose...
//                    Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://group/298534307304196/"));
//                    startActivity(facebook);
//
//                } catch (Exception e) {
//                    Uri uri = Uri.parse("https://www.facebook.com/groups/298534307304196");
//                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW, uri), "Open GreenFo's Facebook Group Using: "));
//                }
//            }
//        });

        closeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bitspider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {     //left out the profile id on purpose...
                    Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/501392203248927"));
                    startActivity(facebook);

                } catch (Exception e) {
                    Uri uri = Uri.parse("https://www.facebook.com/BitSpiderLtd");
                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW, uri), "Open Bit-Spider's Facebook Page Using: "));
                }
            }
        });

        fuadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == fuadImage.getId()) {
                    //Toast.makeText(context, "ImageView Clicked!", Toast.LENGTH_LONG).show();

                    String imageUri = getURLForResource(R.drawable.mrfuad);
                    Intent i = new Intent(DeveloperActivity.this, FullscreenImageView.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("purl", imageUri);
                    startActivity(i);
                }
            }
        });
    }

    public String getURLForResource (int resourceId) {
        return Uri.parse("android.resource://"+R.class.getPackage().getName()+"/" +resourceId).toString();
    }
}
