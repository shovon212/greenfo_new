package com.muttasimfuad.greenfo.ui.activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.muttasimfuad.greenfo.R;
import com.muttasimfuad.greenfo.SeeMe;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.models.Comment;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.models.User;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;
import com.muttasimfuad.greenfo.utils.Constants;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PostActivityNotice extends AppCompatActivity implements View.OnClickListener {

    private static final String BUNDLE_COMMENT = "comment";
    private Post mPost;
    private EditText mCommentEditTextView;
    private Comment mComment;

    //new
    private ValueEventListener mUserValueEventListener;
    private DatabaseReference mUserRef;

    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseAuth mAuthx ;
    private FirebaseUser mFirebaseUser ;
    String  MyName,pp;

    Calendar calander;
    SimpleDateFormat simpledateformat;
    String Date,comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_notice);

        calander = Calendar.getInstance();
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date = simpledateformat.format(calander.getTime());

        if (savedInstanceState != null) {
            mComment = (Comment) savedInstanceState.getSerializable(BUNDLE_COMMENT);
        }

        Intent intent = getIntent();
        mPost = (Post) intent.getSerializableExtra(Constants.EXTRA_POST);

        //new
        mAuthx=FirebaseAuth.getInstance();
        mFirebaseUser = mAuthx.getCurrentUser();
        if (mFirebaseUser != null) {
            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
        }
        mUserValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {

                    AppUser appUser = dataSnapshot.getValue(AppUser.class);
                    if(appUser.getPropicurl() == null){
                        pp= "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                    }else pp= appUser.getPropicurl();

                    MyName=appUser.getUsername();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };



        init();
        initPost();
        initCommentSection();
    }




    private void initCommentSection() {
        RecyclerView commentRecyclerView = (RecyclerView) findViewById(R.id.comment_recyclerview);
        commentRecyclerView.setLayoutManager(new LinearLayoutManager(PostActivityNotice.this));

        FirebaseRecyclerAdapter<Comment, CommentHolder> commentAdapter = new FirebaseRecyclerAdapter<Comment, CommentHolder>(
                Comment.class,
                R.layout.row_comment,
                CommentHolder.class,
                FirebaseUtils.getCommentRefNoticeBoard(mPost.getPostId())
        ) {
            @Override
            protected void populateViewHolder(CommentHolder viewHolder, final Comment model, int position) {
                viewHolder.setUsername(model.getUser());
                viewHolder.setComment(model.getComment());
                viewHolder.setTime(DateUtils.getRelativeTimeSpanString(model.getTimeCreated()));

                Glide.with(PostActivityNotice.this)
                       // .load("https://ibb.co/jHi68m")
                        .load(model.getCommenterphotourl())
                        .into(viewHolder.commentOwnerDisplay);

                viewHolder.commentOwnerDisplay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = model.getCommenterphotourl();
                        String name = model.getUser();
                        String batch = " ";
                        Intent i = new Intent(getApplicationContext(), SeeMe.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("propic", url);
                        i.putExtra("name", name);
                        i.putExtra("batch", batch);
                        getApplicationContext().startActivity(i);
                    }
                });

                viewHolder.usernameTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = model.getCommenterphotourl();
                        String name = model.getUser();
                        String batch = " ";
                        Intent i = new Intent(getApplicationContext(), SeeMe.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("propic", url);
                        i.putExtra("name", name);
                        i.putExtra("batch", batch);
                        getApplicationContext().startActivity(i);
                    }
                });

            }
        };

        commentRecyclerView.setAdapter(commentAdapter);
    }

    private void initPost() {

        TextView postTimeCreatedTextView = (TextView) findViewById(R.id.tv_time);
        ImageView postDisplayImageView = (ImageView) findViewById(R.id.iv_post_display);
        LinearLayout postLikeLayout = (LinearLayout) findViewById(R.id.like_layout);
        LinearLayout postCommentLayout = (LinearLayout) findViewById(R.id.comment_layout);
        TextView postNumLikesTextView = (TextView) findViewById(R.id.tv_likes);
        TextView postNumCommentsTextView = (TextView) findViewById(R.id.tv_comments);
        TextView postTextTextView = (TextView) findViewById(R.id.tv_post_text);
        TextView postTitleTextView = (TextView) findViewById(R.id.tv_title_text);



        postTimeCreatedTextView.setText(DateUtils.getRelativeTimeSpanString(mPost.getTimeCreated()));
        postTextTextView.setText(mPost.getPostText());
        postTitleTextView.setText(mPost.getPostTitle().toUpperCase());
        postNumLikesTextView.setText(String.valueOf(mPost.getNumLikes()));


        if(mPost.getNumComments()<2){
            comment = " Comment";
        }else comment = " Comments";

        postNumCommentsTextView.setText(String.valueOf(mPost.getNumComments())+ comment);

        if (mPost.getPostImageUrl() != null) {
            postDisplayImageView.setVisibility(View.VISIBLE);
//            StorageReference storageReference = FirebaseStorage.getInstance().getReference(mPost.getPostImageUrl());

//            Glide.with(PostActivityNotice.this)
//                    .using(new FirebaseImageLoader())
//                    .load(storageReference)
//                    .into(postDisplayImageView);

            Glide.with(PostActivityNotice.this).load(mPost.getPostImageUrl()).into(postDisplayImageView);

           // postDisplayImageView.setOnClickListener(this);


        } else {
            postDisplayImageView.setImageBitmap(null);
            postDisplayImageView.setVisibility(View.GONE);
        }



    }

    private void init() {
        mCommentEditTextView = (EditText) findViewById(R.id.et_comment);
        findViewById(R.id.iv_send).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send:
                if (mFirebaseUser != null) {
                    sendComment();
                }else  Toast.makeText(this, "You need to login to comment!", Toast.LENGTH_LONG).show();

//            case R.id.iv_post_display:
//                sendImage();
        }
    }


    private void sendComment() {

        final String uid = FirebaseUtilsForUserInfo.getCurrentUser().getUid();//FirebaseUtils.getUid(); //fix
        String strComment = mCommentEditTextView.getText().toString();

        if(strComment.isEmpty()){
            mCommentEditTextView.setHint("Type a comment first !!!!!");
        }else {



            final ProgressDialog progressDialog = new ProgressDialog(PostActivityNotice.this);
            progressDialog.setMessage("Sending comment..");
            progressDialog.setCancelable(true);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            mComment = new Comment();

            mComment.setCommentId(uid);
            mComment.setComment(strComment);
            mComment.setTimeCreated(System.currentTimeMillis());
            mComment.setCommenterphotourl(pp);

            mComment.setUser(MyName);
            FirebaseUtils.getUserRefNoticeBoard(FirebaseUtils.getCurrentUser().getPhoneNumber())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User user = dataSnapshot.getValue(User.class);
                            long value = Long.parseLong("99999999999999999");
                            long number = value-System.currentTimeMillis();
                            FirebaseUtils.getCommentRefNoticeBoard(mPost.getPostId())
                                    .child(number+"_"+Date.replace(".", "_")+"_"+user.getUser()+mPost.getUser().getEmail())
                                    .setValue(mComment);

                            FirebaseUtils.getNoticeRefFinal().child(mPost.getPostId())
                                    .child(Constants.NUM_COMMENTS_KEY)
                                    .runTransaction(new Transaction.Handler() {
                                        @Override
                                        public Transaction.Result doTransaction(MutableData mutableData) {
                                            long num = (long) mutableData.getValue();
                                            mutableData.setValue(num + 1);
                                            return Transaction.success(mutableData);
                                        }

                                        @Override
                                        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                            progressDialog.dismiss();
                                           // FirebaseUtils.addToMyRecord(Constants.COMMENTS_KEY, Date+mPost.getUser().getEmail());
                                        }
                                    });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            progressDialog.dismiss();
                        }
                    });
            mCommentEditTextView.setText("");
        }
    }


    public static class CommentHolder extends RecyclerView.ViewHolder {
        ImageView commentOwnerDisplay;
        TextView usernameTextView;
        TextView timeTextView;
        TextView commentTextView;

        public CommentHolder(View itemView) {
            super(itemView);
            commentOwnerDisplay = (ImageView) itemView.findViewById(R.id.iv_comment_owner_display);
            usernameTextView = (TextView) itemView.findViewById(R.id.tv_username);
            timeTextView = (TextView) itemView.findViewById(R.id.tv_time);
            commentTextView = (TextView) itemView.findViewById(R.id.tv_comment);
        }

        public void setUsername(String username) {
            usernameTextView.setText(username);
        }

        public void setTime(CharSequence time) {
            timeTextView.setText(time);
        }

        public void setComment(String comment) {
            commentTextView.setText(comment);
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(BUNDLE_COMMENT, mComment);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onStart() {
        super.onStart();
        //mAuthx.addAuthStateListener(mAuthStateListener);
        if (mUserRef != null) {
            mUserRef.addValueEventListener(mUserValueEventListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthStateListener != null)
            mAuthx.removeAuthStateListener(mAuthStateListener);
        if (mUserRef != null)
            mUserRef.removeEventListener(mUserValueEventListener);
    }
}
