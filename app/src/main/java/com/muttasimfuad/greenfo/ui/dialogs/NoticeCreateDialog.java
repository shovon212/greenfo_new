package com.muttasimfuad.greenfo.ui.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.muttasimfuad.greenfo.R;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.models.User;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;
import com.muttasimfuad.greenfo.utils.Constants;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static androidx.appcompat.app.AppCompatActivity.RESULT_OK;
import static com.muttasimfuad.greenfo.utils.Constants.POST_IMAGES_Notice;
import static com.muttasimfuad.greenfo.utils.FirebaseUtils.getImageSRefNoticeBoard;


public class NoticeCreateDialog extends DialogFragment implements View.OnClickListener {
    private static final int RC_PHOTO_PICKER = 1;
    private Post mPost;
    private ProgressDialog mProgressDialog;
    private Uri mSelectedUri;
    private ImageView mPostDisplay;
    private View mRootView;
    private EditText noticeDescriptionET;
    private EditText noticeTitleET;

    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private ValueEventListener mUserValueEventListener;
    private String userFullBatch,pp,MyName;
    private FirebaseUser mFirebaseUser ;
    private FirebaseAuth mAuthx ;
    private DatabaseReference mUserRef;

    //new
    Calendar calander;
    SimpleDateFormat simpledateformat;
    String Date;



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        mPost = new Post();
        mProgressDialog = new ProgressDialog(getContext());

        mRootView = getActivity().getLayoutInflater().inflate(R.layout.create_notice_dialog, null);
        mPostDisplay = (ImageView) mRootView.findViewById(R.id.post_dialog_display);
        mRootView.findViewById(R.id.post_dialog_send_imageview).setOnClickListener(this);
        mRootView.findViewById(R.id.post_dialog_select_imageview).setOnClickListener(this);
        builder.setTitle("Send notice to admin for approval");
        builder.setView(mRootView);
        mPostDisplay.setVisibility(View.GONE);

        mAuthx = FirebaseAuth.getInstance();
        mFirebaseUser = mAuthx.getCurrentUser();
        if (mFirebaseUser != null) {
            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
        }


        FirebaseUtils.getUserDetailsRef(FirebaseUtils.getCurrentUser().getPhoneNumber())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        AppUser user = dataSnapshot.getValue(AppUser.class);

                        userFullBatch= user.getDepartment()+"_"+user.getBatch()+"_"+user.getShift()+"_"+user.getSec();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        mUserValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {

                    AppUser appUser = dataSnapshot.getValue(AppUser.class);
                    if(appUser.getPropicurl() == null){
                        pp= "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                    }
                    //else pp= "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                    else pp= appUser.getPropicurl();

                    MyName = appUser.getUsername();
                    User user = new User();
                    user.setUser(MyName);
                    user.setPhotoUrl(pp);
                    user.setUid(mFirebaseUser.getUid());
                    user.setEmail(mFirebaseUser.getPhoneNumber());
                    user.setUserFullBatch(userFullBatch);


                    FirebaseUtils.getUserRefNoticeBoard(mAuthx.getCurrentUser().getPhoneNumber())
                            .setValue(user, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    mFirebaseUser = mAuthx.getCurrentUser();
                                    //finish();
                                }
                            });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        return builder.create();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.post_dialog_send_imageview:
                sendPost();
                break;
            case R.id.post_dialog_select_imageview:
                selectImage();
                break;
        }
    }

    private void sendPost() {
        mProgressDialog.setMessage("Sending notice...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();

        calander = Calendar.getInstance();
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date = simpledateformat.format(calander.getTime());
        


        FirebaseUtils.getUserRefNoticeBoard(FirebaseUtils.getCurrentUser().getPhoneNumber())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        long value = Long.parseLong("99999999999999999");
                        long number = value-System.currentTimeMillis();
                        final String postId = number+"_"+Date.replace(".", "_")+"_"+user.getUser()+"_"+user.getUserFullBatch()+FirebaseUtils.getCurrentUser().getPhoneNumber();//FirebaseUtils.getUid();//fix
                        noticeDescriptionET = (EditText) mRootView.findViewById(R.id.notice_desc_edittext);
                        noticeTitleET = (EditText) mRootView.findViewById(R.id.notice_titleET);
                        String text = noticeDescriptionET.getText().toString();
                        String title = noticeTitleET.getText().toString();

                        mPost.setUser(user);
                        mPost.setNumComments(0);
                        mPost.setNumLikes(0);
                        mPost.setTimeCreated(System.currentTimeMillis());
                        mPost.setPostId(postId);
                        mPost.setPostText(text);
                        mPost.setPostTitle(title);

                        if (mSelectedUri != null) {
                            getImageSRefNoticeBoard()
                                    //.child(mSelectedUri.getLastPathSegment())
                                    .child(postId)
                                    .putFile(mSelectedUri)
                                    .addOnSuccessListener(getActivity(),
                                            new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                @Override
                                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                                    //StorageReference photoRef2 = getImageSRefNoticeBoard().child(mSelectedUri.getLastPathSegment());
                                                    StorageReference photoRef =getImageSRefNoticeBoard().child("/" + postId);
                                                    // Download file From Firebase Storage
                                                    photoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                        @Override
                                                        public void onSuccess(Uri downloadPhotoUrl) {
                                                            //Now play with downloadPhotoUrl
                                                            //Store data into Firebase Realtime Database
                                                            String url = downloadPhotoUrl.toString();
                                                            //String url = Constants.POST_IMAGES + "/" + postId;
                                                            Log.i("url",url);
                                                            mPost.setPostImageUrl(url);
                                                            addToMyPostList(postId);
                                                        }
                                                    });

                                                    //String url = Constants.POST_IMAGES + "/" + postId; //mSelectedUri.getLastPathSegment();



                                                    Toast.makeText(getContext(),"Your notice has been Submitted & waiting for approval!",Toast.LENGTH_LONG).show();
                                                    noticeDescriptionET.setText("");
                                                    mPostDisplay.setImageResource(0);

                                                    mProgressDialog.dismiss();
                                                    getDialog().dismiss();

                                                }
                                            });
                        } else {
                            addToMyPostList(postId);
                            noticeDescriptionET.setText("");
                            noticeTitleET.setText("");
                            mPostDisplay.setImageResource(0);
                            mProgressDialog.dismiss();
                            getDialog().dismiss();
                            Toast.makeText(getContext(),"Your notice has been Submitted & waiting for approval!",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mProgressDialog.dismiss();
                    }
                });
    }

    private void addToMyPostList(String postId) {
        FirebaseUtils.getNoticeRef().child(postId)
                .setValue(mPost);
    }

    private void selectImage() {
        mPostDisplay.setVisibility(View.VISIBLE);
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_PHOTO_PICKER) {
            if (resultCode == RESULT_OK) {
                mSelectedUri = data.getData();
                mPostDisplay.setImageURI(mSelectedUri);
            }else mPostDisplay.setVisibility(View.GONE);
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        //mAuthx.addAuthStateListener(mAuthStateListener);
        if (mUserRef != null) {
            mUserRef.addValueEventListener(mUserValueEventListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthStateListener != null)
            mAuthx.removeAuthStateListener(mAuthStateListener);
        if (mUserRef != null)
            mUserRef.removeEventListener(mUserValueEventListener);
    }
}
