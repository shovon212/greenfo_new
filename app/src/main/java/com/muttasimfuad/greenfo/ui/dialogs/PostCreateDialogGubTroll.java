package com.muttasimfuad.greenfo.ui.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;
import com.muttasimfuad.greenfo.R;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.models.User;
import com.muttasimfuad.greenfo.utils.Constants;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static androidx.appcompat.app.AppCompatActivity.RESULT_OK;

/**
 * Created by brad on 2017/02/05.
 */

public class PostCreateDialogGubTroll extends DialogFragment implements View.OnClickListener {
    private static final int RC_PHOTO_PICKER = 1;
    private Post mPost;
    private ProgressDialog mProgressDialog;
    private Uri mSelectedUri;
    private ImageView mPostDisplay;
    private View mRootView;
    private TextView postDialogTextView;

    //new

    Calendar calander;
    SimpleDateFormat simpledateformat;
    String Date;



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        mPost = new Post();
        mProgressDialog = new ProgressDialog(getContext());

        mRootView = getActivity().getLayoutInflater().inflate(R.layout.create_post_dialog, null);
        mPostDisplay = (ImageView) mRootView.findViewById(R.id.post_dialog_display);
        mRootView.findViewById(R.id.post_dialog_send_imageview).setOnClickListener(this);
        mRootView.findViewById(R.id.post_dialog_select_imageview).setOnClickListener(this);
        builder.setView(mRootView);
        return builder.create();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.post_dialog_send_imageview:
                sendPost();
                break;
            case R.id.post_dialog_select_imageview:
                selectImage();
                break;
        }
    }

    private void sendPost() {
        mProgressDialog.setMessage("Sending post...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();

        calander = Calendar.getInstance();
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date = simpledateformat.format(calander.getTime());
        
        


        FirebaseUtils.getUserRefGubTroll(FirebaseUtils.getCurrentUser().getPhoneNumber())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        long value = Long.parseLong("99999999999999999");
                        long number = value-System.currentTimeMillis();
                        final String postId = number+"_"+Date.replace(".", "_")+"_"+user.getUser()+"_"+user.getUserFullBatch()+FirebaseUtils.getCurrentUser().getPhoneNumber();//FirebaseUtils.getUid();//fix
                        postDialogTextView = (TextView) mRootView.findViewById(R.id.post_dialog_edittext);
                        String text = postDialogTextView.getText().toString();

                        mPost.setUser(user);
                        mPost.setNumComments(0);
                        mPost.setNumLikes(0);
                        mPost.setTimeCreated(System.currentTimeMillis());
                        mPost.setPostId(postId);
                        mPost.setPostText(text);

                        if (mSelectedUri != null) {
                            FirebaseUtils.getImageSRefGubTroll()
                                    //.child(mSelectedUri.getLastPathSegment())
                                    .child(postId)
                                    .putFile(mSelectedUri)
                                    .addOnSuccessListener(getActivity(),
                                            new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                @Override
                                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                    String url = Constants.POST_IMAGES_GUB_TROLL + "/" + postId; //mSelectedUri.getLastPathSegment();
                                                    mPost.setPostImageUrl(url);
                                                    addToMyPostList(postId);
                                                    Toast.makeText(getContext(),"Your Post has been Submitted & waiting for approve!",Toast.LENGTH_LONG).show();
                                                    postDialogTextView.setText("");
                                                    mPostDisplay.setImageResource(0);
                                                    mProgressDialog.dismiss();
                                                    getDialog().dismiss();

                                                }
                                            });
                        } else {
                            addToMyPostList(postId);
                            postDialogTextView.setText("");
                            mPostDisplay.setImageResource(0);
                            mProgressDialog.dismiss();
                            getDialog().dismiss();
                            Toast.makeText(getContext(),"Your Post has been Submitted & waiting for approve!",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mProgressDialog.dismiss();
                    }
                });
    }

    private void addToMyPostList(String postId) {
        FirebaseUtils.getPostRefGubTroll().child(postId)
                .setValue(mPost);
//        FirebaseUtils.getMyPostRef().child(postId).setValue(true)
//                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        mProgressDialog.dismiss();
//                        dismiss();
//                    }
//                });
//
//        FirebaseUtils.addToMyRecordFinal(Constants.POST_KEY, postId);
    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_PHOTO_PICKER) {
            if (resultCode == RESULT_OK) {
                mSelectedUri = data.getData();
                mPostDisplay.setImageURI(mSelectedUri);
            }
        }
    }
}
