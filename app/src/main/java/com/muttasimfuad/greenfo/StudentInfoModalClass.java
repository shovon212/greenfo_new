package com.muttasimfuad.greenfo;

/**
 * Created by Fuad on 04-Feb-18.
 */

public class StudentInfoModalClass {

    public String userDept, userBatch, userShift, UserID, UserGender, MyName;


    public String getUserDept() {
        return userDept;
    }

    public void setUserDept(String userDept) {
        this.userDept = userDept;
    }

    public String getUserBatch() {
        return userBatch;
    }

    public void setUserBatch(String userBatch) {
        this.userBatch = userBatch;
    }

    public String getUserShift() {
        return userShift;
    }

    public void setUserShift(String userShift) {
        this.userShift = userShift;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserGender() {
        return UserGender;
    }

    public void setUserGender(String userGender) {
        UserGender = userGender;
    }

    public String getMyName() {
        return MyName;
    }

    public void setMyName(String myName) {
        MyName = myName;
    }
}

