package com.muttasimfuad.greenfo.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.muttasimfuad.greenfo.DeveloperActivity;
import com.muttasimfuad.greenfo.FullscreenImageView;
import com.muttasimfuad.greenfo.GroupChat;
import com.muttasimfuad.greenfo.MyProfileActivity;
import com.muttasimfuad.greenfo.R;
import com.muttasimfuad.greenfo.SeeMe;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;

import java.util.Map;

import layout.EditProfileActivity;

public class MessageAdapter extends FirebaseListAdapter<ChatMessage> {

    private GroupChat activity;
    private String Power;


    public MessageAdapter(GroupChat activity, Class<ChatMessage> modelClass, int modelLayout, DatabaseReference ref) {
        super(activity, modelClass, modelLayout, ref);
        this.activity = activity;

    }

    @Override
    protected void populateView(View v, final ChatMessage model, int position) {
        TextView messageText = (TextView) v.findViewById(R.id.message_text);
        TextView messageUser = (TextView) v.findViewById(R.id.message_user);
        TextView messageTime = (TextView) v.findViewById(R.id.message_time);
        final ImageView msgOwnerDisplayImageView = (ImageView) v.findViewById(R.id.iv_post_owner_display);
        final ImageView powerIcon = (ImageView) v.findViewById(R.id.powerIcon);

        messageText.setText(model.getMessageText());
        messageUser.setText(model.getMessageUserName());

        // Format the date before showing it
        messageTime.setText(DateFormat.format("dd-MM-yyyy (h:mm:ss a)", model.getMessageTime()));

        try {
            Glide.with(activity)
                    .load(model.getPp())
                    .into(msgOwnerDisplayImageView);

            msgOwnerDisplayImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = model.getPp();
                    String name = model.getMessageUserName();
                    String batch = model.getUserbatchinfo();
                    Intent i = new Intent(activity, SeeMe.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("propic", url);
                    i.putExtra("name", name);
                    i.putExtra("batch", batch);
                    activity.startActivity(i);

                }
            });

            messageUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = model.getPp();
                    String name = model.getMessageUserName();
                    String batch = model.getUserbatchinfo();
                    Intent i = new Intent(activity, SeeMe.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("propic", url);
                    i.putExtra("name", name);
                    i.putExtra("batch", batch);
                    activity.startActivity(i);

                }
            });

            messageText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = model.getPp();
                    String name = model.getMessageUserName();
                    String batch = model.getUserbatchinfo();
                    Intent i = new Intent(activity, SeeMe.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("propic", url);
                    i.putExtra("name", name);
                    i.putExtra("batch", batch);
                    activity.startActivity(i);

                }
            });

            messageTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = model.getPp();
                    String name = model.getMessageUserName();
                    String batch = model.getUserbatchinfo();
                    Intent i = new Intent(activity, SeeMe.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("propic", url);
                    i.putExtra("name", name);
                    i.putExtra("batch", batch);
                    activity.startActivity(i);

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        // POST DELETE ADMIN START

        powerIcon.setVisibility(View.GONE);

        try {

            DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("verified_users").child(model.getMessageUser());
            Query ModeratorQuery = crpassPathModerator.orderByChild(model.getMessageUser());

            ModeratorQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ModeratorData(dataSnapshot);

                    if (Power.equals("v")) {
                        powerIcon.setVisibility(View.VISIBLE);
                        powerIcon.setBackgroundResource(R.drawable.ic_verified_account32);
                    }else if (Power.equals("d")) {
                        powerIcon.setVisibility(View.VISIBLE);
                        powerIcon.setBackgroundResource(R.drawable.ic__crown);
                    }  else {
                        powerIcon.setVisibility(View.GONE);

                    }


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        // END of delete admin code


    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        try {
            ChatMessage chatMessage = getItem(position);
            if (chatMessage.getMessageUserId().equals(activity.getLoggedInUserName()))
                view = activity.getLayoutInflater().inflate(R.layout.item_out_message, viewGroup, false);
            else
                view = activity.getLayoutInflater().inflate(R.layout.item_in_message, viewGroup, false);

            //generating view
            populateView(view, chatMessage, position);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public int getViewTypeCount() {
        // return the total number of view types. this value should never change
        // at runtime
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        // return a value between 0 and (getViewTypeCount - 1)
        return position % 2;
    }

    private void ModeratorData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            Power = String.valueOf(value.get("power"));
        }
    }



}
