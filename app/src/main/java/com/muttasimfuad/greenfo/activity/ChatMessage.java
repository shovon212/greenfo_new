package com.muttasimfuad.greenfo.activity;

/**
 * Created by Fuad on 12/9/2017.
 */

import java.util.Date;

public class ChatMessage {
    private String messageText;
    private String messageUser;
    private String messageUserId;
    private String messageUserName;
    private long messageTime;
    private String pp;
    private String msgID;
    private String userbatchinfo;


    public ChatMessage(String messageText, String messageUser, String messageUserId, String messageUserName, String pp, String msgId,String userbatchinfo) {
        this.messageText = messageText;
        this.messageUser = messageUser;
        messageTime = new Date().getTime();
        this.messageUserId = messageUserId;
        this.messageUserName=messageUserName;
        this.pp = pp;
        this.msgID=  msgId;
        this.userbatchinfo=userbatchinfo;
    }

    public ChatMessage(){

    }

    public String getUserbatchinfo() {
        return userbatchinfo;
    }

    public void setUserbatchinfo(String userbatchinfo) {
        this.userbatchinfo = userbatchinfo;
    }

    public String getMsgID() {
        return msgID;
    }

    public void setMsgID(String msgID) {
        this.msgID = msgID;
    }

    public String getPp() {
        return pp;
    }

    public void setPp(String pp) {
        this.pp = pp;
    }

    public String getMessageUserName() {
        return messageUserName;
    }

    public void setMessageUserName(String messageUserName) {
        this.messageUserName = messageUserName;
    }

    public String getMessageUserId() {
        return messageUserId;
    }

    public void setMessageUserId(String messageUserId) {
        this.messageUserId = messageUserId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }
}
