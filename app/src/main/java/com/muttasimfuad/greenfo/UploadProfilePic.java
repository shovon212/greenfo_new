package com.muttasimfuad.greenfo;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.muttasimfuad.greenfo.app.Config;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;

import java.io.IOException;
import java.util.Map;

import static com.muttasimfuad.greenfo.utils.FirebaseUtils.getImageSRefProPic;

public class UploadProfilePic extends AppCompatActivity {
    public static final String Storage_Path = "All_Profile_Pics/";

    public static final String Database_Path = "personal_info";

    // Creating button.
    Button UploadButton;
    ImageView ChooseButton;

    ImageView SelectImage;
    TextView WarningText;

    String CurrentUserPhoneNo;

    String userDept, userBatch, userShift, UserID, UserGender, MyName;

    Uri FilePathUri;

    StorageReference storageReference;
    DatabaseReference databaseReference;

    int Image_Request_Code = 7;

    ProgressDialog progressDialog;

    private ValueEventListener mUserValueEventListener;
    FirebaseUser mFirebaseUser;
    private FirebaseAuth mAuthx;
    private DatabaseReference mUserRef;
    private String nam, vId, dep, secc, gend, regId, shif, batc, url,Blood_group,donateBlood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_profile_pic);

        storageReference = FirebaseStorage.getInstance().getReference();
        mAuthx = FirebaseAuth.getInstance();
        mFirebaseUser = mAuthx.getCurrentUser();

        if (mFirebaseUser != null) {
            // mUserRef = FirebaseUtilsForUserInfo.getUserDpRef();
            //mUserRef = FirebaseUtilsForUserInfo.getUserPhnRef(mFirebaseUser.getPhoneNumber());
            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
        }

        mUserValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {

                    AppUser appUserR = dataSnapshot.getValue(AppUser.class);
                    nam = appUserR.getUsername();
                    dep = appUserR.getDepartment();
                    batc = appUserR.getBatch();
                    vId = appUserR.getId();
                    shif = appUserR.getShift();
                    secc = appUserR.getSec();
                    gend = appUserR.getGender();
                    regId = appUserR.getFCM_ID();
                    Blood_group= appUserR.getBlood_group();
                    donateBlood= appUserR.getDonate_blood();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        //Assign ID'S to button.
        ChooseButton = (ImageView) findViewById(R.id.post_dialog_select_imageview);
        UploadButton = (Button) findViewById(R.id.post_dialog_send_imageview);
        WarningText = (TextView) findViewById(R.id.warningTV);


        // Assign ID'S to image view.
        SelectImage = (ImageView) findViewById(R.id.post_dialog_display);

        // Assigning Id to ProgressDialog.
        progressDialog = new ProgressDialog(UploadProfilePic.this);


        // Adding click listener to Choose image button.
        ChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Creating intent.
                Intent intent = new Intent();

                // Setting intent type as image to select image from phone storage.
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Please Select Image"), Image_Request_Code);

            }
        });


        // Adding click listener to Upload image button.
        UploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (FilePathUri != null){


                    if (isNetworkConnected()) {


                        AppUser appUser = new AppUser();

                        appUser.setBatch(batc);
                        appUser.setDepartment(dep);
                        appUser.setGender(gend);
                        appUser.setId(vId);
                        appUser.setShift(shif);
                        appUser.setUsername(nam);
                        appUser.setSec(secc);
                        appUser.setBlood_group(Blood_group);
                        appUser.setDonate_blood(donateBlood);
                        appUser.setPropicurl(url);
                        appUser.setFCM_ID(regId);



                        FirebaseUtilsForUserInfo.getUserinfoRef()
                                .setValue(appUser, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                                        finish();
                                    }
                                });


                         startActivity(new Intent(UploadProfilePic.this, MainActivity.class));
                         finish();
                        Toast.makeText(getApplicationContext(), "Profile Picture Uploaded Successfully ", Toast.LENGTH_LONG).show();




                    } else {
                        Toast.makeText(UploadProfilePic.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                    }

            }else Toast.makeText(UploadProfilePic.this, "No Image Selected!", Toast.LENGTH_SHORT).show();


                //new

            }
        });

        // mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();







    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            try {

                // Getting selected image into Bitmap.
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), FilePathUri);

                // Setting up bitmap selected image into ImageView.
                SelectImage.setImageBitmap(bitmap);

                // After selecting image change choose button above text.
                //   ChooseButton.setText("Image Selected");

            } catch (IOException e) {

                e.printStackTrace();
            }

            UploadImageFileToFirebaseStorage();




        }
    }

    // Creating Method to get the selected image file Extension from File Path URI.
    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();

        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));

    }

    // Creating UploadImageFileToFirebaseStorage method to upload image on storage.
    public void UploadImageFileToFirebaseStorage() {

        // Checking whether FilePathUri Is empty or not.
        if (FilePathUri != null) {

            // Setting progressDialog Title.
            progressDialog.setTitle("Profile Pic is Uploading...");
            //WarningText.setText("Profile Pic is Uploading...");

            // Showing progressDialog.
            progressDialog.show();

            CurrentUserPhoneNo = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();

            // Creating second StorageReference.
            StorageReference storageReference2nd = storageReference.child(Storage_Path + CurrentUserPhoneNo + "." + GetFileExtension(FilePathUri));

            // Adding addOnSuccessListener to second StorageReference.
            storageReference2nd.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            // Getting image name from EditText and store into string variable.
                            //  String TempImageName = CurrentUserPhoneNo;  //update it

                            // Hiding the progressDialog after done uploading.


                            // Showing toast message after done uploading.
                            //Toast.makeText(getApplicationContext(), "Profile Picture Uploaded Successfully ", Toast.LENGTH_LONG).show();
                            //WarningText.setText("Profile Picture Uploaded Successfully!");

//                            url = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
//                            Log.i("pro pic url",url);

                            StorageReference photoRef = getImageSRefProPic().child("/"+ CurrentUserPhoneNo);
                            // Download file From Firebase Storage
                            storageReference2nd.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri downloadPhotoUrl) {
                                    //Now play with downloadPhotoUrl
                                    //Store data into Firebase Realtime Database
                                    url = downloadPhotoUrl.toString();
                                    //String url = Constants.POST_IMAGES + "/" + postId;
                                    Log.i("pro pic url",url);
                                }
                            });

                            displayFirebaseRegId();
                            Log.i("FCM ID:", regId);




                            progressDialog.dismiss();




                        }
                    })
                    // If something goes wrong .
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                            // Hiding the progressDialog.
                            progressDialog.dismiss();

                            // Showing exception erro message.
                            //Toast.makeText(UploadProfilePic.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                            WarningText.setText("Failed to upload! Select a picture less then 500 KB and try again please! You can use an app to reduce photo size. Go to google play store and search for 'Photo & Picture Resizer' by farluner apps");
                            WarningText.setTextColor(Color.RED);
                            // Toast.makeText(UploadProfilePic.this, "Failed to upload! Select a picture less then 500 KB and try again please.", Toast.LENGTH_LONG).show();
                        }
                    })

                    // On progress change upload time.
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            // Setting progressDialog Title.
                            progressDialog.setTitle("Sit tight! Picture is uploading!");

                        }
                    });
        } else {

            Toast.makeText(UploadProfilePic.this, "Use the camera button to upload profile pic.", Toast.LENGTH_LONG).show();


        }
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void showDataofUserBatch(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            userDept = String.valueOf(value.get("department"));
            MyName = String.valueOf(value.get("username"));
            userBatch = String.valueOf(value.get("batch"));
            userShift = String.valueOf(value.get("shift"));
            UserID = String.valueOf(value.get("id"));
            UserGender = String.valueOf(value.get("gender"));

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //mAuthx.addAuthStateListener(mAuthStateListener);
        if (mUserRef != null) {
            mUserRef.addValueEventListener(mUserValueEventListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mUserRef != null)
            mUserRef.removeEventListener(mUserValueEventListener);
    }

    private void displayFirebaseRegId() {
//        try {
//            SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
//            regId = pref.getString("regId", "Could Not Received");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        try {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                               // Log.w(TAG, "getInstanceId failed", task.getException());

                                return;
                            }

                            // Get new Instance ID token
                            regId = task.getResult().getToken();
                            Log.i("fcm token: ",regId);
                            storeRegIdInPref(regId);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            regId = "Could not received";
        }
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }


}

