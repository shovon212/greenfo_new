package com.muttasimfuad.greenfo;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class HomeTabAdminActivity extends AppCompatActivity {

    //TextView welcomeMsgforDV, crName, crPhone, crPass;
   // String CrDept, CrBatch, CrShift, OldName, OldPhone, OldPassword, Message;
    //ProgressBar weekPB, monthPB;
    ProgressBar homePB;

    Calendar calander;
    SimpleDateFormat simpledateformat;
    String Date;
    EditText postBoxForHomeET,NoticeNoHomeET;
    //EditText postBoxForWeekET, postBoxForMonthET, NoticeNoWeekET,NoticeNoMonthET;
   // String postTextofWeek, postTextofMonth;
    String postTextofHome;

    TextView homeResult ;
    Button  UploadNoticeBTN;
   // TextView weekResult, monthResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_tab_admin);

        //welcomeMsgforCR = (TextView) findViewById(R.id.welcomeMsgTV);

//        Intent startingIntent = getIntent();
//        Message = startingIntent.getStringExtra("crtotalbatch");
//        CrDept = startingIntent.getStringExtra("crdept");
//        CrBatch = startingIntent.getStringExtra("crbatch");
//        CrShift = startingIntent.getStringExtra("crshift");
//
//
//        crName = (TextView) findViewById(R.id.crnameInCRPanelTV);
//        crPhone = (TextView) findViewById(R.id.crPhoneInCRPanelTV);
//        crPass = (TextView) findViewById(R.id.crPassInCRPanelTV);

        homePB = (ProgressBar) findViewById(R.id.progressBarHOME);
//        weekPB = (ProgressBar) findViewById(R.id.progressBarWEEK);
//        monthPB = (ProgressBar) findViewById(R.id.progressBarMONTH);

        postBoxForHomeET = (EditText) findViewById(R.id.ETpostToHomeTab);
//        postBoxForWeekET = (EditText) findViewById(R.id.ETpostToWeekTab);
//        postBoxForMonthET = (EditText) findViewById(R.id.ETpostToMonthTab);
//        NoticeNoWeekET = (EditText) findViewById(R.id.ETnoticeNoWeek);
//        NoticeNoMonthET = (EditText) findViewById(R.id.ETnoticeNoMonth);
        NoticeNoHomeET = (EditText) findViewById(R.id.ETnoticeNoHome);

        UploadNoticeBTN = (Button) findViewById(R.id.BTNUploadNotice);

        homeResult = (TextView) findViewById(R.id.TVhomeResult);
//        weekResult = (TextView) findViewById(R.id.TVweekResult);
//        monthResult = (TextView) findViewById(R.id.TVmonthResult);


        homePB.setVisibility(View.INVISIBLE);
//        weekPB.setVisibility(View.INVISIBLE);
//        monthPB.setVisibility(View.INVISIBLE);


        UploadNoticeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeTabAdminActivity.this, UploadNotice.class));
            }
        });




    }

//    public void editCRinfoAction(View view) {
//        if (isNetworkConnected() == true) {
//            Intent intent = new Intent(CrPanelActivity.this, CRInfoActivity.class);
//            intent.putExtra("crdept", CrDept);
//            intent.putExtra("crbatch", CrBatch);
//            intent.putExtra("crshift", CrShift);
//            startActivity(intent);
//            finish();
//        }else {
//            Toast.makeText(CrPanelActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
//        }
//    }

    public void exitFromDVpanelAction(View view) {
        startActivity(new Intent(HomeTabAdminActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(HomeTabAdminActivity.this, MainActivity.class));
        finish();
    }

    public void BTNdvPostToHomeOnClickAction(View view) {
        homePB.setVisibility(View.VISIBLE);
        String NoticeNo = NoticeNoHomeET.getText().toString().trim();
        String PostTextHome=postBoxForHomeET.getText().toString().trim();
        if (isNetworkConnected() == true) {

            try {
                if(!TextUtils.isEmpty(NoticeNo) && !TextUtils.isEmpty(PostTextHome)) {


                    calander = Calendar.getInstance();
                    simpledateformat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
                    Date = simpledateformat.format(calander.getTime());

                    postTextofHome = "Notice: "+NoticeNo+" (Posted:"+Date +")"+"\n" +PostTextHome;

                    DatabaseReference setCrPostToHomePath = FirebaseDatabase.getInstance().getReference().child("Powership").child("Dev").child("notifn_dv").child("home_tab_dv").child("h1");

                    setCrPostToHomePath.child(NoticeNo).setValue(postTextofHome);
                    homePB.setVisibility(View.INVISIBLE);
                    homeResult.setText("Posted Successfully!");
                    postBoxForHomeET.setText("");
                    NoticeNoHomeET.setText("");
                }else {
                    homeResult.setText("Fill Up Both Box!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                 homeResult.setText("Not posted! poor network");
            }


        } else {
            Toast.makeText(HomeTabAdminActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            homePB.setVisibility(View.INVISIBLE);
        }


    }

//    public void BTNcrPostToMonthOnClickAction(View view) {
//        monthPB.setVisibility(View.VISIBLE);
//        String NoticeNo2 = NoticeNoMonthET.getText().toString().trim();
//        String PostTextMonth=postBoxForMonthET.getText().toString().trim();
//        if (isNetworkConnected() == true) {
//            try {
//                if(!TextUtils.isEmpty(NoticeNo2) && !TextUtils.isEmpty(PostTextMonth)) {
//                    calander = Calendar.getInstance();
//                    simpledateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
//                    Date = simpledateformat.format(calander.getTime());
//
//                    postTextofMonth = "Notice No: "+NoticeNo2+" (Posted:"+Date +")"+"\n" +PostTextMonth;
//
//                    DatabaseReference setCrPostToMonthPath = FirebaseDatabase.getInstance().getReference().child("batches").child(CrDept).child(CrBatch).child(CrShift).child("notifn_cr").child("month_tab_cr").child("m1");
//
//                    setCrPostToMonthPath.child(NoticeNo2).setValue(postTextofMonth);
//                    monthPB.setVisibility(View.INVISIBLE);
//                    monthResult.setText("Posted Successfully!");
//                    postBoxForMonthET.setText("");
//                    NoticeNoMonthET.setText("");
//                }else {
//                    monthResult.setText("Fill Up Both Box!");
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                monthResult.setText("Not posted! poor network");
//            }
//
//
//        } else {
//            Toast.makeText(CrPanelActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
//            monthPB.setVisibility(View.INVISIBLE);
//        }
//
//    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void BTNviewpostHomeAction(View view) {

        Intent intent = new Intent(HomeTabAdminActivity.this, ThisHomeDvPostActivity.class);
        startActivity(intent);
    }

//    public void BTNviewpostMonthAction(View view) {
//
//        Intent intent = new Intent(CrPanelActivity.this, ThisMonthCrPostActivity.class);
//        intent.putExtra("crdept", CrDept);
//        intent.putExtra("crbatch", CrBatch);
//        intent.putExtra("crshift", CrShift);
//        startActivity(intent);
//    }
}
