package com.muttasimfuad.greenfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.muttasimfuad.greenfo.app.Config;
import com.muttasimfuad.greenfo.models.AppUser;
import com.muttasimfuad.greenfo.oldutil.FirebaseUtilsForUserInfo;

import java.util.Map;

import layout.SignupActivity;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    static boolean calledAlready = false;

    //  FirebaseAuth auth;
    NavigationView navigationView;
    LinearLayout bottomBarLinearLayout;
    String CurrentUserPhoneNo, CurrentUserName, CurrentUserBatch;
    private static String gen;
    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference myRef;
    ImageView greenLogo;
    private ImageView mDisplayImageView;
    private Button profileEditBTN;
    private ImageButton uploadMyPicBTN;
    public ImageView myImg;

    GubianChecker gubianChecker;

    ImageButton homeImgBTN, GroupChatHome, noticeImgBTN, GumsLoginImgBTNinHome;


    String userDept, userBatch, userShift, UserID, UserGender, MyName, pp;
    FirebaseAuth mAuth;
    private TextView nav_user, nav_id;
    //new
    private ValueEventListener mUserValueEventListener;
    private DatabaseReference mUserRef;

    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseAuth mAuthx;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!calledAlready) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            calledAlready = true;
        }

        mAuthx = FirebaseAuth.getInstance();
        mFirebaseUser = mAuthx.getCurrentUser();

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8614904278210110~2305070735");


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {

                    // startActivity(new Intent(MainActivity.this, SignupActivity.class));
                    Toast.makeText(MainActivity.this, "Login to enjoy all hidden features of this app!", Toast.LENGTH_LONG).show();
                }
            }
        };


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        bottomBarLinearLayout = (LinearLayout) findViewById(R.id.bottomBar);
        bottomBarLinearLayout.setVisibility(View.VISIBLE);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        init();

        // Setting up Main Fragment Container
        HomeFragment homeFragment = new HomeFragment();
        FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, homeFragment);
        fragmentTransaction.commit();
        // End of setting Main Fragment Container


        try {
            View navHeaderView = navigationView.getHeaderView(0);
            initNavHeader(navHeaderView);
        } catch (Exception e) {
            e.printStackTrace();
        }


        homeImgBTN = (ImageButton) findViewById(R.id.HomeimageButton);
        GroupChatHome = (ImageButton) findViewById(R.id.GroupChatBTNinHome);
        noticeImgBTN = (ImageButton) findViewById(R.id.NoticeimageButton);
        GumsLoginImgBTNinHome = (ImageButton) findViewById(R.id.gumssLoginimg);
        greenLogo = (ImageView) findViewById(R.id.gubLogoIMG);

//        greenLogo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(MainActivity.this, DeveloperActivity.class));
//            }
//        });


        homeImgBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent home = new Intent(MainActivity.this, MainActivity.class);
//                startActivity(home);
//                finish();
                bottomBarLinearLayout.setVisibility(View.VISIBLE);
                // Setting up Main Fragment Container
                HomeFragment homeFragment = new HomeFragment();
                FragmentTransaction fragmentTransaction =
                        getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, homeFragment);
                fragmentTransaction.commit();
                // End of setting Main Fragment Container
            }
        });
        GroupChatHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkConnected()) {
                    //  mAuth = FirebaseAuth.getInstance();
                    //  FirebaseUser user = mAuth.getCurrentUser();

                    if (mFirebaseUser != null) {

                        CurrentUserPhoneNo = mFirebaseUser.getPhoneNumber();

                        // getting  user id fcm

                        mFirebaseDatabase = FirebaseDatabase.getInstance();
                        myRef = mFirebaseDatabase.getReference();

                        DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

                        Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

                        findBatchQuery.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                showDataofUserBatch(dataSnapshot);
                                // FOUND USERS DEPT and ID...
                                //____________
                                try {
                                    if (UserID.length() == 9) {
                                        gubianChecker = new GubianChecker();
                                        gubianChecker.setDept(userDept);

                                        //                           if (UserID.substring(0, 3).equals(userBatch) || UserID.substring(0, 4).equals(userBatch)) {
                                        if (UserID.substring(3, 6).equals(gubianChecker.getDcode()) || UserID.substring(4, 7).equals(gubianChecker.getDcode()) || gubianChecker.getDcode().equals("dont know") || UserID.substring(3, 6).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeTEXev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeTEXev())) {


                                            startActivity(new Intent(MainActivity.this, GroupChat.class));


                                        } else {

                                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                            builder.setTitle("Found Wrong information! Edit Your Profile to Join Group Chat!");
                                            builder.setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    finish();
                                                    dialogInterface.cancel();
                                                    Intent editMe = new Intent(MainActivity.this, MyProfileActivity.class);
                                                    startActivity(editMe);
                                                }
                                            });

                                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.cancel();

                                                }
                                            });

                                            builder.show();

                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplication(),"Incorrect Profile Information!",Toast.LENGTH_SHORT).show();
                                }
//                            else {
//
//                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                                builder.setTitle("Found Wrong information! Edit Your Profile to Join Group Chat!");
//                                builder.setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        finish();
//                                        dialogInterface.cancel();
//                                        Intent editMe = new Intent(MainActivity.this, MyProfileActivity.class);
//                                        startActivity(editMe);
//                                    }
//                                });
//
//                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        dialogInterface.cancel();
//
//                                    }
//                                });
//
//                                builder.show();
//
//                            }
                                // ___________


                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.i("Item not found: ", "this item is not in the list");
                            }
                        });


                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Login to Join Group Chat!");

                        builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                                dialogInterface.cancel();
                                Intent editMe = new Intent(MainActivity.this, SignupActivity.class);
                                startActivity(editMe);
                            }
                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();

                            }
                        });

                        builder.show();
                    }

                } else {
                    Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                }

            }


        });
        noticeImgBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    startActivity(new Intent(MainActivity.this, AppNoticeBoardActivity.class));
                } else {
                    Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        GumsLoginImgBTNinHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()) {
                    FragmentGums fragmentGums = new FragmentGums();
                    FragmentTransaction fragmentTransaction =
                            getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, fragmentGums);
                    fragmentTransaction.commit();
                } else {
                    Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }
        });


//        try {
//            auth = FirebaseAuth.getInstance();
//            final FirebaseUser user = auth.getCurrentUser();
//
//            if (auth.getCurrentUser() != null) {
//
//                CurrentUserPhoneNo = user.getPhoneNumber();
//                mFirebaseDatabase = FirebaseDatabase.getInstance();
//                myRef = mFirebaseDatabase.getReference();
//
//                DatabaseReference user_info_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);
//
//                Query deleteQuery = user_info_Ref.orderByChild(CurrentUserPhoneNo);
//
//                deleteQuery.addChildEventListener(new ChildEventListener() {
//                    @Override
//                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                        showData(dataSnapshot);
//
//
//                        View navHeaderView = navigationView.getHeaderView(0);
//                        initNavHeader(navHeaderView);
//
////                        try {
////                            View hView = navigationView.getHeaderView(0);
////                            TextView nav_user = (TextView) hView.findViewById(R.id.TVnav_View_Name);
////                            TextView nav_id = (TextView) hView.findViewById(R.id.TVnav_View_Id);
////                            myImg = (ImageView) findViewById(R.id.MyimageView);
////                            Button profileEditBTN = (Button) hView.findViewById(R.id.BTNprofile_edit);
////                            ImageButton uploadMyPicBTN = (ImageButton) hView.findViewById(R.id.uploadMyPhoto);
////                            nav_user.setText(CurrentUserName);
////                            nav_id.setText("Batch: " + CurrentUserBatch);
////                            profileEditBTN.setOnClickListener(new View.OnClickListener() {
////                                @Override
////                                public void onClick(View view) {
////                                    startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
////                                    finish();
////                                }
////                            });
////
//////new code
////                            Glide.with(MainActivity.this)
////                                    .load(user.getPhotoUrl())
////                                    .into(myImg);
////
////                            //new code
////
////                            uploadMyPicBTN.setOnClickListener(new View.OnClickListener() {
////                                @Override
////                                public void onClick(View view) {
////
////                                    startActivity(new Intent(MainActivity.this,UploadProfilePic.class));
////
////
////
////                                }
////                            });
////
////                            try {
////                                if (gen.equals("MALE")) {
////                                    myImg.setImageResource(R.drawable.amale_graduate);
////                                } else if (gen.equals("FEMALE")) {
////                                    myImg.setImageResource(R.drawable.afemale_graduate);
////                                } else {
////                                    myImg.setImageResource(R.drawable.account);
////                                }
////                            } catch (Exception e) {
////                                e.printStackTrace();
////                            }
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        }
//
//                    }
//
//                    @Override
//                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//                    }
//
//                    @Override
//                    public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//                    }
//
//                    @Override
//                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        Log.i("Item not found: ", "this item is not in the list");
//                    }
//                });
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    private void init() {
        if (mFirebaseUser != null) {
            // mUserRef = FirebaseUtilsForUserInfo.getUserDpRef();
            //mUserRef = FirebaseUtilsForUserInfo.getUserPhnRef(mFirebaseUser.getPhoneNumber());
            mUserRef = FirebaseUtilsForUserInfo.getUserinfoRef();
        }
    }


//    private void checkLogin() {
//        LinearLayout loginLayout = (LinearLayout) findViewById(R.id.homeLoginView);
//        LinearLayout afterLoginLayout = (LinearLayout) findViewById(R.id.AfterLoginHome);
//
//        auth = FirebaseAuth.getInstance();
//
//        if (auth.getCurrentUser() != null) {
//            //Toast.makeText(getApplicationContext(), "আপনি লগিন আছেন!", Toast.LENGTH_LONG).show();
//            loginLayout.setVisibility(View.GONE);
//
//        } else {
//
//            loginLayout.setVisibility(View.VISIBLE);
//            afterLoginLayout.setVisibility(View.GONE);
//        }
//    }


    private void initNavHeader(View view) {
        mDisplayImageView = (ImageView) view.findViewById(R.id.MyimageView);
        nav_user = (TextView) view.findViewById(R.id.TVnav_View_Name);
        nav_id = (TextView) view.findViewById(R.id.TVnav_View_Id);
        profileEditBTN = (Button) view.findViewById(R.id.BTNprofile_edit);
        uploadMyPicBTN = (ImageButton) view.findViewById(R.id.uploadMyPhoto);

        mUserValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {

                    // ProfilePicUploadInfo  ppuinfo = dataSnapshot.getValue(ProfilePicUploadInfo.class);
                    AppUser appUser = dataSnapshot.getValue(AppUser.class);
                    if (appUser.getPropicurl() == null) {
                        pp = "https://firebasestorage.googleapis.com/v0/b/gub-portal.appspot.com/o/All_Profile_Pics%2Ffacebook_avatar.png?alt=media&token=8617e13f-7ea7-486c-9517-81cceebc94b3";
                    } else pp = appUser.getPropicurl();
                    Glide.with(MainActivity.this)
                            .load(pp)
                            .into(mDisplayImageView);
                    nav_user.setText(appUser.getUsername());
                    nav_id.setText(appUser.getDepartment()+" "+appUser.getShift()+" "+appUser.getSec()+" (ID:"+appUser.getId()+")");
                } else {
                    nav_user.setText("Log in to set your name");
                    nav_id.setText("log in to setup batch");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


//        View hView = navigationView.getHeaderView(0);
//        nav_user = (TextView) hView.findViewById(R.id.TVnav_View_Name);
//        nav_id = (TextView) hView.findViewById(R.id.TVnav_View_Id);
//        myImg = (ImageView) findViewById(R.id.MyimageView);
//           Button profileEditBTN = (Button) hView.findViewById(R.id.BTNprofile_edit);
//        ImageButton uploadMyPicBTN = (ImageButton) hView.findViewById(R.id.uploadMyPhoto);
//        nav_user.setText(CurrentUserName);
//        nav_id.setText("Batch: " + CurrentUserBatch);
        profileEditBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
                finish();
            }
        });


//        Glide.with(MainActivity.this)
//                .load(user.get)
//                .into(postOwnerDisplayImageView);

        uploadMyPicBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(MainActivity.this, UploadProfilePic.class));
                finish();


            }
        });

//        try {
//            if (gen.equals("MALE")) {
//                myImg.setImageResource(R.drawable.amale_graduate);
//            } else if (gen.equals("FEMALE")) {
//                myImg.setImageResource(R.drawable.afemale_graduate);
//            } else {
//                myImg.setImageResource(R.drawable.account);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.profileMenu) {
            startActivity(new Intent(MainActivity.this, DeveloperActivity.class));
            return true;

        }

        if (id == R.id.action_login_cr) {
             if (isNetworkConnected()) {

                 bottomBarLinearLayout.setVisibility(View.GONE);

                 CRnDevLoginActivity cRnDevLoginActivity = new CRnDevLoginActivity();
                 FragmentTransaction fragmentTransaction =
                         getSupportFragmentManager().beginTransaction();
                 fragmentTransaction.replace(R.id.fragment_container, cRnDevLoginActivity);
                 fragmentTransaction.commit();

             } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }

        }

        return super.onOptionsItemSelected(item);
    }

//    private void logoutMethod() {
//        auth = FirebaseAuth.getInstance();
//        if (auth.getCurrentUser() != null) {
//            auth.signOut();
//
//// this listener will be called when there is change in firebase user session
//            FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
//                @Override
//                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                    FirebaseUser user = firebaseAuth.getCurrentUser();
//                    if (user == null) {
//                        // user auth state is changed - user is null
//                        // launch login activity
//                        startActivity(new Intent(MainActivity.this, EditProfileActivity.class));
//                        finish();
//                    }
//                }
//            };
//            checkLogin();
//        } else {
//            Toast.makeText(MainActivity.this, "অ্যাপের সুবিধাগুলি পেতে আপনাকে অবশ্যই লগিন করতে হবে!!", Toast.LENGTH_LONG).show();
//        }
//
//
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            bottomBarLinearLayout.setVisibility(View.VISIBLE);
            // Setting up Main Fragment Container
            HomeFragment homeFragment = new HomeFragment();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, homeFragment);
            fragmentTransaction.commit();
            // End of setting Main Fragment Container

        } else if (id == R.id.notice_board) {
            if (isNetworkConnected()) {
                bottomBarLinearLayout.setVisibility(View.VISIBLE);
                startActivity(new Intent(MainActivity.this, AppNoticeBoardActivity.class));
            } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.gums) {
            if (isNetworkConnected()) {
                bottomBarLinearLayout.setVisibility(View.VISIBLE);
                FragmentGums fragmentGums = new FragmentGums();
                FragmentTransaction fragmentTransaction =
                        getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragmentGums);
                fragmentTransaction.commit();
            } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.elibrary) {
            if (isNetworkConnected()) {
                bottomBarLinearLayout.setVisibility(View.VISIBLE);
                E_LibraryFragment fragmentGums = new E_LibraryFragment();
                FragmentTransaction fragmentTransaction =
                        getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragmentGums);
                fragmentTransaction.commit();
            } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.group_chat) {
            if (isNetworkConnected()) {
                //mAuth = FirebaseAuth.getInstance();
                //FirebaseUser user = mAuth.getCurrentUser();

                if (mFirebaseUser != null) {

                    CurrentUserPhoneNo = mFirebaseUser.getPhoneNumber();

                    // getting  user id fcm

                    mFirebaseDatabase = FirebaseDatabase.getInstance();
                    myRef = mFirebaseDatabase.getReference();

                    DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

                    Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

                    findBatchQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            showDataofUserBatch(dataSnapshot);
                            // FOUND USERS DEPT and ID...
                            //____________
                            if (UserID.length() == 9) {
                                gubianChecker = new GubianChecker();
                                gubianChecker.setDept(userDept);


                                //                           if (UserID.substring(0, 3).equals(userBatch) || UserID.substring(0, 4).equals(userBatch)) {
                                if (UserID.substring(3, 6).equals(gubianChecker.getDcode()) || UserID.substring(4, 7).equals(gubianChecker.getDcode()) || gubianChecker.getDcode().equals("dont know") || UserID.substring(3, 6).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeTEXev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeTEXev())) {


                                    startActivity(new Intent(MainActivity.this, GroupChat.class));


                                } else {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle("Found Wrong information! Edit Your Profile to Join Group Chat!");
                                    builder.setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                            dialogInterface.cancel();
                                            Intent editMe = new Intent(MainActivity.this, MyProfileActivity.class);
                                            startActivity(editMe);
                                        }
                                    });

                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();

                                        }
                                    });

                                    builder.show();

                                }
                            }


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("Item not found: ", "this item is not in the list");
                        }
                    });


                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Login to Join Group Chat!");

                    builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                            dialogInterface.cancel();
                            Intent editMe = new Intent(MainActivity.this, SignupActivity.class);
                            startActivity(editMe);
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    });

                    builder.show();
                }

            } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.friend_finder) {
            bottomBarLinearLayout.setVisibility(View.VISIBLE);
            Toast.makeText(MainActivity.this, "This feature is under development!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_blood) {
            bottomBarLinearLayout.setVisibility(View.VISIBLE);
            Toast.makeText(MainActivity.this, "This feature is under development!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.termsofService) {
            bottomBarLinearLayout.setVisibility(View.VISIBLE);
            startActivity(new Intent(MainActivity.this, Activity_TermsOfService.class));
            return true;

        } else if (id == R.id.greenfo_fb_group) {


            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            try {     //left out the profile id on purpose...
                                Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://group/298534307304196/"));
                                startActivity(facebook);

                            } catch (Exception e) {
                                Uri uri = Uri.parse("https://www.facebook.com/groups/298534307304196");
                                startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW, uri), "Open GreenFo's Facebook Group Using: "));
                            }


                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
            builder.setMessage("Wanna join Facebook Group of GreenFo?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();






        } else if (id == R.id.about_dev) {
            startActivity(new Intent(MainActivity.this, DeveloperActivity.class));
            return true;

        } else if (id == R.id.troll) {

            if (!isNetworkConnected()) {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }

                if (mFirebaseUser != null) {

                    CurrentUserPhoneNo = mFirebaseUser.getPhoneNumber();

                    // getting  user id fcm

                    mFirebaseDatabase = FirebaseDatabase.getInstance();
                    myRef = mFirebaseDatabase.getReference();

                    DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

                    Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

                    findBatchQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            showDataofUserBatch(dataSnapshot);
                            // FOUND USERS DEPT and ID...
                            //____________
                            if (UserID.length() == 9) {
                                gubianChecker = new GubianChecker();
                                gubianChecker.setDept(userDept);


                                //                           if (UserID.substring(0, 3).equals(userBatch) || UserID.substring(0, 4).equals(userBatch)) {
                                if (UserID.substring(3, 6).equals(gubianChecker.getDcode()) || UserID.substring(4, 7).equals(gubianChecker.getDcode()) || gubianChecker.getDcode().equals("dont know") || UserID.substring(3, 6).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeTEXev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeTEXev())) {


                                    bottomBarLinearLayout.setVisibility(View.GONE);
                                    GubTrollFragment helpZoneFragment = new GubTrollFragment();
                                    FragmentTransaction fragmentTransaction =
                                            getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragment);
                                    fragmentTransaction.commit();


                                } else {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle("Found Wrong information! Edit Your Profile to Join GUB TROLL page!");
                                    builder.setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                            dialogInterface.cancel();
                                            Intent editMe = new Intent(MainActivity.this, MyProfileActivity.class);
                                            startActivity(editMe);
                                        }
                                    });

                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();

                                        }
                                    });

                                    builder.show();

                                }
                            }


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("Item not found: ", "this item is not in the list");
                        }
                    });


                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Login to Join Gub Troll Page!");

                    builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                            dialogInterface.cancel();
                            Intent editMe = new Intent(MainActivity.this, SignupActivity.class);
                            startActivity(editMe);
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    });

                    builder.show();
                }



        } else if (id == R.id.nav_result) {
            if (isNetworkConnected()) {
                bottomBarLinearLayout.setVisibility(View.VISIBLE);
                FragmentVarsityResult fragmentVarsityResult = new FragmentVarsityResult();
                FragmentTransaction fragmentTransaction =
                        getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragmentVarsityResult);
                fragmentTransaction.commit();
            } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_help) {

            if (!isNetworkConnected()) {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
                if (mFirebaseUser != null) {

                    CurrentUserPhoneNo = mFirebaseUser.getPhoneNumber();

                    // getting  user id fcm

                    mFirebaseDatabase = FirebaseDatabase.getInstance();
                    myRef = mFirebaseDatabase.getReference();

                    DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

                    Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

                    findBatchQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            showDataofUserBatch(dataSnapshot);
                            // FOUND USERS DEPT and ID...
                            //____________
                            if (UserID.length() == 9) {
                                gubianChecker = new GubianChecker();
                                gubianChecker.setDept(userDept);


                                //                           if (UserID.substring(0, 3).equals(userBatch) || UserID.substring(0, 4).equals(userBatch)) {
                                if (UserID.substring(3, 6).equals(gubianChecker.getDcode()) || UserID.substring(4, 7).equals(gubianChecker.getDcode()) || gubianChecker.getDcode().equals("dont know") || UserID.substring(3, 6).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeTEXev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeTEXev())) {


                                    bottomBarLinearLayout.setVisibility(View.GONE);
                                    HelpZoneFragment helpZoneFragment = new HelpZoneFragment();
                                    FragmentTransaction fragmentTransaction =
                                            getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragment);
                                    fragmentTransaction.commit();


                                } else {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle("Found Wrong information! Edit Your Profile to Join GUB TROLL page!");
                                    builder.setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                            dialogInterface.cancel();
                                            Intent editMe = new Intent(MainActivity.this, MyProfileActivity.class);
                                            startActivity(editMe);
                                        }
                                    });

                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();

                                        }
                                    });

                                    builder.show();

                                }
                            }


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("Item not found: ", "this item is not in the list");
                        }
                    });


                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Login to Join Gub Troll Page!");

                    builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                            dialogInterface.cancel();
                            Intent editMe = new Intent(MainActivity.this, SignupActivity.class);
                            startActivity(editMe);
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    });

                    builder.show();
                }




        } else if (id == R.id.crush_confession) {

            if (!isNetworkConnected()) {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
                if (mFirebaseUser != null) {

                    CurrentUserPhoneNo = mFirebaseUser.getPhoneNumber();

                    // getting  user id fcm

                    mFirebaseDatabase = FirebaseDatabase.getInstance();
                    myRef = mFirebaseDatabase.getReference();

                    DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

                    Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

                    findBatchQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            showDataofUserBatch(dataSnapshot);
                            // FOUND USERS DEPT and ID...
                            //____________
                            if (UserID.length() == 9) {
                                gubianChecker = new GubianChecker();
                                gubianChecker.setDept(userDept);


                                //                           if (UserID.substring(0, 3).equals(userBatch) || UserID.substring(0, 4).equals(userBatch)) {
                                if (UserID.substring(3, 6).equals(gubianChecker.getDcode()) || UserID.substring(4, 7).equals(gubianChecker.getDcode()) || gubianChecker.getDcode().equals("dont know") || UserID.substring(3, 6).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeTEXev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeTEXev())) {


                                    bottomBarLinearLayout.setVisibility(View.GONE);
                                    CrushConfessionFragment helpZoneFragment = new CrushConfessionFragment();
                                    FragmentTransaction fragmentTransaction =
                                            getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragment);
                                    fragmentTransaction.commit();


                                } else {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle("Found Wrong information! Edit Your Profile to Join GUB CRUSH & CONFESSION page!");
                                    builder.setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                            dialogInterface.cancel();
                                            Intent editMe = new Intent(MainActivity.this, MyProfileActivity.class);
                                            startActivity(editMe);
                                        }
                                    });

                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();

                                        }
                                    });

                                    builder.show();

                                }
                            }


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("Item not found: ", "this item is not in the list");
                        }
                    });


                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Login to Join Gub Crush and Confession Page!");

                    builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                            dialogInterface.cancel();
                            Intent editMe = new Intent(MainActivity.this, SignupActivity.class);
                            startActivity(editMe);
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    });

                    builder.show();
                }



        } else if (id == R.id.Adrmire) {
            if (!isNetworkConnected()) {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
                if (mFirebaseUser != null) {

                    CurrentUserPhoneNo = mFirebaseUser.getPhoneNumber();

                    // getting  user id fcm

                    mFirebaseDatabase = FirebaseDatabase.getInstance();
                    myRef = mFirebaseDatabase.getReference();

                    DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

                    Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

                    findBatchQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            showDataofUserBatch(dataSnapshot);
                            // FOUND USERS DEPT and ID...
                            //____________
                            if (UserID.length() == 9) {
                                gubianChecker = new GubianChecker();
                                gubianChecker.setDept(userDept);


                                //                           if (UserID.substring(0, 3).equals(userBatch) || UserID.substring(0, 4).equals(userBatch)) {
                                if (UserID.substring(3, 6).equals(gubianChecker.getDcode()) || UserID.substring(4, 7).equals(gubianChecker.getDcode()) || gubianChecker.getDcode().equals("dont know") || UserID.substring(3, 6).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(3, 6).equals(gubianChecker.getDcodeTEXev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeCSEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeEEEev()) || UserID.substring(4, 7).equals(gubianChecker.getDcodeTEXev())) {


                                    bottomBarLinearLayout.setVisibility(View.GONE);
                                    AdmirationPostFragment helpZoneFragment = new AdmirationPostFragment();
                                    FragmentTransaction fragmentTransaction =
                                            getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragment);
                                    fragmentTransaction.commit();


                                } else {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle("Found Wrong information! Edit Your Profile to Join GUB ADMIRATION page!");
                                    builder.setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                            dialogInterface.cancel();
                                            Intent editMe = new Intent(MainActivity.this, MyProfileActivity.class);
                                            startActivity(editMe);
                                        }
                                    });

                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();

                                        }
                                    });

                                    builder.show();

                                }
                            }


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("Item not found: ", "this item is not in the list");
                        }
                    });


                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Login to Join GUB ADMIRATION POST Page!");

                    builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                            dialogInterface.cancel();
                            Intent editMe = new Intent(MainActivity.this, SignupActivity.class);
                            startActivity(editMe);
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    });

                    builder.show();
                }



        } else if (id == R.id.ac_myprofile) {
            if (isNetworkConnected()) {
                bottomBarLinearLayout.setVisibility(View.VISIBLE);
                startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
                finish();
            } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.varsity_news) {
            if (isNetworkConnected()) {
                bottomBarLinearLayout.setVisibility(View.GONE);
                NewsFromWebsiteFragment newsFromWebsiteFragment = new NewsFromWebsiteFragment();
                FragmentTransaction fragmentTransaction =
                        getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, newsFromWebsiteFragment);
                fragmentTransaction.commit();
            } else {
                Toast.makeText(MainActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void showData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();

            CurrentUserName = String.valueOf(value.get("username"));
            String dept = String.valueOf(value.get("department"));
            String ba = String.valueOf(value.get("batch"));
            String sft = String.valueOf(value.get("shift"));
            gen = String.valueOf(value.get("gender"));
            CurrentUserBatch = dept + " " + ba + " " + sft;

        }
    }


    private void showDataofUserBatch(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            userDept = String.valueOf(value.get("department"));
            MyName = String.valueOf(value.get("username"));
            userBatch = String.valueOf(value.get("batch"));
            userShift = String.valueOf(value.get("shift"));
            UserID = String.valueOf(value.get("id"));
            UserGender = String.valueOf(value.get("gender"));

        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        //mAuthx.addAuthStateListener(mAuthStateListener);
        if (mUserRef != null) {
            mUserRef.addValueEventListener(mUserValueEventListener);
        }


            SharedPreferences pref = this.getSharedPreferences(Config.SHOW_TERMS, 0);
            String pass = pref.getString("show_terms", "UNKNOWN");

        try {
            if(pass.equals("YES")){


                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked

                                try {
                                    Intent intent = new Intent(MainActivity.this, Activity_TermsOfService.class);
                                    startActivity(intent);

                                } catch (Exception e) {
                                    e.printStackTrace();

                                }


                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                finish();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false);
                builder.setMessage("To use the app 'Accept' Terms of Service!").setPositiveButton("Accept", dialogClickListener)
                        .setNegativeButton("Exit", dialogClickListener).show();




            } else if(pass.equals("NO")){

            } else if (pass.equals("UNKNOWN")){
                Intent intent = new Intent(this, Activity_TermsOfService.class);
                startActivity(intent);
            }else {
                Intent intent = new Intent(this, Activity_TermsOfService.class);
                startActivity(intent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthStateListener != null)
            mAuthx.removeAuthStateListener(mAuthStateListener);
        if (mUserRef != null)
            mUserRef.removeEventListener(mUserValueEventListener);
    }


}
