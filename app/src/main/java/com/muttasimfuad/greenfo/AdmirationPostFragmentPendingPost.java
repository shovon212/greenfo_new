package com.muttasimfuad.greenfo;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.utils.Constants;
import com.muttasimfuad.greenfo.utils.FirebaseUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdmirationPostFragmentPendingPost extends Fragment {

    private View mRootVIew;
    private FirebaseRecyclerAdapter<Post, PostHolder> mPostAdapter;
    private RecyclerView mPostRecyclerView;

    //new
    private ValueEventListener mUserValueEventListener;
    private DatabaseReference mUserRef;

    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseAuth mAuthx;
    private FirebaseUser mFirebaseUser;
    String MyName, pp, ModName;
    private FragmentActivity myContext;


    public AdmirationPostFragmentPendingPost() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootVIew = inflater.inflate(R.layout.fragment_help_zone_fragment_pending_post, container, false);
        try {
            getActivity().setTitle("Pending Admiration Post");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Retrieve the value
        ModName = getArguments().getString("ModName");




        init();
        return mRootVIew;
    }

    private void init() {
        mPostRecyclerView = (RecyclerView) mRootVIew.findViewById(R.id.recyclerview_post);
        mPostRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        setupAdapter();
        mPostRecyclerView.setAdapter(mPostAdapter);
    }

    private void setupAdapter() {
        mPostAdapter = new FirebaseRecyclerAdapter<Post, PostHolder>(
                Post.class,
                R.layout.row_post_help_zone_pending,
                PostHolder.class,
                FirebaseUtils.getPostRefadmiration_post()
        ) {
            @Override
            protected void populateViewHolder(PostHolder viewHolder, final Post model, int position) {
//                viewHolder.setNumCOmments(String.valueOf(model.getNumComments()));
//                viewHolder.setNumLikes(String.valueOf(model.getNumLikes()));
                viewHolder.setTIme(DateUtils.getRelativeTimeSpanString(model.getTimeCreated()));
                viewHolder.setUsername(model.getUser().getUserFullBatch());
                viewHolder.setPostText(model.getPostText());

                Glide.with(getActivity())
                        .load(model.getUser().getPhotoUrl())
                        .into(viewHolder.postOwnerDisplayImageView);

                if (model.getPostImageUrl() != null) {
                    viewHolder.postDisplayImageVIew.setVisibility(View.VISIBLE);
                    StorageReference storageReference = FirebaseStorage.getInstance()
                            .getReference(model.getPostImageUrl());
                    Glide.with(getActivity())
                            .using(new FirebaseImageLoader())
                            .load(storageReference)
                            .into(viewHolder.postDisplayImageVIew);
                } else {
                    viewHolder.postDisplayImageVIew.setImageBitmap(null);
                    viewHolder.postDisplayImageVIew.setVisibility(View.GONE);
                }
//                viewHolder.postLikeLayout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        onLikeClick(model.getPostId());
//                    }
//                });


//                viewHolder.postCommentLayout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(getContext(), PostActivity.class);
//                        intent.putExtra(Constants.EXTRA_POST, model);
//                        startActivity(intent);
//                    }
//                });
                viewHolder.deletePostBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (isNetworkConnected()) {


                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            //Yes button clicked

                                            try {
                                                String postId = model.getPostId();


                                                DatabaseReference rootpath_of_postContent = FirebaseDatabase.getInstance().getReference("admiration_post_pending_post");
                                                DatabaseReference postContecnt_fromPath = rootpath_of_postContent.child("posts").child(postId);


                                                postContecnt_fromPath.removeValue();

                                                try {
                                                    StorageReference photoRef = FirebaseUtils.getImageSRefadmiration_post().child(postId);
                                                    photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            Toast.makeText(getContext(),"Uploaded picture deleted!",Toast.LENGTH_LONG).show();
                                                            // File deleted successfully
                                                            // Log.d(TAG, "onSuccess: deleted file");
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception exception) {
                                                            // Uh-oh, an error occurred!
                                                            // Log.d(TAG, "onFailure: did not delete file");
                                                            //Toast.makeText(getContext(),"Failed to delete! Inform it to Muttasim Fuad",Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Toast.makeText(getContext(), "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
                                            }


                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
                            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();


                        } else {
                            Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                        }

                    }

                });
                viewHolder.KeepPostBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (isNetworkConnected()) {


                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            //Yes button clicked

                                            try {
                                                String postId = model.getPostId();


                                                DatabaseReference rootpath_of_postContent = FirebaseDatabase.getInstance().getReference("admiration_post_pending_post");
                                                DatabaseReference postContecnt_fromPath = rootpath_of_postContent.child("posts").child(postId); 
                                                DatabaseReference postContecnt_toPath = FirebaseDatabase.getInstance().getReference("admiration_post").child("posts").child(postId);

                                                addToMyPostListFinal(postId);  // postCreate dialog a dorkar nai ei action



                                                moveGameRoom(postContecnt_fromPath, postContecnt_toPath);



                                               // postContecnt_fromPath.child("postText").setValue("Post was Approved by: "+ModName);

                                                postContecnt_fromPath.removeValue();

                                               // postContecnt_toPath.setValue(ModName); // TO DO : fix it









                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Toast.makeText(getContext(), "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
                                            }


                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
                            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();


                        } else {
                            Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        };
    }


    private void addToMyPostListFinal(String postId) {
//        FirebaseUtils.getPostRef().child(postId)
//                .setValue(mPost);
        FirebaseUtils.getMyPostRefFinaladmiration_post().child(postId).setValue(true);

        FirebaseUtils.addToMyRecordFinaladmiration_post(Constants.POST_KEY, postId);
    }

    //During the tutorial I think I messed up this code. Make sure your's aligns to this, or just
    //check out the github code




    public static class PostHolder extends RecyclerView.ViewHolder {
        ImageView postOwnerDisplayImageView;
        TextView postOwnerUsernameTextView;
        TextView postTimeCreatedTextView;
        ImageView postDisplayImageVIew;
        TextView postTextTextView;
        LinearLayout postLikeLayout;
        LinearLayout postCommentLayout;
        TextView postNumLikesTextView;
        TextView postNumCommentsTextView;
        Button deletePostBTN, KeepPostBTN;


        public PostHolder(View itemView) {
            super(itemView);
            postOwnerDisplayImageView = (ImageView) itemView.findViewById(R.id.iv_post_owner_display);
            postOwnerUsernameTextView = (TextView) itemView.findViewById(R.id.tv_post_username);
            postTimeCreatedTextView = (TextView) itemView.findViewById(R.id.tv_time);
            postDisplayImageVIew = (ImageView) itemView.findViewById(R.id.iv_post_display);
//            postLikeLayout = (LinearLayout) itemView.findViewById(R.id.like_layout);
//            postCommentLayout = (LinearLayout) itemView.findViewById(R.id.comment_layout);
//            postNumLikesTextView = (TextView) itemView.findViewById(R.id.tv_likes);
//            postNumCommentsTextView = (TextView) itemView.findViewById(R.id.tv_comments);
            postTextTextView = (TextView) itemView.findViewById(R.id.tv_post_text);
            deletePostBTN = (Button) itemView.findViewById(R.id.deleteBTNhelpZone);
            KeepPostBTN = (Button) itemView.findViewById(R.id.allowBTNhelpZone);
        }

        public void setUsername(String username) {
            postOwnerUsernameTextView.setText(username);
        }

        public void setTIme(CharSequence time) {
            postTimeCreatedTextView.setText(time);
        }

//        public void setNumLikes(String numLikes) {
//            postNumLikesTextView.setText(numLikes);
//        }
//
//        public void setNumCOmments(String numComments) {
//            postNumCommentsTextView.setText(numComments);
//        }

        public void setPostText(String text) {
            postTextTextView.setText(text);
        }

    }


//    @Override
//    public void onStart() {
//        super.onStart();
//        //mAuthx.addAuthStateListener(mAuthStateListener);
//        if (mUserRef != null) {
//            mUserRef.addValueEventListener(mUserValueEventListener);
//        }
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mAuthStateListener != null)
//            mAuthx.removeAuthStateListener(mAuthStateListener);
//        if (mUserRef != null)
//            mUserRef.removeEventListener(mUserValueEventListener);
//    }

    private void moveGameRoom(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                        if (firebaseError != null) {
                            //toPath.child("Approved By: "+ModName);
                            //   DisPlayText.setText("Copy failed");
                            Toast.makeText(getContext(),"Network error! Failed to approve!",Toast.LENGTH_LONG).show();

                        } else {
                            //  DisPlayText.setText("Successfully Copied!!");
                            toPath.child("Approved By: ").setValue(ModName);
                            Toast.makeText(getContext(),"Post has been approved!",Toast.LENGTH_LONG).show();

                        }
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    HomeFragment helpZoneFragmentPendingPost = new HomeFragment();


                    FragmentTransaction fragmentTransaction =
                            myContext.getSupportFragmentManager().beginTransaction();

                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                    fragmentTransaction.commit();





                    return true;

                }

                return false;
            }
        });

        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

}
