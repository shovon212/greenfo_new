package com.muttasimfuad.greenfo;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Fuad on 12/1/2017.
 */

public class MApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
   //     FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        //new

        if (FirebaseApp.getApps(this).isEmpty()) {
            FirebaseApp.initializeApp(this);
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}