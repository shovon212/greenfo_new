package com.muttasimfuad.greenfo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.muttasimfuad.greenfo.app.Config;

import java.util.Map;

public class CRnDevLoginActivity extends Fragment {

    Spinner loginAsSpinner, loggerDeptSpinner, loggerBatchSpinner, loggerShiftSpinner, loggerSectionSpinner;
    EditText crPasswordET;
    Button cRLoginButton;
    TextView loginStatus;
    String RealPassword, RealPasswordDev, RealPasswordModerator, RealNameOfMod;
    String crORdevPassword;
    Button AutoBTN;
    private View mRootVIew;

    private FragmentActivity myContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootVIew = inflater.inflate(R.layout.activity_crn_dev_login, container, false);
        try {
            getActivity().setTitle("Admin Panel Login");
        } catch (Exception e) {
            e.printStackTrace();
        }
        loginAsSpinner = (Spinner) mRootVIew.findViewById(R.id.loginAsSP);
        loggerDeptSpinner = (Spinner) mRootVIew.findViewById(R.id.deptOfLoggerSP);
        loggerBatchSpinner = (Spinner) mRootVIew.findViewById(R.id.batchOfLoggerSP);
        loggerShiftSpinner = (Spinner) mRootVIew.findViewById(R.id.shiftOfLoggerSP);
        loggerSectionSpinner = (Spinner) mRootVIew.findViewById(R.id.secOfLoggerSP);

        crPasswordET = (EditText) mRootVIew.findViewById(R.id.crpasswordET);
        cRLoginButton = (Button) mRootVIew.findViewById(R.id.crloginBTN);
        loginStatus = (TextView) mRootVIew.findViewById(R.id.loginstatusTV);
        AutoBTN = (Button) mRootVIew.findViewById(R.id.autoGen);

        AutoBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crORdevPassword = crPasswordET.getText().toString().trim();
                String loggerName = loginAsSpinner.getSelectedItem().toString();
                final String loggerDept = loggerDeptSpinner.getSelectedItem().toString();
                final String loggerBatch = loggerBatchSpinner.getSelectedItem().toString();
                final String loggerShif = loggerShiftSpinner.getSelectedItem().toString();
                final String loggerSec = loggerSectionSpinner.getSelectedItem().toString();
                if (isNetworkConnected() == true) {
                    DatabaseReference crpassPath = FirebaseDatabase.getInstance().getReference().child("batches");
                    //  crpassPath.child(loggerDept).child(loggerBatch).child(loggerShif).child("cr_info").child(loggerSec).child(loggerSec).setValue("null");
                    DatabaseReference crpassPath2 = crpassPath.child(loggerDept).child(loggerBatch).child(loggerShif).child("cr_info").child(loggerSec).child(crORdevPassword);
                    crpassPath2.child("name").setValue("CR");
                    crpassPath2.child("pass").setValue("fuad99");
                    crpassPath2.child("phone").setValue("01674");
                    Toast.makeText(getActivity(), "DONE!", Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        AutoBTN.setVisibility(View.GONE);

        cRLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                crORdevPassword = crPasswordET.getText().toString().trim();

                String loggerName = loginAsSpinner.getSelectedItem().toString();
                final String loggerDept = loggerDeptSpinner.getSelectedItem().toString();
                final String loggerBatch = loggerBatchSpinner.getSelectedItem().toString();
                final String loggerShif = loggerShiftSpinner.getSelectedItem().toString();
                final String loggerSec = loggerSectionSpinner.getSelectedItem().toString();
                if (isNetworkConnected() == true) {
                    if (!TextUtils.isEmpty(crORdevPassword)) {
                        if (loggerName.equals("Login As CR")) {


                            DatabaseReference crpassPath = FirebaseDatabase.getInstance().getReference().child("batches").child(loggerDept).child(loggerBatch).child(loggerShif).child("cr_info").child(loggerSec); //update
                            Query deleteQuery = crpassPath.orderByChild(loggerSec);
                            try {
                                deleteQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        showData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPassword)) {
                                            String totalBatch = loggerDept + " " + loggerBatch + " " + loggerShif + " " + loggerSec;
                                            Intent intent = new Intent(getActivity(), CrPanelActivity.class);
                                            intent.putExtra("crtotalbatch", totalBatch);
                                            intent.putExtra("crdept", loggerDept);
                                            intent.putExtra("crbatch", loggerBatch);
                                            intent.putExtra("crshift", loggerShif);
                                            intent.putExtra("crsec", loggerSec);
                                            startActivity(intent);
                                            Toast.makeText(getActivity(), "Fuad says you WELCOME !!", Toast.LENGTH_LONG).show();
                                            //getActivity().finish();
                                        } else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }


                        if (loggerName.equals("Developer")) {
                            try {

                                DatabaseReference crpassPathDev = FirebaseDatabase.getInstance().getReference().child("Powership").child("Dev").child("dev_info");
                                Query devQuery = crpassPathDev.orderByChild("dev_info");

                                devQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        devData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPasswordDev)) {
                                            Intent intent = new Intent(getActivity(), HomeTabAdminActivity.class);
                                            startActivity(intent);
                                            Toast.makeText(getActivity(), "Welcome Boss !!", Toast.LENGTH_SHORT).show();
                                        } else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {

                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                            }


                        }

                        if (loggerName.equals("Notice Mod")) {
                            try {

                                DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("Powership").child("Mod").child("NoticeMod");
                                Query ModeratorQuery = crpassPathModerator.orderByChild("NoticeMod");

                                ModeratorQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        ModeratorData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPasswordModerator)) {
                                    Intent intent = new Intent(getActivity(), HomeTabAdminActivity.class);
                                    startActivity(intent);



                                            Toast.makeText(getActivity(), "Fuad says you WELCOME !!", Toast.LENGTH_SHORT).show();
                                        }  else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                            }

                        }
                        if (loggerName.equals("Admin")) {
                            try {

                                DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("Powership").child("Mod");
                                Query ModeratorQuery = crpassPathModerator.orderByChild("Mod");

                                ModeratorQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        ModeratorData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPasswordModerator)) {

                                            SharedPreferences pref = getActivity().getSharedPreferences(Config.ALLOW_ADMIN_LOGIN, 0);
                                            SharedPreferences.Editor editor = pref.edit();
                                            editor.putString("allow_admin_login", "YES");
                                            editor.commit();
//                                    Intent intent = new Intent(CRnDevLoginActivity.this, UploadNotice.class);
//                                    startActivity(intent);

                                            AdminFragment helpZoneFragmentPendingPost = new AdminFragment();

                                            Bundle args = new Bundle();
                                            args.putString("ModName", RealNameOfMod);
                                            helpZoneFragmentPendingPost.setArguments(args);

                                            FragmentTransaction fragmentTransaction =
                                                    myContext.getSupportFragmentManager().beginTransaction();

                                            fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                                            fragmentTransaction.commit();


                                            Toast.makeText(getActivity(), "Fuad says you WELCOME !!", Toast.LENGTH_SHORT).show();
                                        } else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                            }

                        }
                        if (loggerName.equals("Help Mod")) {
                            try {

                                DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("Powership").child("Mod");
                                Query ModeratorQuery = crpassPathModerator.orderByChild("Mod");

                                ModeratorQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        ModeratorData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPasswordModerator)) {
//                                    Intent intent = new Intent(CRnDevLoginActivity.this, UploadNotice.class);
//                                    startActivity(intent);

                                            HelpZoneFragmentPendingPost helpZoneFragmentPendingPost = new HelpZoneFragmentPendingPost();

                                            Bundle args = new Bundle();
                                            args.putString("ModName", RealNameOfMod);
                                            helpZoneFragmentPendingPost.setArguments(args);

                                            FragmentTransaction fragmentTransaction =
                                                    myContext.getSupportFragmentManager().beginTransaction();

                                            fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                                            fragmentTransaction.commit();


                                            Toast.makeText(getActivity(), "Fuad says you WELCOME !!", Toast.LENGTH_SHORT).show();
                                        } else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                            }

                        }
                        if (loggerName.equals("Troll Mod")) {
                            try {

                                DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("Powership").child("Mod");
                                Query ModeratorQuery = crpassPathModerator.orderByChild("Mod");

                                ModeratorQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        ModeratorData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPasswordModerator)) {
//                                    Intent intent = new Intent(CRnDevLoginActivity.this, UploadNotice.class);
//                                    startActivity(intent);

                                            GubTrollFragmentPendingPost helpZoneFragmentPendingPost = new GubTrollFragmentPendingPost();

                                            Bundle args = new Bundle();
                                            args.putString("ModName", RealNameOfMod);
                                            helpZoneFragmentPendingPost.setArguments(args);

                                            FragmentTransaction fragmentTransaction =
                                                    myContext.getSupportFragmentManager().beginTransaction();

                                            fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                                            fragmentTransaction.commit();


                                            Toast.makeText(getActivity(), "Fuad says you WELCOME !!", Toast.LENGTH_SHORT).show();
                                        } else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                            }

                        }
                        if (loggerName.equals("Confession Mod")) {
                            try {

                                DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("Powership").child("Mod");
                                Query ModeratorQuery = crpassPathModerator.orderByChild("Mod");

                                ModeratorQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        ModeratorData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPasswordModerator)) {
//                                    Intent intent = new Intent(CRnDevLoginActivity.this, UploadNotice.class);
//                                    startActivity(intent);

                                            CrushConfessionFragmentPendingPost helpZoneFragmentPendingPost = new CrushConfessionFragmentPendingPost();

                                            Bundle args = new Bundle();
                                            args.putString("ModName", RealNameOfMod);
                                            helpZoneFragmentPendingPost.setArguments(args);

                                            FragmentTransaction fragmentTransaction =
                                                    myContext.getSupportFragmentManager().beginTransaction();

                                            fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                                            fragmentTransaction.commit();


                                            Toast.makeText(getActivity(), "Fuad says you WELCOME !!", Toast.LENGTH_SHORT).show();
                                        } else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                            }

                        }
                        if(loggerName.equals("Admiration Mod")) {
                            try {

                                DatabaseReference crpassPathModerator = FirebaseDatabase.getInstance().getReference().child("Powership").child("Mod");
                                Query ModeratorQuery = crpassPathModerator.orderByChild("Mod");

                                ModeratorQuery.addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        ModeratorData(dataSnapshot);

                                        if (crORdevPassword.equals(RealPasswordModerator)) {
//                                    Intent intent = new Intent(CRnDevLoginActivity.this, UploadNotice.class);
//                                    startActivity(intent);

                                            AdmirationPostFragmentPendingPost helpZoneFragmentPendingPost = new AdmirationPostFragmentPendingPost();

                                            Bundle args = new Bundle();
                                            args.putString("ModName", RealNameOfMod);
                                            helpZoneFragmentPendingPost.setArguments(args);

                                            FragmentTransaction fragmentTransaction =
                                                    myContext.getSupportFragmentManager().beginTransaction();

                                            fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                                            fragmentTransaction.commit();


                                            Toast.makeText(getActivity(), "Fuad says you WELCOME !!", Toast.LENGTH_SHORT).show();
                                        } else if (crORdevPassword.equals("5233bd")) {

                                            AutoBTN.setVisibility(View.VISIBLE);
                                            Toast.makeText(getActivity(), "Welcome to create new CR!!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            loginStatus.setText("Incorrect Password! Try Again!");

                                        }


                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.i("Item not found: ", "this item is not in the list");
                                        Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "Check Input Again! Either Wrong Input or Poor Internet Connection!", Toast.LENGTH_SHORT).show();
                            }

                        }



                    } else {
                        Toast.makeText(getActivity(), "Fill Up Password then Click Log In!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
                }




            }
        });

        return mRootVIew;
    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void showData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            RealPassword = String.valueOf(value.get("pass"));
        }
    }

    private void devData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            RealPasswordDev = String.valueOf(value.get("pass"));
        }
    }

    private void ModeratorData(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            RealPasswordModerator = String.valueOf(value.get("pass"));
            RealNameOfMod         = String.valueOf(value.get("name"));
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    HomeFragment helpZoneFragmentPendingPost = new HomeFragment();


                    FragmentTransaction fragmentTransaction =
                            myContext.getSupportFragmentManager().beginTransaction();

                    fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                    fragmentTransaction.commit();





                    return true;

                }

                return false;
            }
        });

        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onStart() {
        super.onStart();
        try {
            SharedPreferences pref = getActivity().getSharedPreferences(Config.ALLOW_ADMIN_LOGIN, 0);
            String pass = pref.getString("allow_admin_login", null);
            if(pass.equals("YES")){

                AdminFragment helpZoneFragmentPendingPost = new AdminFragment();

                Bundle args = new Bundle();
                args.putString("ModName", RealNameOfMod);
                helpZoneFragmentPendingPost.setArguments(args);

                FragmentTransaction fragmentTransaction =
                        myContext.getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, helpZoneFragmentPendingPost);
                fragmentTransaction.commit();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
