package com.muttasimfuad.greenfo;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ThisMonthCrPostActivity extends AppCompatActivity {

    private DatabaseReference Cref2;
    private String CRdept2, CRbatch2, CRshift2,CRsec2;
    private TextView userBatchInCrView2;

    private ListView crNoticeListView2;
    private ArrayList<String> crNoticeList2 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_this_month_cr_post);

        userBatchInCrView2 = (TextView) findViewById(R.id.batchVIEWCR2);
        crNoticeListView2 = (ListView) findViewById(R.id.thisweektabCrViewLV2);

        Intent cr2 = getIntent();

        CRdept2 = cr2.getStringExtra("crdept");
        CRbatch2 = cr2.getStringExtra("crbatch");
        CRshift2 = cr2.getStringExtra("crshift");
        CRsec2 = cr2.getStringExtra("crsec");


        // FOUND USERS DEPT...


        try {
            userBatchInCrView2.setText("Batch: " + CRdept2 + " " + CRbatch2 + " " + CRshift2+" "+CRsec2);

            final ArrayAdapter<String> adapterForCrNotice2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, crNoticeList2);
            crNoticeListView2.setAdapter(adapterForCrNotice2);
            Cref2 = FirebaseDatabase.getInstance().getReference().child("batches").child(CRdept2).child(CRbatch2).child(CRshift2).child("notifn_cr").child(CRsec2).child("month_tab_cr").child("m1");
            Cref2.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    crNoticeList2.add(dataSnapshot.getValue(String.class));
                    adapterForCrNotice2.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {


                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    crNoticeList2.remove(dataSnapshot.getValue(String.class));
                    adapterForCrNotice2.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        // DONE of Setting Up AppUser's CR NOTICE






    }

    public void DeleteRowAction2(View view) {
        EditText edit2 = (EditText) findViewById(R.id.delText2);
        String name2 = edit2.getText().toString();
        Query query2 = Cref2.orderByKey().equalTo(name2);
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapshot : dataSnapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        edit2.setText("");
    }
}
