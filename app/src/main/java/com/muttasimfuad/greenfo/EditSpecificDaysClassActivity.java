package com.muttasimfuad.greenfo;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Adapter.CustomAdapterEditRoutine;
import ModalClass.RoutineInfoForEditRoutine;

public class EditSpecificDaysClassActivity extends AppCompatActivity {

    ListView listViewClasses;
    List<RoutineInfoForEditRoutine> Classes;
    DatabaseReference databaseReference;
    FirebaseAuth mAuth;
    String CurrentUserPhoneNo;
    String TodaysDay;
    Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_specific_days_class);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addUser();
            }
        });

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();
        CurrentUserPhoneNo = user.getPhoneNumber();


        TextView ClassDetailsTV = (TextView) findViewById(R.id.classDetails);
        listViewClasses = (ListView) findViewById(R.id.listViewClassesLV);

        //list for store objects of user
        Classes = new ArrayList<>();

        if (getIntent() != null) {
            String info = getIntent().getStringExtra("info");

            switch (info) {

                case "x0":
                    ClassDetailsTV.setText("Friday's Class");
                    TodaysDay = "Friday";
                    break;
                case "x1":
                    ClassDetailsTV.setText("Saturday's Class");
                    TodaysDay = "Saturday";
                    break;
                case "x2":
                    ClassDetailsTV.setText("Sunday's Class");
                    TodaysDay = "Sunday";
                    break;
                case "x3":
                    ClassDetailsTV.setText("Monday's Class");
                    TodaysDay = "Monday";
                    break;
                case "x4":
                    ClassDetailsTV.setText("Tuesday's Class");
                    TodaysDay = "Tuesday";
                    break;
                case "x5":
                    ClassDetailsTV.setText("Wednesday's Class");
                    TodaysDay = "Wednesday";
                    break;
                case "x6":
                    ClassDetailsTV.setText("Thusrsday's Class");
                    TodaysDay = "Thursday";
                    break;


            }

            databaseReference = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(TodaysDay);


        }

        listViewClasses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                RoutineInfoForEditRoutine routineInfo = Classes.get(i);
                CallUpdateAndDeleteDialog(routineInfo.getCourse_code(), routineInfo.getName(), routineInfo.getTime(), routineInfo.getRoom());
            }
        });


    }

    private void addUser() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.addclass_dialog, null);
        dialogBuilder.setView(dialogView);
        //Access Dialog views
        final EditText updateTextCourseNo = (EditText) dialogView.findViewById(R.id.updateTextCourseNo);
        final EditText updateTextCourseName = (EditText) dialogView.findViewById(R.id.updateTextCourseName);
        final EditText updateTextClassTime = (EditText) dialogView.findViewById(R.id.updateTextClassTime);
        final EditText updateTextClassTime2 = (EditText) dialogView.findViewById(R.id.updateTextClassTime2);
        final EditText updateTextRoomNo = (EditText) dialogView.findViewById(R.id.updateTextRoomNo);
        final Button classStartTimeBT = (Button) dialogView.findViewById(R.id.startTimeBT);
        final Button classEdTimeBT = (Button) dialogView.findViewById(R.id.endTimeBT);

        final Button buttonAddCourse = (Button) dialogView.findViewById(R.id.buttonAddClass);
        //coursename for set dialog title
        dialogBuilder.setTitle("Add Course for "+TodaysDay);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        classStartTimeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);


                TimePickerDialog timePickerDialog = new TimePickerDialog(EditSpecificDaysClassActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String status = "AM";
                                String min = "";

                                if (hourOfDay > 11) {
                                    status = "PM";
                                }

                                int hour_of_12_hour_format;

                                if (hourOfDay > 12) {

                                    hour_of_12_hour_format = hourOfDay - 12;
                                } else {
                                    hour_of_12_hour_format = hourOfDay;
                                }

                                if (hourOfDay < 1) {

                                    hour_of_12_hour_format = hourOfDay + 12;
                                }

                                if (hourOfDay < 1) {

                                    hour_of_12_hour_format = hourOfDay + 12;
                                }

                                if (minute < 10) {

                                    min = "0" + String.valueOf(minute);
                                } else min = String.valueOf(minute);

                                updateTextClassTime.setText(hour_of_12_hour_format + ":" + min + " " + status);


                                //ClassStartTimeET.setText(hourOfDay + ":" + minute);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        classEdTimeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);


                TimePickerDialog timePickerDialog = new TimePickerDialog(EditSpecificDaysClassActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String status = "AM";
                                String min = "";

                                if (hourOfDay > 11) {
                                    status = "PM";
                                }

                                int hour_of_12_hour_format;


                                if (hourOfDay > 12) {

                                    hour_of_12_hour_format = hourOfDay - 12;
                                } else {
                                    hour_of_12_hour_format = hourOfDay;
                                }

                                if (minute < 10) {

                                    min = "0" + String.valueOf(minute);
                                } else min = String.valueOf(minute);

                                updateTextClassTime2.setText(hour_of_12_hour_format + ":" + min + " " + status);

                                //ClassEndTimeET.setText(hourOfDay + ":" + minute);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        // Click listener for Update data
        buttonAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String no = updateTextCourseNo.getText().toString().trim();
                String name = updateTextCourseName.getText().toString().trim();
                String time = updateTextClassTime.getText().toString().trim() + " - " + updateTextClassTime2.getText().toString().trim();
                String roomnumber = updateTextRoomNo.getText().toString().trim();


                if (!TextUtils.isEmpty(no)) {
                    if (!TextUtils.isEmpty(name)) {
                        if (!TextUtils.isEmpty(time)) {
                            if (!TextUtils.isEmpty(roomnumber)) {
                                //Method for update data
                                if(!updateTextClassTime.getText().toString().trim().equals("Start Time") && !updateTextClassTime2.getText().toString().trim().equals("End Time")) {
                                    updateClass(no, name, time, roomnumber);
                                    b.dismiss();
                                }else {
                                    Toast.makeText(getApplicationContext(), "Select Class Time!", Toast.LENGTH_LONG).show();
                                }

                            }
                        }
                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Fill Up The Form!", Toast.LENGTH_LONG).show();

                }

                //checking if the value is provided or not Here, you can Add More Validation as you required


            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //clearing the previous User list
                Classes.clear();

                //getting all nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting User from firebase console
                    RoutineInfoForEditRoutine Class = postSnapshot.getValue(RoutineInfoForEditRoutine.class);
                    //adding User to the list
                    Classes.add(Class);
                }
                //creating Userlist adapter
                CustomAdapterEditRoutine classAdapter = new CustomAdapterEditRoutine(EditSpecificDaysClassActivity.this, Classes);
                //attaching adapter to the listview
                listViewClasses.setAdapter(classAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    //_____________________________________

    private void CallUpdateAndDeleteDialog(final String coursecode, String coursename, String coursetime, String roomnumber) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_dialog, null);
        dialogBuilder.setView(dialogView);
        //Access Dialog views
        final EditText updateTextCourseNo = (EditText) dialogView.findViewById(R.id.updateTextCourseNo);
        final EditText updateTextCourseName = (EditText) dialogView.findViewById(R.id.updateTextCourseName);
        final EditText updateTextClassTime = (EditText) dialogView.findViewById(R.id.updateTextClassTime);
        final EditText updateTextClassTime2 = (EditText) dialogView.findViewById(R.id.updateTextClassTime2);
        final EditText updateTextRoomNo = (EditText) dialogView.findViewById(R.id.updateTextRoomNo);
        final Button classStartTimeBT = (Button) dialogView.findViewById(R.id.startTimeBT);
        final Button classEdTimeBT = (Button) dialogView.findViewById(R.id.endTimeBT);


        String kept = coursetime.substring(0, coursetime.indexOf("-"));
        String remainder = coursetime.substring(coursetime.indexOf("-") + 1, coursetime.length());

        updateTextCourseNo.setText(coursecode);
        updateTextCourseName.setText(coursename);
        updateTextClassTime.setText(kept);
        updateTextClassTime2.setText(remainder);
        updateTextRoomNo.setText(roomnumber);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdateClass);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeleteClass);
        //coursename for set dialog title
        dialogBuilder.setTitle(coursename);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        classStartTimeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);


                TimePickerDialog timePickerDialog = new TimePickerDialog(EditSpecificDaysClassActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String status = "AM";
                                String min = "";

                                if (hourOfDay > 11) {
                                    status = "PM";
                                }

                                int hour_of_12_hour_format;

                                if (hourOfDay > 12) {

                                    hour_of_12_hour_format = hourOfDay - 12;
                                } else {
                                    hour_of_12_hour_format = hourOfDay;
                                }

                                if (hourOfDay < 1) {

                                    hour_of_12_hour_format = hourOfDay + 12;
                                }

                                if (hourOfDay < 1) {

                                    hour_of_12_hour_format = hourOfDay + 12;
                                }

                                if (minute < 10) {

                                    min = "0" + String.valueOf(minute);
                                } else min = String.valueOf(minute);

                                updateTextClassTime.setText(hour_of_12_hour_format + ":" + min + " " + status);


                                //ClassStartTimeET.setText(hourOfDay + ":" + minute);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        classEdTimeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);


                TimePickerDialog timePickerDialog = new TimePickerDialog(EditSpecificDaysClassActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String status = "AM";
                                String min = "";

                                if (hourOfDay > 11) {
                                    status = "PM";
                                }

                                int hour_of_12_hour_format;


                                if (hourOfDay > 12) {

                                    hour_of_12_hour_format = hourOfDay - 12;
                                } else {
                                    hour_of_12_hour_format = hourOfDay;
                                }

                                if (minute < 10) {

                                    min = "0" + String.valueOf(minute);
                                } else min = String.valueOf(minute);

                                updateTextClassTime2.setText(hour_of_12_hour_format + ":" + min + " " + status);

                                //ClassEndTimeET.setText(hourOfDay + ":" + minute);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        // Click listener for Update data
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String no = updateTextCourseNo.getText().toString().trim();
                String name = updateTextCourseName.getText().toString().trim();
                String time = updateTextClassTime.getText().toString().trim() + " - " + updateTextClassTime2.getText().toString().trim();
                String roomnumber = updateTextRoomNo.getText().toString().trim();

                if (no.equals(coursecode)) {

                    if (!TextUtils.isEmpty(no)) {
                        if (!TextUtils.isEmpty(name)) {
                            if (!TextUtils.isEmpty(time)) {
                                if (!TextUtils.isEmpty(roomnumber)) {
                                    //Method for update data
                                    updateClass(no, name, time, roomnumber);
                                    b.dismiss();
                                }
                            }
                        }
                    }else Toast.makeText(getApplicationContext(), "Fill Up The Form!", Toast.LENGTH_LONG).show();

                } else {

                    deleteClass(coursecode);

                    if (!TextUtils.isEmpty(no)) {
                        if (!TextUtils.isEmpty(name)) {
                            if (!TextUtils.isEmpty(time)) {
                                if (!TextUtils.isEmpty(roomnumber)) {
                                    //Method for update data
                                    updateClass(no, name, time, roomnumber);
                                    b.dismiss();
                                }
                            }
                        }
                    }


                }

                //checking if the value is provided or not Here, you can Add More Validation as you required


            }
        });

        // Click listener for Delete data
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Method for delete data
                deleteClass(coursecode);
                b.dismiss();
            }
        });
    }

    private boolean updateClass(String coursecod, String name, String classtime, String roomnum) {
        //getting the specified User reference
        DatabaseReference UpdateReference = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(TodaysDay).child(coursecod);
        RoutineInfoForEditRoutine routineInfo = new RoutineInfoForEditRoutine(coursecod, name, classtime, roomnum);
        //update  User  to firebase
        UpdateReference.setValue(routineInfo);
        Toast.makeText(getApplicationContext(), "Course Updated", Toast.LENGTH_LONG).show();
        return true;
    }

    private boolean deleteClass(String coursecode) {
        //getting the specified User reference
        DatabaseReference DeleteReference = FirebaseDatabase.getInstance().getReference().child("user_info").child(CurrentUserPhoneNo).child("courses").child(TodaysDay).child(coursecode);
        //removing User
        DeleteReference.removeValue();
        Toast.makeText(getApplicationContext(), "Course Deleted", Toast.LENGTH_LONG).show();
        return true;
    }

    //______________________________

    @Override
    public void onBackPressed() {
        startActivity(new Intent(EditSpecificDaysClassActivity.this, MainActivity.class));
        finish();
    }


}
