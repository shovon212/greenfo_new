package com.muttasimfuad.greenfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SetupClass extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText friendsPhoneOrID;
    private TextView DisPlayText;
    private Button CopyNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_class);
        friendsPhoneOrID = (EditText) findViewById(R.id.friendsPhoneOrIdET);
        DisPlayText = (TextView) findViewById(R.id.resultBoxTV);
        CopyNow = (Button) findViewById(R.id.copyRoutineFromFriendBTN);


    }



    // check

    public void copyRoutineAction(View view) {

        if (isNetworkConnected()) {


            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked

                            try {
                                mAuth = FirebaseAuth.getInstance();
                                if (mAuth.getCurrentUser() != null) {
                                    FirebaseUser user = mAuth.getCurrentUser();

                                    String PhoneOrId = friendsPhoneOrID.getText().toString().trim();
                                    if (!PhoneOrId.equals("")) {

                                        if (PhoneOrId.length() == 11) {
                                            String FriendsPhoneNo = "+88" + PhoneOrId;

                                            String CurrentUserPhoneNo = user.getPhoneNumber();

                                            DatabaseReference rootpath = FirebaseDatabase.getInstance().getReference("user_info");
                                            DatabaseReference fromPath = rootpath.child(FriendsPhoneNo).child("courses");
                                            DatabaseReference toPath = rootpath.child(CurrentUserPhoneNo).child("courses");


                                            moveGameRoom(fromPath, toPath);

                                            friendsPhoneOrID.setText("");

                                            CopyNow.setEnabled(false);

                                            CopyNow.setEnabled(true);


                                        } else {
                                            DisPlayText.setText("Wrong Input! Phone Number Must Be 11 Digit Long!(Example: 01674112233)");
                                            CopyNow.setEnabled(true);
                                        }

                                    } else {
                                        Toast.makeText(SetupClass.this, "Type your friend's Phone No first!", Toast.LENGTH_SHORT).show();
                                        CopyNow.setEnabled(true);
                                    }


                                } else {
                                    Toast.makeText(SetupClass.this, "You must have to login to perform this!", Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(SetupClass.this, "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
                            }


                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


        } else {
            Toast.makeText(SetupClass.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }

    }


    //check done





    public void AddClassAction(View view) {

        if (isNetworkConnected()) {

            try {
                mAuth = FirebaseAuth.getInstance();
                if (mAuth.getCurrentUser() != null) {
                    finish();
                    startActivity(new Intent(SetupClass.this, AddRoutineActivity.class));

                } else {
                    Toast.makeText(SetupClass.this, "You must have to login to perform this!", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(SetupClass.this, "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();

            }


        }else {
            Toast.makeText(SetupClass.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }
    }

    public void EditClassAction(View view) {
//        mAuth = FirebaseAuth.getInstance();
//        if (mAuth.getCurrentUser() != null) {
//            DisPlayText.setText("You will get this feature  in the next version of this app! Rather You Can Use 'DELETE CLASS' Button to delete all courses! and Add or Copy Class Again.");
//        } else {
//            Toast.makeText(SetupClass.this, "You must have to login to perform this!", Toast.LENGTH_LONG).show();
//        }
        if (isNetworkConnected()) {

            try {
                mAuth = FirebaseAuth.getInstance();
                if (mAuth.getCurrentUser() != null) {
                    finish();
                    startActivity(new Intent(SetupClass.this, EditClassActivity.class));

                } else {
                    Toast.makeText(SetupClass.this, "You must have to login to perform this!", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(SetupClass.this, "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();

            }


        }else {
            Toast.makeText(SetupClass.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }

    }

    public void DeleteClass(View view) {

        if (isNetworkConnected()) {


            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked

                            try {
                                mAuth = FirebaseAuth.getInstance();
                                if (mAuth.getCurrentUser() != null) {
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    String MyPhoneNo=user.getPhoneNumber();
                                    String DeletedPhoneNoRef= "+8800000000000";
                                    DatabaseReference rootpath = FirebaseDatabase.getInstance().getReference("user_info");
                                    DatabaseReference fromPath = rootpath.child(DeletedPhoneNoRef).child("courses");
                                    DatabaseReference toPath = rootpath.child(MyPhoneNo).child("courses");
                                    deleteAllClasses(fromPath,toPath);

                                } else {
                                    Toast.makeText(SetupClass.this, "You must have to login to perform this!!", Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(SetupClass.this, "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
                            }


                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext()); //creating the Dialog from within a View.OnClickListener, you can use view.getContext() to get the Context. Alternatively you can use yourFragmentName.getActivity()
            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


        } else {
            Toast.makeText(SetupClass.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }

    }



    @Override
    public void onBackPressed() {
        startActivity(new Intent(SetupClass.this, MainActivity.class));
        finish();
    }

    private void moveGameRoom(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                        if (firebaseError != null) {
                            DisPlayText.setText("Copy failed");
                        } else {
                            DisPlayText.setText("Successfully Copied!!");

                        }
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void deleteAllClasses(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                        if (firebaseError != null) {
                            DisPlayText.setText("Delete failed");
                        } else {
                            DisPlayText.setText("Successfully Deleted All Course!!");

                        }
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }






}
