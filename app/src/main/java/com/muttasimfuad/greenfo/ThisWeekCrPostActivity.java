package com.muttasimfuad.greenfo;

import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class ThisWeekCrPostActivity extends AppCompatActivity {

    private DatabaseReference Cref;
    private String CRdept, CRbatch, CRshift,CRsec;
    private TextView userBatchInCrView;

    private ListView crNoticeListView;
    private ArrayList<String> crNoticeList = new ArrayList<>();

    String mKey;

    List<String> ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_this_week_cr_post);

        userBatchInCrView = (TextView) findViewById(R.id.batchVIEWCR);
        crNoticeListView = (ListView) findViewById(R.id.thisweektabCrViewLV);

        Intent cr = getIntent();

        CRdept = cr.getStringExtra("crdept");
        CRbatch = cr.getStringExtra("crbatch");
        CRshift = cr.getStringExtra("crshift");
        CRsec = cr.getStringExtra("crsec");


        // FOUND USERS DEPT...


        try {
            userBatchInCrView.setText("Batch: " + CRdept + " " + CRbatch + " " + CRshift+" "+CRsec);

            final ArrayAdapter<String> adapterForCrNotice = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, crNoticeList);
            crNoticeListView.setAdapter(adapterForCrNotice);
            Cref = FirebaseDatabase.getInstance().getReference().child("batches").child(CRdept).child(CRbatch).child(CRshift).child("notifn_cr").child(CRsec).child("week_tab_cr").child("w1");
            Cref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    crNoticeList.add(dataSnapshot.getValue(String.class));
                    adapterForCrNotice.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {


                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    crNoticeList.remove(dataSnapshot.getValue(String.class));
                    adapterForCrNotice.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        // DONE of Setting Up AppUser's CR NOTICE

//        Cref.getRoot().addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot messages : dataSnapshot.getChildren()) {
//
//                    Map<String,Object> value = (Map<String, Object>) dataSnapshot.getValue();
//                    keyList.add(dataSnapshot.getKey());
//                    items.add(value.get(keyList));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//        /*handle errors*/
//            }
//        });
//
//        crNoticeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                items.remove(position);
//               // adapt.notifyDataSetChanged();
//                //new code below
//                Cref.getRoot().child(keyList.get(position)).removeValue();
//                keyList.remove(position);
//            }
//        });


        //crNoticeListView.






    }

    public void DeleteRowAction(View view) {
        EditText edit = (EditText) findViewById(R.id.delText);
        String name = edit.getText().toString();
        Query query = Cref.orderByKey().equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapshot : dataSnapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        edit.setText("");
    }
}

