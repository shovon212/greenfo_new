package com.muttasimfuad.greenfo;

/**
 * Created by Fuad on 29-Jan-18.
 */

public class GubianChecker {
    public String Dept;
    public String Dcode;
    public String DcodeCSEev;
    public String DcodeEEEev;
    public String DcodeTEXev;

    public String getDcodeCSEev() {
        return DcodeCSEev;
    }

    public String getDcodeEEEev() {
        return DcodeEEEev;
    }

    public String getDcodeTEXev() {
        return DcodeTEXev;
    }

    public String getDept() {
        return Dept;
    }

    public void setDept(String dept) {
        Dept = dept;
        switch (dept) {
            case "CSE":
                Dcode = "002";
                DcodeCSEev = "015";
                break;
            case "EEE":
                Dcode = "001";
                DcodeEEEev = "010";
                break;
            case "TEXTILE":
                Dcode = "003";
                DcodeTEXev = "014";
                break;
            case "ENGLISH":
                Dcode = "020";
                break;
            case "BBA":
                Dcode = "006";
                break;
            case "MBA":
                Dcode = "005";
                break;
            case "EMBA":
                Dcode = "004";
                break;
            case "SOCIOLOGY":
                Dcode = "018";
                break;
            case "ANTHROPOLOGY":
                Dcode = "dont know";
                break;
            case "ANTHROPOLOGY-MSS":
                Dcode = "dont know";
                break;
            case "LLB(Honors)":
                Dcode = "011";
                break;
            case "LLB(Pass)":
                Dcode = "dont know";
                break;
            case "LLM":
                Dcode = "012";
                break;
            case "FTDM":
                Dcode = "019";
                break;
        }
    }

    public String getDcode() {
        return Dcode;
    }

    public void setDcode(String dcode) {
        Dcode = dcode;
    }
}
