package com.muttasimfuad.greenfo;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddRoutineActivity extends AppCompatActivity {

    EditText CourseCode, CourseName, RoomNo, ClassStartTimeET,ClassEndTimeET;
    CheckBox SatCB, SunCB, MonCB, TueCB, WedCB, ThuCB, FriCB;
    Button AddAsRoutine;

    FirebaseAuth mAuth;
    FirebaseUser user;
    DatabaseReference user_ifo;
    DatabaseReference currentUserRefNew;
    DatabaseReference currentUserRefChildRef;
    String CurrentUserPhoneNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_routine);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        CurrentUserPhoneNo = user.getPhoneNumber();

        CourseCode = (EditText) findViewById(R.id.courseCodeET);
        CourseName = (EditText) findViewById(R.id.courseNameET);
        RoomNo = (EditText) findViewById(R.id.roomNoET);
        ClassEndTimeET = (EditText) findViewById(R.id.classTimeET);
        ClassStartTimeET = (EditText) findViewById(R.id.c);

        SatCB = (CheckBox) findViewById(R.id.satCB);
        SunCB = (CheckBox) findViewById(R.id.sunCB);
        MonCB = (CheckBox) findViewById(R.id.monCB);
        TueCB = (CheckBox) findViewById(R.id.tueCB);
        WedCB = (CheckBox) findViewById(R.id.wedCB);
        ThuCB = (CheckBox) findViewById(R.id.thuCB);
        FriCB = (CheckBox) findViewById(R.id.friCB);

        AddAsRoutine = (Button) findViewById(R.id.addCourseBT);


        user_ifo = FirebaseDatabase.getInstance().getReference("user_info");
        currentUserRefNew = user_ifo.child(CurrentUserPhoneNo).child("courses");

    }

    public void AddToRoutineOnClick(View view) {

        if (isNetworkConnected() == true) {

            try {
                String courseCode = CourseCode.getText().toString().trim();
                String courseName = CourseName.getText().toString().trim();
                String roomNo = RoomNo.getText().toString().trim();
                String classTimex = ClassEndTimeET.getText().toString().trim();
                String classTime = ClassStartTimeET.getText().toString().trim()+" - "+classTimex;


                if (!courseCode.equals("") && !courseName.equals("") && !roomNo.equals("") && !classTime.equals("")) {

                    if (SatCB.isChecked() || SunCB.isChecked() || MonCB.isChecked() || TueCB.isChecked() || WedCB.isChecked() || ThuCB.isChecked() || FriCB.isChecked()) {

                        if (SatCB.isChecked()) {

                            currentUserRefChildRef = currentUserRefNew.child("Saturday").child(courseCode);

                            currentUserRefChildRef.child("course_code").setValue(courseCode);
                            currentUserRefChildRef.child("name").setValue(courseName);
                            currentUserRefChildRef.child("time").setValue(classTime);
                            currentUserRefChildRef.child("room").setValue(roomNo);
                        }

                        if (SunCB.isChecked()) {

                            currentUserRefChildRef = currentUserRefNew.child("Sunday").child(courseCode);

                            currentUserRefChildRef.child("course_code").setValue(courseCode);
                            currentUserRefChildRef.child("name").setValue(courseName);
                            currentUserRefChildRef.child("time").setValue(classTime);
                            currentUserRefChildRef.child("room").setValue(roomNo);
                        }

                        if (MonCB.isChecked()) {
                            currentUserRefChildRef = currentUserRefNew.child("Monday").child(courseCode);

                            currentUserRefChildRef.child("course_code").setValue(courseCode);
                            currentUserRefChildRef.child("name").setValue(courseName);
                            currentUserRefChildRef.child("time").setValue(classTime);
                            currentUserRefChildRef.child("room").setValue(roomNo);
                        }

                        if (TueCB.isChecked()) {
                            currentUserRefChildRef = currentUserRefNew.child("Tuesday").child(courseCode);

                            currentUserRefChildRef.child("course_code").setValue(courseCode);
                            currentUserRefChildRef.child("name").setValue(courseName);
                            currentUserRefChildRef.child("time").setValue(classTime);
                            currentUserRefChildRef.child("room").setValue(roomNo);
                        }

                        if (WedCB.isChecked()) {

                            currentUserRefChildRef = currentUserRefNew.child("Wednesday").child(courseCode);

                            currentUserRefChildRef.child("course_code").setValue(courseCode);
                            currentUserRefChildRef.child("name").setValue(courseName);
                            currentUserRefChildRef.child("time").setValue(classTime);
                            currentUserRefChildRef.child("room").setValue(roomNo);
                        }

                        if (ThuCB.isChecked()) {

                            currentUserRefChildRef = currentUserRefNew.child("Thursday").child(courseCode);

                            currentUserRefChildRef.child("course_code").setValue(courseCode);
                            currentUserRefChildRef.child("name").setValue(courseName);
                            currentUserRefChildRef.child("time").setValue(classTime);
                            currentUserRefChildRef.child("room").setValue(roomNo);
                        }

                        if (FriCB.isChecked()) {

                            currentUserRefChildRef = currentUserRefNew.child("Friday").child(courseCode);

                            currentUserRefChildRef.child("course_code").setValue(courseCode);
                            currentUserRefChildRef.child("name").setValue(courseName);
                            currentUserRefChildRef.child("time").setValue(classTime);
                            currentUserRefChildRef.child("room").setValue(roomNo);
                        }

                        CourseCode.setText("");
                        CourseName.setText("");
                        RoomNo.setText("");
                        ClassStartTimeET.setText("");
                        ClassEndTimeET.setText("");

                        SatCB.setChecked(false);
                        SunCB.setChecked(false);
                        MonCB.setChecked(false);
                        TueCB.setChecked(false);
                        WedCB.setChecked(false);
                        ThuCB.setChecked(false);
                        FriCB.setChecked(false);

                        Toast.makeText(AddRoutineActivity.this, "Information Saved !", Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(AddRoutineActivity.this, "Please select day of class or classes!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(AddRoutineActivity.this, "Please Fill up all the information!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(AddRoutineActivity.this, "Failed !Poor Network Connection!", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(AddRoutineActivity.this, "Check Your Internet Connection!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(AddRoutineActivity.this, MainActivity.class));
        finish();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void setStartTime(View view) {

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);


        TimePickerDialog timePickerDialog = new TimePickerDialog(AddRoutineActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String status = "AM";
                        String min = "";

                        if(hourOfDay > 11)
                        {
                            status = "PM";
                        }

                        int hour_of_12_hour_format;

                        if(hourOfDay > 12){

                            hour_of_12_hour_format = hourOfDay - 12;
                        }
                        else {
                            hour_of_12_hour_format = hourOfDay;
                        }

                        if(hourOfDay < 1){

                            hour_of_12_hour_format = hourOfDay + 12;
                        }

                        if(hourOfDay < 1){

                            hour_of_12_hour_format = hourOfDay + 12;
                        }

                        if(minute < 10)
                        {

                            min ="0"+String.valueOf(minute);
                        }else min= String.valueOf(minute);

                        ClassStartTimeET.setText(hour_of_12_hour_format + ":" + min + " " + status);

                        //ClassStartTimeET.setText(hourOfDay + ":" + minute);
                    }
                }, hour, minute, false);
        timePickerDialog.show();

    }

    public void setEndTime(View view) {

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);


        TimePickerDialog timePickerDialog = new TimePickerDialog(AddRoutineActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String status = "AM";
                        String min = "";

                        if(hourOfDay > 11)
                        {
                            status = "PM";
                        }

                        int hour_of_12_hour_format;



                        if(hourOfDay > 12){

                            hour_of_12_hour_format = hourOfDay - 12;
                        }
                        else {
                            hour_of_12_hour_format = hourOfDay;
                        }

                        if(minute < 10)
                        {

                            min ="0"+String.valueOf(minute);
                        }else min= String.valueOf(minute);

                        ClassEndTimeET.setText(hour_of_12_hour_format + ":" + min + " " + status);

                        //ClassEndTimeET.setText(hourOfDay + ":" + minute);
                    }
                }, hour, minute, false);
        timePickerDialog.show();
    }
}
