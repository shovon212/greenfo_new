package com.muttasimfuad.greenfo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

public class SeeMe extends AppCompatActivity {

    ImageView imageView;
    TextView Name, Batch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_me);
        imageView = (ImageView) findViewById(R.id.seemeIV) ;
        Name = (TextView) findViewById(R.id.userNameTV) ;
        Batch = (TextView) findViewById(R.id.userInfoTV) ;

        Intent cr = getIntent();
        String pp = (String) cr.getStringExtra("propic");
        String name = (String) cr.getStringExtra("name");
        String batch = (String) cr.getStringExtra("batch");

        try {
            Glide.with(this)
                    .load(pp)
                    .into(imageView);
            Name.setText(name);
            Batch.setText(batch);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
