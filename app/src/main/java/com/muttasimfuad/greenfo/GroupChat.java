package com.muttasimfuad.greenfo;

        import android.content.Intent;
        import androidx.appcompat.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.Toast;

        import com.firebase.ui.database.FirebaseListAdapter;
        import com.google.android.gms.ads.AdRequest;
        import com.google.android.gms.ads.AdSize;
        import com.google.android.gms.ads.AdView;
        import com.google.android.gms.ads.MobileAds;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.auth.FirebaseUser;
        import com.google.firebase.database.ChildEventListener;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.Query;
        import com.muttasimfuad.greenfo.activity.ChatMessage;
        import com.muttasimfuad.greenfo.activity.MessageAdapter;
        import com.muttasimfuad.greenfo.utils.FirebaseUtils;
        import java.text.SimpleDateFormat;
        import java.util.Calendar;
        import java.util.Map;

public class GroupChat extends AppCompatActivity {
    private static final int SIGN_IN_REQUEST_CODE = 111;
    private FirebaseListAdapter<ChatMessage> adapter;
    private ListView listView;
    private String loggedInUserName = "";
    String CurrentUserPhoneNo,MyName;
    FirebaseAuth mAuth;
    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference myRef;

    private AdView mAdView;
    private ImageView sendMsg;

    Calendar calander;
    SimpleDateFormat simpledateformat;
    String Date,propicURL,dep,batch,shift,sec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);

        //Admob Code
        MobileAds.initialize(this, "ca-app-pub-8614904278210110~2305070735");
        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-8614904278210110/6708964574");
// TODO: Add adView to your view hierarchy.

        mAdView = findViewById(R.id.adViewGrpChat);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        sendMsg = (ImageView) findViewById(R.id.fab);

        //Admob Code Done

        // Getting user name


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        if (mAuth.getCurrentUser() != null) {

            CurrentUserPhoneNo = user.getPhoneNumber();

            // getting  user id fcm

            mFirebaseDatabase = FirebaseDatabase.getInstance();
            myRef = mFirebaseDatabase.getReference();

            DatabaseReference user_batch_Ref = myRef.child("user_info").child(CurrentUserPhoneNo);

            Query findBatchQuery = user_batch_Ref.orderByChild(CurrentUserPhoneNo);

            findBatchQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    showDataofUserBatch(dataSnapshot);
                    // FOUND USERS DEPT and ID...



                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i("Item not found: ", "this item is not in the list");
                }
            });



        }


        // got user name



        //find views by Ids
   //     FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final EditText input = (EditText) findViewById(R.id.input);
        listView = (ListView) findViewById(R.id.list);

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Start sign in/sign up activity
//            startActivityForResult(AuthUI.getInstance()
//                    .createSignInIntentBuilder()
//                    .build(), SIGN_IN_REQUEST_CODE);
            Toast.makeText(GroupChat.this, "Not Logged in!", Toast.LENGTH_SHORT).show();
        } else {
            // AppUser is already signed in, show list of messages
            showAllOldMessages();
        }

        sendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calander = Calendar.getInstance();
                simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                Date = simpledateformat.format(calander.getTime());
                long number = System.currentTimeMillis();

                final String msgID = number+"_"+ Date.replace(".", "_")+"_"+MyName+"_"+dep+"_"+batch+"_"+shift+"_"+sec+ FirebaseUtils.getCurrentUser().getPhoneNumber();
                final String userfullbatchinfo = dep+"_"+batch+"_"+shift;
                if (input.getText().toString().trim().equals("")) {
                    Toast.makeText(GroupChat.this, "Please enter a msg first!", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseDatabase.getInstance()
                            .getReference().child("group_chat").child("m1")
                            .child(msgID)
                            .setValue(new ChatMessage(input.getText().toString(),
                                    FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber(),
                                    FirebaseAuth.getInstance().getCurrentUser().getUid(),MyName,propicURL,msgID,userfullbatchinfo)

                                    //ChatMessage(String messageText, String messageUser, String messageUserId) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            );
                    input.setText("");
                }
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Signed in successful!", Toast.LENGTH_LONG).show();
                showAllOldMessages();
            } else {
                Toast.makeText(this, "Sign in failed, please try again later", Toast.LENGTH_LONG).show();

                // Close the app
                finish();
            }
        }
    }

    private void showAllOldMessages() {
        loggedInUserName = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.d("Main", "user id: " + loggedInUserName);

        adapter = new MessageAdapter(this, ChatMessage.class, R.layout.item_in_message,
                FirebaseDatabase.getInstance().getReference().child("group_chat").child("m1")); //MessageAdapter(GroupChat activity, Class<ChatMessage> modelClass, int modelLayout, DatabaseReference ref)
        listView.setAdapter(adapter);
    }

    public String getLoggedInUserName() {
        return loggedInUserName;
    }

    private void showDataofUserBatch(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
            //           userDept = String.valueOf(value.get("department"));
            MyName = String.valueOf(value.get("username"));
            propicURL= String.valueOf(value.get("propicurl"));
            batch = String.valueOf(value.get("batch"));
            shift = String.valueOf(value.get("shift"));
            dep = String.valueOf(value.get("department"));
            sec = String.valueOf(value.get("sec"));

        }
    }
}
