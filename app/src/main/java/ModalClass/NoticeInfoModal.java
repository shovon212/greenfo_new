package ModalClass;


public class NoticeInfoModal {
    private String postTitle;
    private String postImageUrl;
    private String postText;
    private Long timeCreated;
    private Long numComments;


    public NoticeInfoModal() {

    }

    public NoticeInfoModal(String postTitle, String postImageUrl, String postText, Long timeCreated, Long numComments) {
        this.postTitle = postTitle;
        this.postImageUrl = postImageUrl;
        this.postText = postText;
        this.timeCreated = timeCreated;
        this.numComments = numComments;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostImageUrl() {
        return postImageUrl;
    }

    public void setPostImageUrl(String postImageUrl) {
        this.postImageUrl = postImageUrl;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public Long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Long getNumComments() {
        return numComments;
    }

    public void setNumComments(Long numComments) {
        this.numComments = numComments;
    }
}
