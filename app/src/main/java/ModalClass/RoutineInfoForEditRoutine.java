package ModalClass;


public class RoutineInfoForEditRoutine {
    private String course_code;
    private String name;
    private String time;
    private String room;

    public RoutineInfoForEditRoutine(String course_code, String name, String time, String room) {
        this.course_code = course_code;
        this.name = name;
        this.time = time;
        this.room = room;
    }

    public String getCourse_code() {
        return course_code;
    }

    public void setCourse_code(String course_code) {
        this.course_code = course_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public RoutineInfoForEditRoutine() {

    }
}

