package ModalClass;

/**
 * Created by Fuad on 11/10/2017.
 */

public class RoutineInfo{
    private String ClassTime;
    private String CourseCode;
    private String CourseName;
    private String RoomNo;

    public RoutineInfo() {

    }


    public String getClassTime() {
        return ClassTime;
    }

    public void setClassTime(String classTime) {
        ClassTime = classTime;
    }

    public String getCourseCode() {
        return CourseCode;
    }

    public void setCourseCode(String courseCode) {
        CourseCode = courseCode;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public String getRoomNo() {
        return RoomNo;
    }

    public void setRoomNo(String roomNo) {
        RoomNo = roomNo;
    }

    public RoutineInfo(String classTime, String courseCode, String courseName, String roomNo) {
        ClassTime = classTime;
        CourseCode = courseCode;
        CourseName = courseName;
        RoomNo = roomNo;
    }
}
