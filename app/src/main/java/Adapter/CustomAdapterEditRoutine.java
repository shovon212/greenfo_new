package Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.muttasimfuad.greenfo.R;

import java.util.List;

import ModalClass.RoutineInfoForEditRoutine;

public class CustomAdapterEditRoutine extends ArrayAdapter<RoutineInfoForEditRoutine> {
    private Activity context;
    List<RoutineInfoForEditRoutine> Classes;

    public CustomAdapterEditRoutine (Activity context, List<RoutineInfoForEditRoutine> Classes){
        super(context, R.layout.row_style_of_editclass_routine, Classes);
        this.context = context;
        this.Classes = Classes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.row_style_of_editclass_routine, null, true);
        //initialize
        TextView courseCodeTV = (TextView) listViewItem.findViewById(R.id.courseCodeTvEditRoutinee);
        TextView courseNameTV = (TextView) listViewItem.findViewById(R.id.courseNameTvEditRoutine);
        TextView courseTimeTV = (TextView) listViewItem.findViewById(R.id.courseTimeTvEditRoutine);
        TextView courseRoomTV = (TextView) listViewItem.findViewById(R.id.courseRoomNoTvEditRoutine);

        //getting user at position
        RoutineInfoForEditRoutine routineInfo = Classes.get(position);
        Log.v("Routine info:", routineInfo.toString());

        //set user name
        courseCodeTV.setText(routineInfo.getCourse_code());
        //set user email
        courseNameTV.setText(routineInfo.getName());
        //set user mobilenumber
        courseTimeTV.setText(routineInfo.getTime());
        courseRoomTV.setText(routineInfo.getRoom());

        return listViewItem;
    }

}
