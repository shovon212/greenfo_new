package Adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.muttasimfuad.greenfo.R;
import com.muttasimfuad.greenfo.models.Post;
import com.muttasimfuad.greenfo.ui.activities.PostActivityNotice;
import com.muttasimfuad.greenfo.utils.Constants;

import java.io.Serializable;
import java.util.List;


public class CustomAdapterNoticeList extends ArrayAdapter<Post> {
    private Activity context;
    List<Post> Notices;
    private String comment;

    public CustomAdapterNoticeList(Activity context, List<Post> notices){
        super(context, R.layout.row_style_of_notice_board, notices);
        this.context = context;
        this.Notices = notices;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.row_style_of_notice_board, null, true);
        //initialize
        TextView noticeTitleTV = (TextView) listViewItem.findViewById(R.id.noticeTitleinHomeId);
        TextView noticeDescTV = (TextView) listViewItem.findViewById(R.id.noticeDescTV);
        TextView noticeTimeTv = (TextView) listViewItem.findViewById(R.id.noticePostedTimeHomeId);
        TextView totalCommentTV = (TextView) listViewItem.findViewById(R.id.totalCommentsHomeNoticeId);
        TextView detailedNoticeTV = (TextView) listViewItem.findViewById(R.id.detailsBtnHomeid);

        //getting user at position
        Post noticeInfoModal = Notices.get(position);
        //mPost = Notices.get(position);

        //Log.v("Notice info:", noticeInfoModal.toString());

        //set user email
        noticeTitleTV.setText(noticeInfoModal.getPostTitle().toUpperCase());
        //set user mobilenumber
        noticeDescTV.setText(noticeInfoModal.getPostText());
        noticeTimeTv.setText("Posted: "+DateUtils.getRelativeTimeSpanString(noticeInfoModal.getTimeCreated()));
        if(noticeInfoModal.getNumComments()<2){
            comment = " Comment";
        }else comment = " Comments";
        totalCommentTV.setText(String.valueOf(noticeInfoModal.getNumComments())+comment);

        detailedNoticeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PostActivityNotice.class);
                intent.putExtra(Constants.EXTRA_POST, noticeInfoModal);
                context.startActivity(intent);
            }
        });

        noticeDescTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PostActivityNotice.class);
                intent.putExtra(Constants.EXTRA_POST, noticeInfoModal);
                context.startActivity(intent);
            }
        });


        return listViewItem;
    }

}
