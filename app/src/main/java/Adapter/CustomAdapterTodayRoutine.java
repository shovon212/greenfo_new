package Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.muttasimfuad.greenfo.R;

import java.util.List;

import ModalClass.RoutineInfo;

/**
 * Created by Fuad on 11/11/2017.
 */

public class CustomAdapterTodayRoutine extends ArrayAdapter<RoutineInfo> {


    private List<RoutineInfo> classInfos;
//    private TextView Today;
//    private TextView Name;
//    private  TextView PhoneNo;
//    private TextView relationStatus;

    private Activity context;


    public CustomAdapterTodayRoutine(Activity context, List<RoutineInfo> classInfos) {
        super(context, R.layout.row_style_of_today, classInfos);
        this.context = context;
        this.classInfos = classInfos;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
       // Log.v("Shovon Context Status:", context.toString());
        LayoutInflater inflater = context.getLayoutInflater();
        View convertView2 = inflater.inflate(R.layout.row_style_of_today, null, true);


        //View listViewItem = inflater.inflate(R.layout.row_style_of_editclass_routine, null, true);
        //initialize

        TextView Today = (TextView) convertView2.findViewById(R.id.textView4);
        TextView Name = (TextView) convertView2.findViewById(R.id.nameTv);
        TextView PhoneNo = (TextView) convertView2.findViewById(R.id.phoneTv);
        TextView relationStatus = (TextView) convertView2.findViewById(R.id.marryTv);


        Today.setText(classInfos.get(position).getClassTime());
        Name.setText(classInfos.get(position).getCourseName());
        PhoneNo.setText(classInfos.get(position).getRoomNo());
        relationStatus.setText(classInfos.get(position).getCourseCode());


        return convertView2;
    }
}
