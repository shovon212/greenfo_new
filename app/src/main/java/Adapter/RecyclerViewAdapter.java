package Adapter;

/**
 * Created by Fuad on 11/29/2017.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
//
import com.bumptech.glide.Glide;
import com.muttasimfuad.greenfo.FullscreenImageView;
import com.muttasimfuad.greenfo.ImageUploadInfo;
import com.muttasimfuad.greenfo.R;

import java.util.List;

/**
 * Created by fuad
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    Context context;
    List<ImageUploadInfo> MainImageUploadInfoList;

    public RecyclerViewAdapter(Context context, List<ImageUploadInfo> TempList) {

        this.MainImageUploadInfoList = TempList;

        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recylerview_items, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ImageUploadInfo UploadInfo = MainImageUploadInfoList.get(position);

        holder.imageNameTextView.setText(UploadInfo.getImageName());

        //Loading image from Glide library.
        Glide.with(context).load(UploadInfo.getImageURL()).into(holder.imageView);
        holder.imageNameTextView.setText(MainImageUploadInfoList.get(position).getImageName());
//        Picasso.with(context).load(MainImageUploadInfoList.get(position).getImageURL()).resize(240, 320).into(holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                         String ImageURL = UploadInfo.getImageURL();

//                        Bundle bundle = new Bundle();
//                        bundle.putInt("id", getLayoutPosition());

                        Intent i = new Intent(context, FullscreenImageView.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("purl", ImageURL);
                        view.getContext().startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {

        return MainImageUploadInfoList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView imageNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageNameTextView = (TextView) itemView.findViewById(R.id.ImageNameTextView);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.getId() == imageNameTextView.getId()) {
                        Toast.makeText(context, "Zoom in feature will be available from next version", Toast.LENGTH_LONG).show();
                    } else if (view.getId() == imageView.getId()) {
                        Toast.makeText(context, "Zoom in feature will be available from next version", Toast.LENGTH_LONG).show();
//
//                        imageView.setDrawingCacheEnabled(true);
//                        Bitmap b=imageView.getDrawingCache();
//
////                        Bundle bundle = new Bundle();
////                        bundle.putInt("id", getLayoutPosition());
//
//                        Intent i = new Intent(context, FullscreenImageView.class);
//                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        i.putExtra("Bitmap", b);
//                        view.getContext().startActivity(i);
                    }

                }
            });



        }
    }


}
